# Linux Movie Maker

Linux Movie Maker is a little video editor that uses SFML and ffmpeg. It uses my own custom GUI made in SFML.

It uses only one thread (if you don't count external libraries' threads).

## Installation

First, ensure you have all the [runtime dependencies](#Runtime-dependencies) installed. **TL;DR:**

- **Linux/Unix**: Install packages providing the `ffmpeg` binary and SFML shared libraries.

    On Debian-based distributions this would be:

    ```
    sudo apt install ffmpeg libsfml-dev
    ```

- **Windows**: Download a ffmpeg build and put `ffmpeg.exe` binary into your PATH.

Then, to install a stable version of this software, go to the releases page, and download an archive for your operating system. Unpack it, and launch the executable inside.

### Runtime dependencies

- ffmpeg

    **Recommended.** For rendering the frames into a video.

    - Linux/Unix

        Install the package providing the `ffmpeg` binary.

        It's just called `ffmpeg` in Debian and most other distributions.

        On Debian, run this to install:

        ```
        sudo apt install ffmpeg
        ```

    - Windows

        You'll probably want to download a Windows build for ffmpeg, for example from there: https://github.com/BtbN/FFmpeg-Builds/releases.

        After downloading, you need to add the `ffmpeg.exe` binary to your PATH. The easiest way to do it is via the *Environment Variables* menu (search in the *Start* menu). You'll need to select the PATH variable, click *Edit*, and add the path to the folder with the `ffmpeg.exe` file.

- SFML (Graphics, Audio, System, Window)

    **Necessary.** For GUI, audio and rendering code.

    - Linux/Unix

        Install the package(s) providing the shared libraries.
        
        In Debian-based distributions, install `libsfml-dev` (it will provide all SFML libraries and development headers):

        ```
        sudo apt install libsfml-dev
        ```

    - Windows

        The libraries are provided inside the release archive and should work™.

## Building

### Build-time dependencies

**TL;DR:**

- **Linux/Unix**: Install packages providing development headers for SFML and RapidJSON.

    On Debian-based distributions this would be:

    ```
    sudo apt install libsfml-dev rapidjson-dev
    ```

- SFML

    **Necessary.**

    - Linux/Unix

        Install the package providing the development headers.

        It's called `libsfml-dev` in Debian-based distributions. To install, use:

        ```
        sudo apt install libsfml-dev
        ```
    
- RapidJSON

    **Necessary.**

    - Linux/Unix

        Install the package providing the development headers.

        It's called `rapidjson-dev` in Debian-based distributions. To install, use:

        ```
        sudo apt install rapidjson-dev
        ```

- CMake

    **Recommended.** It's the build system used by this project. It's not needed to build it, but you probably should use it.

    - Linux/Unix

        Install the package providing the `cmake` binary.

        It's just called `cmake` in Debian and most other distributions.

        On Debian, run this to install:

        ```
        sudo apt install cmake
        ```

### Building via CMake (recommended)

- Create a temporary build directory and change into it (to keep the projects' root clean):
    
    ```
    mkdir build && cd build
    ```

- Configure the project (you also need to do this when you change the CMakeLists.txt, for example to add/remove files):

    - For a debug build:

        ```
        cmake ..
        ```

    - For a release build:

        ```
        cmake -DCMAKE_BUILD_TYPE=Release ..
        ```

- Build the project (`-j $(nproc --all)` will set the amount of spawned processes to the amount of CPU cores in your system. This is not necessary, but will build the project as fast as possible):

    ```
    make -j $(nproc --all)
    ```

### Building manually

You can build manually using a command like this:

```
g++ src/*.cpp src/*/*.cpp src/*/*/*.cpp -g -DDEBUG -std=c++17 -lstdc++fs -lsfml-system -lsfml-window -lsfml-graphics -lsfml-audio -o linux-movie-maker
```

This will not give you any advantages of CMake like fast rebuilding.
