#include "Exception.hpp"

#include <string>
#include <iostream>


int Exception::getCode() {
    return code;
}

std::string Exception::getMessage() {
    return message;
}

void Exception::print() {
    std::cerr << "Exception: " << message << " (code: " << code << ")\n";
}

Exception::Exception(std::string message) 
: Exception(message, 0) {}

Exception::Exception(std::string message, int code) {
    this->message = message;
    this->code = code;
}
