#pragma once

#include <SFML/Graphics.hpp>
#include "Resource.hpp"


class ImageResource : public Resource {

public:
    Resource* loadFromFile(std::filesystem::path);

    sf::Texture* getThumbnail();
    sf::Image* getImage();

    Resource* setImage(sf::Image);

    ImageResource();
    ImageResource(std::filesystem::path);
    ImageResource(std::filesystem::path, std::string);
    ~ImageResource();

private:
    sf::Image image;

};
