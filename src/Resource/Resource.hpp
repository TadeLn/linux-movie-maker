#pragma once

#include <string>
#include <SFML/Graphics.hpp>
#include <filesystem>


enum ResourceType {
    RESOURCE_UNKNOWN = -1,
    RESOURCE_NONE,
    RESOURCE_IMAGE,
};

class Resource {

public:
    std::string getName();
    std::filesystem::path getPath();
    virtual sf::Texture* getThumbnail();

    Resource* setName(std::string);
    Resource* setPath(std::filesystem::path);

    virtual Resource* loadFromFile(std::filesystem::path);

    ResourceType getType();

    Resource();
    Resource(std::string, std::filesystem::path);
    ~Resource();

protected:
    ResourceType type;

private:
    std::string name;
    std::filesystem::path path;
    sf::Texture thumbnail;

};
