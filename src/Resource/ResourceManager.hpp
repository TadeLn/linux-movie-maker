#pragma once

#include <vector>
#include "Resource.hpp"


class ResourceManager {

public:
    Resource* getResource(unsigned int);
    unsigned int getResourceCount();
    std::vector<Resource*>* getResourceVector();
    bool isResourceLoaded(unsigned int);
    bool isResourceLoaded(std::string);

    unsigned int findResourceByName(std::string);

    unsigned int loadResource(Resource*);
    void loadResourceFromFile(std::filesystem::path, std::string);
    void unloadResource(unsigned int);

    void renameResource(unsigned int, std::string);

    ResourceManager();

    static std::string getTypeString(int);

private:
    std::vector<Resource*> resources;

};
