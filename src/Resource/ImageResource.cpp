#include "ImageResource.hpp"
#include "../Exception.hpp"
#include "../Global.hpp"


Resource* ImageResource::loadFromFile(std::filesystem::path path) {
    if (!image.loadFromFile((Global::getApp()->getProjectDir() / path).string())) throw new Exception("Couldn't load from file", EXCEPTION_FILE_ERROR);
    
    Resource::loadFromFile(path);
    return this;
}

sf::Texture* ImageResource::getThumbnail() {
    sf::Texture* texture = new sf::Texture();
    texture->loadFromImage(*getImage());
    return texture;
}

sf::Image* ImageResource::getImage() {
    return &image;
}

Resource* ImageResource::setImage(sf::Image image) {
    this->image = image;
    return this;
}

ImageResource::ImageResource() {
    image = sf::Image();
    this->type = RESOURCE_IMAGE;
}

ImageResource::ImageResource(std::filesystem::path filename)
: ImageResource() {
    loadFromFile(filename);
}

ImageResource::ImageResource(std::filesystem::path filename, std::string name)
: ImageResource(filename) {
    setName(name);
}

ImageResource::~ImageResource() {
}
