#include "ResourceManager.hpp"

#include <iostream>
#include "Resource.hpp"
#include "../Exception.hpp"
#include "../Global.hpp"


Resource* ResourceManager::getResource(unsigned int resId) {
    if (resId > resources.size()) {
        return nullptr;
    }
    return resources[resId];
}

unsigned int ResourceManager::getResourceCount() {
    return resources.size();
}

std::vector<Resource*>* ResourceManager::getResourceVector() {
    return &resources;
}

bool ResourceManager::isResourceLoaded(unsigned int resId) {
    return getResource(resId) != nullptr;
}

bool ResourceManager::isResourceLoaded(std::string name) {
    try {
        findResourceByName(name);
        return true;
    } catch (Exception* e) {
        if (e->getCode() == EXCEPTION_NOT_FOUND) {
            return false;
        } else {
            throw e;
        }
    }
}


unsigned int ResourceManager::findResourceByName(std::string name) {
    for (int i = 0; i < resources.size(); i++) {
        if (resources[i]->getName() == name) {
            return i;
        }
    }
    throw new Exception("Resource not found", EXCEPTION_NOT_FOUND);
}


unsigned int ResourceManager::loadResource(Resource* res) {
    if (res != nullptr) {
        if (isResourceLoaded(res->getName())) {
            throw new Exception("A resource with that name already exists", EXCEPTION_INVALID_VALUE);
        }
    }
    resources.push_back(res);
    Global::getApp()->setUnsaved();
    return resources.size() - 1;
}

void ResourceManager::loadResourceFromFile(std::filesystem::path filename, std::string resourceName) {
    //loadResource()
}

void ResourceManager::unloadResource(unsigned int resId) {
    if (!isResourceLoaded(resId)) {
        throw new Exception("Resource with this id is not loaded ", EXCEPTION_NOT_FOUND);
    }
    delete resources[resId];
    resources[resId] = nullptr;

}


void ResourceManager::renameResource(unsigned int resId, std::string name) {
    if (isResourceLoaded(name)) {
        throw new Exception("A resource with that name already exists", EXCEPTION_INVALID_VALUE);
    }
    if (!isResourceLoaded(resId)) {
        throw new Exception("Resource with this id is not loaded ", EXCEPTION_NOT_FOUND);
    }
    getResource(resId)->setName(name);
}


ResourceManager::ResourceManager() {
    std::cout << "ResourceManager Init\n";
}


std::string ResourceManager::getTypeString(int resourceType) {
    switch (resourceType) {
        case RESOURCE_UNKNOWN: return "unknown";
        case RESOURCE_NONE: return "none";
        case RESOURCE_IMAGE: return "image";
        default: return "invalid resource type";
    }
}