#include "Resource.hpp"

#include "../Global.hpp"


std::string Resource::getName() {
    return name;
}

std::filesystem::path Resource::getPath() {
    return path;
}

sf::Texture* Resource::getThumbnail() {
    return &thumbnail;
}


Resource* Resource::setName(std::string name) {
    this->name = name;
    Global::getApp()->setUnsaved();
    return this;
}

Resource* Resource::setPath(std::filesystem::path path) {
    this->path = path;
    Global::getApp()->setUnsaved();
    return this;
}


Resource* Resource::loadFromFile(std::filesystem::path path) {
    setPath(path);
    return this;
}

ResourceType Resource::getType() {
    return type;
}

Resource::Resource()
: Resource("resource", "") {
}

Resource::Resource(std::string name, std::filesystem::path path) {
    this->name = name;
    this->path = path;
    this->type = RESOURCE_NONE;
    thumbnail = sf::Texture(*Global::getInvalidTexture());
}

Resource::~Resource() {
    
}
