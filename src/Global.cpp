#include "Global.hpp"

#include <SFML/Graphics.hpp>
#include "Settings.hpp"
#include "Application.hpp"


namespace Global {
    Application* application;
    sf::Font font;
    sf::Texture invalidTexture;
    
    bool loadFont() {
        return font.loadFromFile(Settings::getSetting_string("fontFileName"));
    }

    sf::Font* getFont() {
        return &font;
    }

    int getDelta(int baseDelta) {
        int delta = baseDelta;
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift) || sf::Keyboard::isKeyPressed(sf::Keyboard::RShift)) {
            if (baseDelta >= 0) {
                delta = 1;
            } else {
                delta = -1;
            }
        }
        delta *= 1 + ((sf::Keyboard::isKeyPressed(sf::Keyboard::LControl) || sf::Keyboard::isKeyPressed(sf::Keyboard::RControl)) * 9);
        return delta;
    }

    sf::Texture* getInvalidTexture() {
        return &invalidTexture;
    }
    
    void loadApp() {
        application = new Application();
        sf::Uint8* invalidTexturePixels = new sf::Uint8[16]{
            1, 0, 1, 1,
            0, 0, 0, 1,
            1, 0, 1, 1,
            0, 0, 0, 1
        };
        sf::Image invalidTextureImage;
        invalidTextureImage.create(2, 2, invalidTexturePixels);
        invalidTexture.loadFromImage(invalidTextureImage);
    }
    
    Application* getApp() {
        return application;
    }
}
