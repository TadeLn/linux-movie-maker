#pragma once

#include <string>


enum ExceptionCodes {
    EXCEPTION_UNKNOWN = -1,
    EXCEPTION_OK,
    EXCEPTION_INVALID_VALUE,
    EXCEPTION_INVALID_LENGTH,
    EXCEPTION_INVALID_TYPE,
    EXCEPTION_INVALID_POSITION,
    EXCEPTION_NOT_FOUND,
    EXCEPTION_NOT_LOADED,
    EXCEPTION_FILE_ERROR,
    EXCEPTION_CODE_ERROR
};


class Exception {

public:
    int getCode();
    std::string getMessage();
    void print();

    Exception(std::string);
    Exception(std::string, int);

private:
    int code;
    std::string message;

};
