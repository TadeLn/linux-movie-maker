#pragma once

#include <string>
#include <filesystem>
#include "Timeline/Timeline.hpp"


class Project {

public:
    unsigned int getFramerate();
    unsigned int getWidth();
    unsigned int getHeight();
    int getLength();
    std::string getName();
    unsigned int getVersion();
    Timeline* getTimeline();

    Project* setFramerate(unsigned int);
    Project* setWidth(unsigned int);
    Project* setHeight(unsigned int);
    Project* setLength(int);
    Project* setName(std::string);
    Project* setVersion(int);

    double frameToSeconds(int);
    int secondsToFrame(double);

    void loadFromFile(std::filesystem::path);
    void loadFromString(std::string);

    void saveToFile(std::filesystem::path);
    std::string saveToString();

    Project();

private:
    unsigned int framerate;
    unsigned int width;
    unsigned int height;
    int length;
    std::string name;
    unsigned int version;
    Timeline timeline;

};
