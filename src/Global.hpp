#pragma once

#define VERSION "1.0.0"

#include <SFML/Graphics.hpp>
#include "Application.hpp"


namespace Global {
    bool loadFont();
    sf::Font* getFont();
    int getDelta(int);
    sf::Texture* getInvalidTexture();

    void loadApp();
    Application* getApp();
};
