#pragma once


#include <vector>
#include <utility>
#include <string>


/*

All settings:
windowFramerateLimit = 60
fontFileName = "res/Quicksand-Regular.ttf"
playJingle = 1

*/



namespace Settings {

    namespace {
        std::vector<std::pair<std::string, std::string>> settings;
    };

    std::string getSetting_string(std::string);
    std::string getSetting_string(std::string, std::string);
    long getSetting_long(std::string);
    long getSetting_long(std::string, long);
    unsigned long getSetting_ulong(std::string);
    unsigned long getSetting_ulong(std::string, unsigned long);
    double getSetting_double(std::string);
    double getSetting_double(std::string, double);

    void setSetting(std::string, std::string);
    void setSetting(std::string, long);
    void setSetting(std::string, unsigned long);
    void setSetting(std::string, double);

    void loadSettings(std::string);

};
