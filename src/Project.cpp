#include "Project.hpp"

#include "Global.hpp"
#include "Exception.hpp"
#include "Resource/ImageResource.hpp"
#include "Timeline/Clip/ResourceClip.hpp"
#include "Timeline/Clip/TextClip.hpp"
#include <chrono>
#include <iostream>
#include <fstream>
#include <rapidjson/document.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>


unsigned int Project::getFramerate() {
    return framerate;
}

unsigned int Project::getWidth() {
    return width;
}

unsigned int Project::getHeight() {
    return height;
}

int Project::getLength() {
    return length;
}

std::string Project::getName() {
    return name;
}

unsigned int Project::getVersion() {
    return version;
}

Timeline* Project::getTimeline() {
    return &timeline;
}


Project* Project::setFramerate(unsigned int framerate) {
    if (framerate > 1000) {
        throw new Exception("Project framerate cannot be over 1000", EXCEPTION_INVALID_VALUE);
    }
    this->framerate = framerate;
    return this;
}

Project* Project::setWidth(unsigned int width) {
    if (width > 10000) {
        throw new Exception("Project width cannot be over 10000", EXCEPTION_INVALID_VALUE);
    }
    this->width = width;
    return this;
}

Project* Project::setHeight(unsigned int height) {
    if (height > 10000) {
        throw new Exception("Project height cannot be over 10000", EXCEPTION_INVALID_VALUE);
    }
    this->height = height;
    return this;
}

Project* Project::setLength(int length) {
    if (length < 1) {
        throw new Exception("Project length cannot be less than 1", EXCEPTION_INVALID_VALUE);
    }
    this->length = length;
    return this;
}

Project* Project::setName(std::string name) {
    if (name.length() > 200) {
        throw new Exception("Project name cannot be over 200 characters long", EXCEPTION_INVALID_LENGTH);
    }
    this->name = name;
    return this;
}

Project* Project::setVersion(int version) {
    if (version < 0) {
        throw new Exception("Project version cannot be less than 0", EXCEPTION_INVALID_VALUE);
    }
    this->version = version;
    return this;
}

double Project::frameToSeconds(int frame) {
    return (double)frame / (double)framerate;
}

int Project::secondsToFrame(double second) {
    return (int)(second * framerate);
}


void Project::loadFromFile(std::filesystem::path path) {
    std::cout << "Loading from file " << path << "...\n";

    std::ifstream file;
    file.open(path);
    if (!file.is_open()) {
        throw new Exception("File '" + path.string() + "' couldn't be opened", EXCEPTION_FILE_ERROR);
    }
    std::string fileContents;
    std::string line;
    while (getline(file, line)) {
        fileContents += line;
    }
    file.close();
    loadFromString(fileContents);
}

void Project::loadFromString(std::string jsonString) {
    
    while (timeline.getTrackCount() > 0) {
        timeline.removeTrack(0);
    }
    std::vector<Resource*>* allResources = Global::getApp()->resourceManager->getResourceVector();
    while (allResources->size() > 0) {
        delete allResources->at(0);
        allResources->erase(allResources->begin());
    }
    
    rapidjson::Document document;
    document.Parse(jsonString.c_str());

    if (document.HasMember("metadata") && document["metadata"].IsObject()) {
        rapidjson::Value obj_metadata = document["metadata"].GetObject();

        if (obj_metadata.HasMember("name") && obj_metadata["name"].IsString())
            setName(obj_metadata["name"].GetString());

        if (obj_metadata.HasMember("framerate") && obj_metadata["framerate"].IsUint())
            setFramerate(obj_metadata["framerate"].GetUint());

        if (obj_metadata.HasMember("width") && obj_metadata["width"].IsUint())
            setWidth(obj_metadata["width"].GetUint());

        if (obj_metadata.HasMember("height") && obj_metadata["height"].IsUint())
            setHeight(obj_metadata["height"].GetUint());

        if (obj_metadata.HasMember("length") && obj_metadata["length"].IsUint())
            setLength(obj_metadata["length"].GetUint());

        if (obj_metadata.HasMember("version") && obj_metadata["version"].IsUint())
            setVersion(obj_metadata["version"].GetUint());

    }

    if (document.HasMember("resources") && document["resources"].IsArray()) {
        rapidjson::Value arr_resources = document["resources"].GetArray();

        for (int i = 0; i < arr_resources.Size(); i++) {
            if (!arr_resources[i].IsObject()) continue;
            rapidjson::Value obj_resource = arr_resources[i].GetObject();
            Resource* resource;

            if (obj_resource.HasMember("type") && obj_resource["type"].IsInt())
                switch (obj_resource["type"].GetInt()) {
                    case RESOURCE_UNKNOWN: resource = nullptr; break;
                    case RESOURCE_NONE: resource = new Resource(); break;
                    case RESOURCE_IMAGE: resource = new ImageResource(); break;
                }

            if (obj_resource.HasMember("name") && obj_resource["name"].IsString())
                resource->setName(obj_resource["name"].GetString());

            if (obj_resource.HasMember("path") && obj_resource["path"].IsArray()) {
                rapidjson::Value arr_path = obj_resource["path"].GetArray();
                std::filesystem::path path;

                for (int j = 0; j < arr_path.Size(); j++) {
                    if (!arr_path[j].IsString()) continue;
                    path /= arr_path[j].GetString();
                }
                resource->loadFromFile(path);
            }

            if (obj_resource.HasMember("path") && obj_resource["path"].IsString()) {
                std::filesystem::path path = std::string(obj_resource["path"].GetString()).substr(1);
                resource->loadFromFile(path);
            }

            Global::getApp()->resourceManager->loadResource(resource);

        }
    }

    if (document.HasMember("tracks") && document["tracks"].IsArray()) {
        rapidjson::Value arr_tracks = document["tracks"].GetArray();

        for (int i = 0; i < arr_tracks.Size(); i++) {
            if (!arr_tracks[i].IsObject()) continue;
            rapidjson::Value obj_track = arr_tracks[i].GetObject();
            Track* track = new Track();

            if (obj_track.HasMember("name") && obj_track["name"].IsString())
                track->setName(obj_track["name"].GetString());

            if (obj_track.HasMember("color") && obj_track["color"].IsUint())
                track->setColor(sf::Color(obj_track["color"].GetUint()));

            if (obj_track.HasMember("clips") && obj_track["clips"].IsArray()){
                rapidjson::Value arr_clips = obj_track["clips"].GetArray();

                for (int i = 0; i < arr_clips.Size(); i++) {
                    if (!arr_clips[i].IsObject()) continue;
                    rapidjson::Value obj_clip = arr_clips[i].GetObject();
                    Clip* clip;

                    if (obj_clip.HasMember("type") && obj_clip["type"].IsInt())
                        switch (obj_clip["type"].GetInt()) {
                            case CLIP_NONE: clip = new Clip(); break;
                            case CLIP_RESOURCE: clip = new ResourceClip(); break;
                            case CLIP_TEXT: clip = new TextClip(); break;
                        }

                    if (obj_clip.HasMember("position") && obj_clip["position"].IsInt())
                        clip->setTime(obj_clip["position"].GetInt());

                    if (obj_clip.HasMember("length") && obj_clip["length"].IsInt())
                        clip->setLength(obj_clip["length"].GetInt());

                    if (obj_clip.HasMember("trim") && obj_clip["trim"].IsInt())
                        clip->setTrimPosition(obj_clip["trim"].GetInt());

                    if (obj_clip.HasMember("data") && obj_clip["data"].IsObject()) {
                        rapidjson::Value obj_data = obj_clip["data"].GetObject();

                        if (clip->getClipType() == CLIP_RESOURCE) {

                            if (obj_data.HasMember("resourceId") && obj_data["resourceId"].IsInt())
                                ((ResourceClip*)clip)->setResourceId(obj_data["resourceId"].GetInt());

                        }
                        if (clip->getClipType() == CLIP_TEXT) {

                            TextClip* textClip = (TextClip*)clip;
                            textClip->loadFromJSON(obj_data);

                        }
                    }

                    track->addClip(clip);
                }
            }

            timeline.addTrack(track);
        }
    }

    Global::getApp()->setActiveClip(-1, -1);
    Global::getApp()->refreshResourcePanel();
    Global::getApp()->redrawAllTracks();
    Global::getApp()->updatePreview();
    Global::getApp()->updateProjectInfo();
    Global::getApp()->showProjectProperties();
    Global::getApp()->unsetUnsaved();
}


void Project::saveToFile(std::filesystem::path path) {
    std::cout << "Saving to file " << path << "...\n";

    if (!std::filesystem::exists(path.parent_path())) {
        std::filesystem::create_directories(path.parent_path());
    }

    std::string content = saveToString();
    std::ofstream file;
    file.open(path);
    if (!file.is_open()) {
        throw new Exception("File '" + path.string() + "' couldn't be opened", EXCEPTION_FILE_ERROR);
    }
    file.clear();
    file << content;
    file.close();
}

std::string Project::saveToString() {
    rapidjson::Document document;
    document.SetObject();

    std::vector<char*> strings;
    auto addString = [&strings](std::string str){
        strings.push_back(new char[str.size()+1]);
        for (int i = 0; i < str.size(); i++) {
            strings[strings.size()-1][i] = str[i];
        }
        strings[strings.size()-1][str.size()] = '\0';
    };

    rapidjson::Value obj_metadata;
    obj_metadata.SetObject();
    obj_metadata.AddMember("name", rapidjson::Value(rapidjson::StringRef(name.c_str())), document.GetAllocator());
    obj_metadata.AddMember("framerate", rapidjson::Value(framerate), document.GetAllocator());
    obj_metadata.AddMember("width", rapidjson::Value(width), document.GetAllocator());
    obj_metadata.AddMember("height", rapidjson::Value(height), document.GetAllocator());
    obj_metadata.AddMember("length", rapidjson::Value(length), document.GetAllocator());
    obj_metadata.AddMember("version", rapidjson::Value(version), document.GetAllocator());
    document.AddMember("metadata", obj_metadata, document.GetAllocator());

    ResourceManager* resourceManager = Global::getApp()->resourceManager;
    rapidjson::Value arr_resources;
    arr_resources.SetArray();
    for (int i = 0; i < resourceManager->getResourceCount(); i++) {
        Resource* resource = resourceManager->getResource(i);
        rapidjson::Value obj_resource;
        obj_resource.SetObject();
        addString(resource->getName());
        obj_resource.AddMember("name", rapidjson::Value(rapidjson::StringRef(strings[strings.size()-1])), document.GetAllocator());

        rapidjson::Value arr_path;
        arr_path.SetArray();
        std::filesystem::path path = resource->getPath();
        for (auto it = path.begin(); it != path.end(); it++) {
            addString((*it).string());
            arr_path.PushBack(rapidjson::Value(rapidjson::StringRef(strings[strings.size()-1])), document.GetAllocator());
        }
        
        obj_resource.AddMember("path", arr_path, document.GetAllocator());
        obj_resource.AddMember("type", rapidjson::Value(resource->getType()), document.GetAllocator());
        arr_resources.PushBack(obj_resource, document.GetAllocator());
    }
    document.AddMember("resources", arr_resources, document.GetAllocator());

    rapidjson::Value arr_tracks;
    arr_tracks.SetArray();
    for (int i = 0; i < timeline.getTrackCount(); i++) {
        Track* track = timeline.getTrack(i);
        rapidjson::Value obj_track;
        obj_track.SetObject();
        addString(track->getName());
        obj_track.AddMember("name", rapidjson::Value(rapidjson::StringRef(strings[strings.size()-1])), document.GetAllocator());
        obj_track.AddMember("color", rapidjson::Value(track->getColor().toInteger()), document.GetAllocator());

        rapidjson::Value arr_clips;
        arr_clips.SetArray();
        for (int j = 0; j < track->getClipCount(); j++) {
            Clip* clip = track->getClipById(j);
            rapidjson::Value obj_clip;
            obj_clip.SetObject();
            obj_clip.AddMember("type", rapidjson::Value(clip->getClipType()), document.GetAllocator());
            obj_clip.AddMember("position", rapidjson::Value(clip->getTime()), document.GetAllocator());
            obj_clip.AddMember("length", rapidjson::Value(clip->getLength()), document.GetAllocator());
            obj_clip.AddMember("trim", rapidjson::Value(clip->getTrimPosition()), document.GetAllocator());

            rapidjson::Value obj_data;
            obj_data.SetObject();
            if (clip->getClipType() == CLIP_RESOURCE) {
                obj_data.AddMember("resourceId", rapidjson::Value(((ResourceClip*)clip)->getResourceId()), document.GetAllocator());
            }
            if (clip->getClipType() == CLIP_TEXT) {
                TextClip* textClip = (TextClip*)clip;
                textClip->saveToJSON(obj_data, document, strings);
            }
            obj_clip.AddMember("data", obj_data, document.GetAllocator());
            arr_clips.PushBack(obj_clip, document.GetAllocator());
        }
        obj_track.AddMember("clips", arr_clips, document.GetAllocator());
        arr_tracks.PushBack(obj_track, document.GetAllocator());
    }
    document.AddMember("tracks", arr_tracks, document.GetAllocator());
    
    rapidjson::StringBuffer sb;
    rapidjson::Writer<rapidjson::StringBuffer> writer(sb);
    document.Accept(writer);
    std::string result = sb.GetString();

    while (!strings.empty()) {
        delete [] strings[0];
        strings.erase(strings.begin());
    }

    return result;
}


Project::Project() {
    setFramerate(30);
    setWidth(1280);
    setHeight(720);
    setLength(100);
    setName("untitled");
    setVersion(1);
    timeline = Timeline();
}
