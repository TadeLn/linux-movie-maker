#include <iostream>
#include <chrono>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include "Application.hpp"
#include "Global.hpp"
#include "Settings.hpp"
#include "Exception.hpp"


int main() {
    Settings::setSetting("windowFramerateLimit", (unsigned long)60);
    Settings::setSetting("fontFileName", "res/font/Quicksand-Regular.ttf");
    Settings::setSetting("playJingle", (unsigned long)1);
    std::cout << "Loading settings file...\n";
    try {
        Settings::loadSettings("settings.txt");
        std::cout << "Loading settings file... Success\n";
    } catch (Exception* e) {
        std::cout << "Loading settings file... Error!\n";
        e->print();
    }

    if (!Global::loadFont()) {
        std::cout << "Failed to load font!\n";
        return -1;
    }

    Global::loadApp();
    Global::getApp()->start();
}
