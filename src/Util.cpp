#include "Util.hpp"

#include <cmath>
#include <algorithm> 
#include <cctype>
#include <locale>

namespace Util {

    std::string secondsToString(double secondsD) {
        double wholeD, fractionalD;
        fractionalD = std::modf(secondsD, &wholeD);
        unsigned int whole = wholeD;
        unsigned int fractional = fractionalD * 1000;

        std::string hours = std::to_string(whole / 3600);
        while (hours.length() < 2) hours.insert(0, "0");

        std::string minutes = std::to_string(whole / 60 % 60);
        while (minutes.length() < 2) minutes.insert(0, "0");

        std::string seconds = std::to_string(whole % 60);
        while (seconds.length() < 2) seconds.insert(0, "0");

        std::string milliseconds = std::to_string(fractional);
        while (milliseconds.length() < 3) milliseconds.insert(0, "0");

        return hours + ":" + minutes + ":" + seconds + "." + milliseconds;
    }

    // trim from start (in place)
    void ltrim(std::string &s) {
        s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch) {
            return !std::isspace(ch);
        }));
    }

    // trim from end (in place)
    void rtrim(std::string &s) {
        s.erase(std::find_if(s.rbegin(), s.rend(), [](unsigned char ch) {
            return !std::isspace(ch);
        }).base(), s.end());
    }

    // trim from both ends (in place)
    void trim(std::string &s) {
        ltrim(s);
        rtrim(s);
    }

    // trim from start (copying)
    std::string ltrim_copy(std::string s) {
        ltrim(s);
        return s;
    }

    // trim from end (copying)
    std::string rtrim_copy(std::string s) {
        rtrim(s);
        return s;
    }

    // trim from both ends (copying)
    std::string trim_copy(std::string s) {
        trim(s);
        return s;
    }

};
