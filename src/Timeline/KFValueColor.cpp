#include "KFValueColor.hpp"


#include <SFML/Graphics.hpp>


sf::Color KFValueColor::get() {
    return sf::Color(r.get(), g.get(), b.get(), a.get());
}

KFValueColor* KFValueColor::set(sf::Color color) {
    r.set(color.r);
    g.set(color.g);
    b.set(color.b);
    a.set(color.a);
    return this;
}