#pragma once


#include <SFML/Graphics.hpp>

#include "KFValue.hpp"



// Two key-frameable values
class KFValueColor {

public:
    KFValue<char> r;
    KFValue<char> g;
    KFValue<char> b;
    KFValue<char> a;

    sf::Color get();
    KFValueColor* set(sf::Color);

};