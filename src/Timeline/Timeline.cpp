#include "Timeline.hpp"

#include "../Exception.hpp"
#include "../Global.hpp"


unsigned int Timeline::getTrackCount() {
    return tracks.size();
}

Track* Timeline::getTrack(unsigned int id) {
    return tracks[id];
}

unsigned int Timeline::getCurrentPosition() {
    return currentPosition;
}


Timeline* Timeline::setCurrentPosition(int currentPosition) {
    if (currentPosition < 0) {
        throw new Exception("Current position cannot be less than 0", EXCEPTION_INVALID_VALUE);
    }
    this->currentPosition = currentPosition;
    return this;
}


void Timeline::addTrack(Track* track) {
    tracks.push_back(track);
    Global::getApp()->setUnsaved();
}

void Timeline::removeTrack(unsigned int id) {
    tracks.erase(tracks.begin() + id);
    Global::getApp()->setUnsaved();
}

Timeline::Timeline() {
    this->currentPosition = 0;
}
