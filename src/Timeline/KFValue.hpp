#pragma once



// Key-frameable value
template <typename T>
class KFValue {

public:
    T get();
    KFValue<T>* set(T);

private:
    T value;

};



template <typename T>
T KFValue<T>::get() {
    return value;
}

template <typename T>
KFValue<T>* KFValue<T>::set(T value) {
    this->value = value;
    return this;
}