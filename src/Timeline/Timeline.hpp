#pragma once

#include <vector>
#include "Clip/Clip.hpp"
#include "Track.hpp"


class Timeline {

public:
    unsigned int getTrackCount();
    Track* getTrack(unsigned int);
    unsigned int getCurrentPosition();

    Timeline* setCurrentPosition(int);

    void addTrack(Track*);
    void removeTrack(unsigned int);

    Timeline();

private:
    std::vector<Track*> tracks;
    unsigned int currentPosition;

};
