#pragma once


#include "KFValue.hpp"



// Two key-frameable values
template <typename T>
class KFValuePair {

public:
    KFValue<T> x;
    KFValue<T> y;

};