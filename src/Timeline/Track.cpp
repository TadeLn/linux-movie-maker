#include "Track.hpp"

#include "../Exception.hpp"
#include "../Global.hpp"


std::string Track::getName() {
    return name;
}

sf::Color Track::getColor() {
    return color;
}


Track* Track::setName(std::string name) {
    this->name = name;
    Global::getApp()->setUnsaved();
    return this;
}

Track* Track::setColor(sf::Color color) {
    this->color = color;
    Global::getApp()->setUnsaved();
    return this;
}


Track* Track::addClip(Clip* clip) {
    if (isSpaceFree(clip->getTime(), clip->getTime() + clip->getLength() - 1)) {
        clips.push_back(clip);
    } else {
        throw new Exception("That position is occupied", EXCEPTION_INVALID_POSITION);
    }
    Global::getApp()->setUnsaved();
    return this;
}

Track* Track::removeClip(unsigned int clipId) {
    if (clipId > clips.size()) {
        throw new Exception("There is no clip with this id", EXCEPTION_INVALID_VALUE);
    }
    clips.erase(clips.begin() + clipId);
    Global::getApp()->setUnsaved();
    return this;
}

unsigned int Track::getClipCount() {
    return clips.size();
}

Clip* Track::getClipById(unsigned int clipId) {
    if (clipId > clips.size()) {
        throw new Exception("There is no clip with this id", EXCEPTION_INVALID_VALUE);
    }
    return clips[clipId];
}

unsigned int Track::getClipIdAtPosition(int position) {
    // Can be improved if sorted
    for (int i = 0; i < clips.size(); i++) {
        auto clip = clips[i];
        if (position >= clip->getTime() && position < clip->getTime() + clip->getLength()) {
            return i;
        }
    }
    throw new Exception("There is no clip at this position", EXCEPTION_NOT_FOUND);
}

bool Track::isSpaceFree(int positionA, int positionB) {
    return isSpaceFree(positionA, positionB, -1);
}

bool Track::isSpaceFree(int positionA, int positionB, int ignoreClipId) {
    // Can be improved if sorted
    for (int i = 0; i < clips.size(); i++) {
        if (i == ignoreClipId) {
            continue;
        }

        Clip* clip = clips[i];
        if ((positionA < clip->getTime() && positionB < clip->getTime())
         || (positionA >= clip->getTime() + clip->getLength() && positionB >= clip->getTime() + clip->getLength())) {
            continue;
        } else {
            return false;
        }
    }
    return true;
}
