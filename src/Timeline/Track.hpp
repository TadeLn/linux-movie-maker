#pragma once

#include <vector>
#include <string>
#include <SFML/Graphics.hpp>
#include "Clip/Clip.hpp"


class Track {

public:
    std::string getName();
    sf::Color getColor();

    Track* setName(std::string);
    Track* setColor(sf::Color);

    Track* addClip(Clip*);
    Track* removeClip(unsigned int);

    unsigned int getClipCount();
    Clip* getClipById(unsigned int);
    unsigned int getClipIdAtPosition(int);
    bool isSpaceFree(int, int);
    bool isSpaceFree(int, int, int);

private:
    std::string name;
    sf::Color color;
    std::vector<Clip*> clips;

};
