#pragma once


#include <string>
#include <rapidjson/document.h>

#include "Clip.hpp"
#include "../KFValue.hpp"
#include "../KFValuePair.hpp"
#include "../KFValueColor.hpp"



class TextClip : public Clip {

public:
    KFValuePair<float>* getPosition();
    KFValuePair<float>* getOrigin();
    KFValuePair<float>* getScale();
    KFValue<float>* getRotation();
    KFValue<std::string>* getText();
    KFValue<int>* getFontSize();
    KFValueColor* getColor();
    KFValue<float>* getOutline();
    KFValueColor* getOutlineColor();
    KFValue<int>* getStyle();
    KFValue<float>* getLineSpacing();
    KFValue<float>* getLetterSpacing();

    sf::Texture* getThumbnail();
    void updateThumbnail();
    void draw(sf::RenderTarget*, int);

    void loadFromJSON(rapidjson::Value&);
    void saveToJSON(rapidjson::Value&, rapidjson::Document&, std::vector<char*>&);

    TextClip();

private:
    KFValuePair<float> position;
    KFValuePair<float> origin;
    KFValuePair<float> scale;
    KFValue<float> rotation;
    KFValue<std::string> text;
    KFValue<int> fontSize;
    KFValueColor color;
    KFValue<float> outline;
    KFValueColor outlineColor;
    KFValue<int> style;
    KFValue<float> lineSpacing;
    KFValue<float> letterSpacing;

    sf::Text sfText;
    sf::RenderTexture thumbnail;

};
