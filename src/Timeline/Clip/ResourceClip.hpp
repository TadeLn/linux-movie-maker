#pragma once

#include <string>
#include "Clip.hpp"
#include "../../Resource/Resource.hpp"


class ResourceClip : public Clip {

public:
    int getResourceId();
    ResourceClip* setResourceId(int);
    
    Resource* getResource();

    sf::Texture* getThumbnail();
    void draw(sf::RenderTarget*, int);

    ResourceClip();

private:
    int resourceId;

};
