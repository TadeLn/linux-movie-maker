#include "TextClip.hpp"



#include "../../Global.hpp"



KFValuePair<float>* TextClip::getPosition() {
    return &position;
}

KFValuePair<float>* TextClip::getOrigin() {
    return &origin;
}

KFValuePair<float>* TextClip::getScale() {
    return &scale;
}

KFValue<float>* TextClip::getRotation() {
    return &rotation;
}

KFValue<std::string>* TextClip::getText() {
    return &text;
}

KFValue<int>* TextClip::getFontSize() {
    return &fontSize;
}

KFValueColor* TextClip::getColor() {
    return &color;
}

KFValue<float>* TextClip::getOutline() {
    return &outline;
}

KFValueColor* TextClip::getOutlineColor() {
    return &outlineColor;
}

KFValue<int>* TextClip::getStyle() {
    return &style;
}

KFValue<float>* TextClip::getLineSpacing() {
    return &lineSpacing;
}

KFValue<float>* TextClip::getLetterSpacing() {
    return &letterSpacing;
}


sf::Texture* TextClip::getThumbnail() {
    return new sf::Texture(thumbnail.getTexture());
}

void TextClip::updateThumbnail() {
    thumbnail.create(sfText.getLocalBounds().width + 1, sfText.getLocalBounds().height + 1);
    thumbnail.clear(sf::Color::Black);
    thumbnail.draw(sfText);
    thumbnail.display();
}

void TextClip::draw(sf::RenderTarget* target, int frame) {
    sfText.setPosition(sf::Vector2f(position.x.get(), position.y.get()));
    sfText.setOrigin(sf::Vector2f(origin.x.get(), origin.y.get()));
    sfText.setScale(sf::Vector2f(scale.x.get(), scale.y.get()));
    sfText.setRotation(rotation.get());
    sfText.setString(text.get());
    sfText.setCharacterSize(fontSize.get());
    sfText.setFillColor(sf::Color(color.r.get(), color.g.get(), color.b.get(), color.a.get()));
    sfText.setOutlineThickness(outline.get());
    sfText.setOutlineColor(sf::Color(outlineColor.r.get(), outlineColor.g.get(), outlineColor.b.get(), outlineColor.a.get()));
    sfText.setStyle(style.get());
    sfText.setLineSpacing(lineSpacing.get());
    sfText.setLetterSpacing(letterSpacing.get());
    target->draw(sfText);
    updateThumbnail(); // TEMP, update on value change
}


void TextClip::loadFromJSON(rapidjson::Value& obj_data) {
    if (obj_data.HasMember("positionX") && obj_data["positionX"].IsFloat())
        this->getPosition()->x.set(obj_data["positionX"].GetFloat());

    if (obj_data.HasMember("positionY") && obj_data["positionY"].IsFloat())
        this->getPosition()->y.set(obj_data["positionY"].GetFloat());

    if (obj_data.HasMember("originX") && obj_data["originX"].IsFloat())
        this->getOrigin()->x.set(obj_data["originX"].GetFloat());

    if (obj_data.HasMember("originY") && obj_data["originY"].IsFloat())
        this->getOrigin()->y.set(obj_data["originY"].GetFloat());

    if (obj_data.HasMember("scaleX") && obj_data["scaleX"].IsFloat())
        this->getScale()->x.set(obj_data["scaleX"].GetFloat());

    if (obj_data.HasMember("scaleY") && obj_data["scaleY"].IsFloat())
        this->getScale()->y.set(obj_data["scaleY"].GetFloat());

    if (obj_data.HasMember("rotation") && obj_data["rotation"].IsFloat())
        this->getRotation()->set(obj_data["rotation"].GetFloat());

    if (obj_data.HasMember("text") && obj_data["text"].IsString())
        this->getText()->set(obj_data["text"].GetString());

    if (obj_data.HasMember("fontSize") && obj_data["fontSize"].IsUint())
        this->getFontSize()->set(obj_data["fontSize"].GetUint());

    if (obj_data.HasMember("color") && obj_data["color"].IsUint())
        this->getColor()->set(sf::Color(obj_data["color"].GetUint()));

    if (obj_data.HasMember("outline") && obj_data["outline"].IsFloat())
        this->getOutline()->set(obj_data["outline"].GetFloat());

    if (obj_data.HasMember("outlineColor") && obj_data["outlineColor"].IsUint())
        this->getOutlineColor()->set(sf::Color(obj_data["outlineColor"].GetUint()));

    if (obj_data.HasMember("style") && obj_data["style"].IsUint())
        this->getStyle()->set(obj_data["style"].GetUint());

    if (obj_data.HasMember("lineSpacing") && obj_data["lineSpacing"].IsFloat())
        this->getLineSpacing()->set(obj_data["lineSpacing"].GetFloat());

    if (obj_data.HasMember("letterSpacing") && obj_data["letterSpacing"].IsFloat())
        this->getLetterSpacing()->set(obj_data["letterSpacing"].GetFloat());
}

void TextClip::saveToJSON(rapidjson::Value& obj_data, rapidjson::Document& document, std::vector<char*>& strings) {
    auto addString = [&strings](std::string str){
        strings.push_back(new char[str.size()+1]);
        for (int i = 0; i < str.size(); i++) {
            strings[strings.size()-1][i] = str[i];
        }
        strings[strings.size()-1][str.size()] = '\0';
    };
    obj_data.AddMember("positionX", rapidjson::Value(this->getPosition()->x.get()), document.GetAllocator());
    obj_data.AddMember("positionY", rapidjson::Value(this->getPosition()->y.get()), document.GetAllocator());
    obj_data.AddMember("originX", rapidjson::Value(this->getOrigin()->x.get()), document.GetAllocator());
    obj_data.AddMember("originY", rapidjson::Value(this->getOrigin()->y.get()), document.GetAllocator());
    obj_data.AddMember("scaleX", rapidjson::Value(this->getScale()->x.get()), document.GetAllocator());
    obj_data.AddMember("scaleY", rapidjson::Value(this->getScale()->y.get()), document.GetAllocator());
    obj_data.AddMember("rotation", rapidjson::Value(this->getRotation()->get()), document.GetAllocator());
    addString(this->getText()->get());
    obj_data.AddMember("text", rapidjson::Value(rapidjson::StringRef(strings[strings.size()-1])), document.GetAllocator());
    obj_data.AddMember("fontSize", rapidjson::Value(this->getFontSize()->get()), document.GetAllocator());
    obj_data.AddMember("color", rapidjson::Value(this->getColor()->get().toInteger()), document.GetAllocator());
    obj_data.AddMember("outline", rapidjson::Value(this->getOutline()->get()), document.GetAllocator());
    obj_data.AddMember("outlineColor", rapidjson::Value(this->getOutlineColor()->get().toInteger()), document.GetAllocator());
    obj_data.AddMember("style", rapidjson::Value(this->getStyle()->get()), document.GetAllocator());
    obj_data.AddMember("lineSpacing", rapidjson::Value(this->getLineSpacing()->get()), document.GetAllocator());
    obj_data.AddMember("letterSpacing", rapidjson::Value(this->getLetterSpacing()->get()), document.GetAllocator());
}


TextClip::TextClip() 
: Clip() {
    getPosition()->x.set(0);
    getPosition()->y.set(0);
    getOrigin()->x.set(0);
    getOrigin()->y.set(0);
    getScale()->x.set(1);
    getScale()->y.set(1);
    getRotation()->set(0);
    getText()->set("Uninitialized text");
    getFontSize()->set(20);
    getColor()->set(sf::Color::White);
    getOutline()->set(0);
    getOutlineColor()->set(sf::Color::Transparent);
    getStyle()->set(sf::Text::Style::Regular);
    getLineSpacing()->set(1);
    getLetterSpacing()->set(1);
    sfText.setFont(*Global::getFont());
    clipType = CLIP_TEXT;
    updateThumbnail();
}