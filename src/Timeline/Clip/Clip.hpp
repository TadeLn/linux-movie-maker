#pragma once

#include <SFML/Graphics.hpp>


enum ClipType {
    CLIP_NONE,
    CLIP_RESOURCE,
    CLIP_TEXT
};

class Clip {

public:
    int getTime();
    int getLength();
    int getTrimPosition();

    Clip* setTime(int);
    Clip* setLength(int);
    Clip* setTrimPosition(int);

    ClipType getClipType();

    virtual sf::Texture* getThumbnail();
    virtual void draw(sf::RenderTarget*, int);

    Clip();

protected:
    int time; // Position on timeline
    int length; // Length on timeline
    int trimPosition; // Frame number

    ClipType clipType; // Is resource clip

private:
    sf::Texture thumbnail;

};
