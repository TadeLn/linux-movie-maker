#include "ResourceClip.hpp"

#include "../../Global.hpp"
#include "../../Exception.hpp"


int ResourceClip::getResourceId() {
    return resourceId;
}


ResourceClip* ResourceClip::setResourceId(int resourceId) {
    if (Global::getApp()->resourceManager->isResourceLoaded(resourceId)) {
        this->resourceId = resourceId;
    } else {
        throw new Exception("This resource is not loaded", EXCEPTION_NOT_LOADED);
    }
    return this;
}


Resource* ResourceClip::getResource() {
    return Global::getApp()->resourceManager->getResource(resourceId);
}

sf::Texture* ResourceClip::getThumbnail() {
    return getResource()->getThumbnail();
}

void ResourceClip::draw(sf::RenderTarget* target, int frame) {
    Project* project = Global::getApp()->project;

    sf::Texture* texture = getResource()->getThumbnail();
    sf::RectangleShape shape;
    sf::Vector2f size;

    float multiplierX = (float)project->getWidth() / texture->getSize().x;
    float multiplierY = (float)project->getHeight() / texture->getSize().y;
    if (multiplierX < multiplierY) {
        size.x = texture->getSize().x * multiplierX;
        size.y = texture->getSize().y * multiplierX;
        shape.setPosition(sf::Vector2f(0, (project->getHeight() - size.y) / 2));
    } else {
        size.x = texture->getSize().x * multiplierY;
        size.y = texture->getSize().y * multiplierY;
        shape.setPosition(sf::Vector2f((project->getWidth() - size.x) / 2, 0));
    }

    shape.setSize(size);
    shape.setTexture(texture);
    target->draw(shape);
}

ResourceClip::ResourceClip()
: Clip() {
    clipType = CLIP_RESOURCE;
}
