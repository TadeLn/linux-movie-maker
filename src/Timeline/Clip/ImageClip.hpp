#pragma once

#include <string>
#include "ResourceClip.hpp"
#include "../../Resource/ImageResource.hpp"


class ImageClip : public ResourceClip {

public:
    ImageClip* setResourceId(unsigned int);
    
    ImageResource* getResource();

private:
    unsigned int resourceId;

};
