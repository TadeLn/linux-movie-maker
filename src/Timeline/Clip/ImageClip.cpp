#include "ImageClip.hpp"
#include "../../Global.hpp"
#include "../../Exception.hpp"


ImageClip* ImageClip::setResourceId(unsigned int resourceId) {
    auto resManager = Global::getApp()->resourceManager;
    if (resManager->isResourceLoaded(resourceId)) {
        if (resManager->getResource(resourceId)->getType() == RESOURCE_IMAGE) {
            this->resourceId = resourceId;
        } else {
            throw new Exception("This resource's type is not correct", EXCEPTION_INVALID_TYPE);
        }
    } else {
        throw new Exception("This resource is not loaded", EXCEPTION_NOT_LOADED);
    }
    return this;
}
