#include "Clip.hpp"

#include "../../Global.hpp"


int Clip::getTime() {
    return time;
}

int Clip::getLength() {
    return length;
}

int Clip::getTrimPosition() {
    return trimPosition;
}


Clip* Clip::setTime(int position) {
    this->time = position;
    return this;
}

Clip* Clip::setLength(int length) {
    this->length = length;
    return this;
}

Clip* Clip::setTrimPosition(int trimPosition) {
    this->trimPosition = trimPosition;
    return this;
}

ClipType Clip::getClipType() {
    return this->clipType;
}


sf::Texture* Clip::getThumbnail() {
    return &thumbnail;
}

void Clip::draw(sf::RenderTarget*, int) {}


Clip::Clip() {
    setTime(0);
    setLength(1);
    clipType = CLIP_NONE;
    thumbnail = sf::Texture(*Global::getInvalidTexture());
}
