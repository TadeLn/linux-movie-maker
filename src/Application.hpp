#pragma once


class Application;

#include <SFML/Window.hpp>
#include <utility>
#include <filesystem>
#include "Project.hpp"
#include "Widget/RootWidget.hpp"
#include "Widget/ClipWidget.hpp"
#include "Widget/ScrollWidgetContainer.hpp"
#include "Widget/HorizontalScrollWidgetContainer.hpp"
#include "Widget/TextInputWidget.hpp"
#include "Widget/TextWidget.hpp"
#include "Widget/RTWidgetContainer.hpp"
#include "Resource/ResourceManager.hpp"



class Application {

public:
    Project* project;
    sf::RenderWindow window;
    RootWidget* rootWidget;
    ResourceManager* resourceManager;
    unsigned int getFps();

    std::filesystem::path getProjectDir();
    std::filesystem::path getTempDir();
    bool isDefaultTempDir;
    void clearTempData();

    void load();
    void save();
    void setUnsaved();
    void unsetUnsaved();

    void setActiveClip(int, int);
    void setPlaying(bool);

    std::pair<int, int> getActiveClip();
    Clip* getActiveClipPtr();
    ClipWidget* getClipWidget(int, int);
    bool isPlaying(); // Is the project currently playing or is it paused

    void moveActiveClip(int);
    void resizeActiveClip(int);
    void trimActiveClip(int);

    void addClip(int, Clip*); // Add clip to the project + to the timeline
    void deleteClip(int, int); // Remove clip from the project + from the timeline

    void selectTextField(TextInputWidget*);
    void writeChar(char);

    // Move cursor in timeline
    void moveCursor(int);

    void openPreviewPopupWindow();

    void startRendering();
    void cancelRendering();


    // Refresh / update application
    void refreshResourcePanel();
    void redrawAllTracks();
    void updateCursor();
    void updatePreview();
    void drawPreview();
    void updateProjectInfo();
    void refreshProperties();


    // Switch properties sections
    void showProjectProperties(); // Switch properties section to project
    void showResourceProperties(); // Switch properties section to resource
    void showTrackProperties(); // Switch properties section to track
    void showClipProperties(); // Switch properties section to clip

    // Top bar menus
    void toggleFileMenu(); // Toggle menu
    void toggleFileMenu(bool); // Show or hide menu
    bool isFileMenuOpen(); // Check if menu is open

    void toggleEditMenu(); // Toggle menu
    void toggleEditMenu(bool); // Show or hide menu
    bool isEditMenuOpen(); // Check if menu is open

    void toggleClipMenu(); // Toggle menu
    void toggleClipMenu(bool); // Show or hide menu
    bool isClipMenuOpen(); // Check if menu is open

    // Message boxes
    void showAboutMessageBox(); // Show "about" message box
    void showLoadResourceMessageBox(); // Show "load resource" message box
    void showRenameResourceMessageBox(int); // Show "rename resource" message box
    void showCreateTextClipMessageBox();
    void showLoadProjectMessageBox();
    void showNewProjectMessageBox();
    void showChangeTextClipMessageBox();

    // Start app
    void start();

private:
    unsigned int fps;
    void loop(float);
    void renderLoop();
    void convertLoop();
    void finishRendering();
    void startConverting();
    std::filesystem::path lastOutputPath;

    int activeClipTrack;
    int activeClip;
    bool playing;

    bool unsaved;

    bool rendering;
    bool converting;

    double cursorMoveSec;

    TextInputWidget* selectedTextField;

    sf::RenderWindow* previewPopupWindow;
    bool previewPopupWindowFullscreen;


    sf::Text fpsText;

    // IMPORTANT WIDGETS
    WidgetContainer* mainWindow; // Main window, containing other windows

    // Resource window
    WidgetContainer* resourceWindow; // Window containing the resources
    ScrollWidgetContainer* resourcePanel; // Container of resource buttons

    // Properties window
    WidgetContainer* propertiesWindow; // Window containing the properties
    WidgetContainer* projectProperties; // Subsection containing project properties
    WidgetContainer* resourceProperties; // Subsection containing resource properties
    WidgetContainer* trackProperties; // Subsection containing track properties
    WidgetContainer* clipProperties; // Subsection containing clip properties

    // Preview window
    WidgetContainer* previewWindow; // Window containing the preview
    RTWidgetContainer* previewContainer; // RenderTexture preview container
    sf::RenderTexture* previewTexture; // Texture used for rendering the preview
    TextWidget* previewInfo; // Some info above the preview

    // Timeline window
    WidgetContainer* timelineWindow; // Window containing the timeline
    HorizontalScrollWidgetContainer* timeline; // Scrollable timeline
    WidgetContainer* timelineTracks; // Track labels to the left of the timeline
    WidgetContainer* timelineCursor; // Timeline Cursor widget

    // Top bar
    WidgetContainer* topBar;
    Widget* fileMenu;
    Widget* editMenu;
    Widget* clipMenu;

    // Message boxes
    WidgetContainer* aboutMessageBox; // About message
    WidgetContainer* loadResourceMessageBox; // Load resource dialog
    WidgetContainer* renameResourceMessageBox; // Rename resource dialog
    WidgetContainer* renderingMessageBox;
    TextWidget* renderingProgressText;
    WidgetContainer* renderingDoneMessageBox;
    TextWidget* renderingDoneText;
    WidgetContainer* createTextClipMessageBox;
    WidgetContainer* loadProjectMessageBox; // Load project dialog
    WidgetContainer* newProjectMessageBox; // New project dialog
    WidgetContainer* changeTextClipMessageBox;

};
