#include "Application.hpp"

#include <string>
#include <iostream>
#include <chrono>
#include <thread>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <sstream>
#include <iomanip>
#include <filesystem>

#include "Settings.hpp"
#include "Exception.hpp"
#include "Global.hpp"
#include "Util.hpp"

#include "Resource/ResourceManager.hpp"
#include "Resource/Resource.hpp"
#include "Resource/ImageResource.hpp"
#include "Timeline/Clip/ResourceClip.hpp"
#include "Timeline/Clip/TextClip.hpp"

#include "Widget/ButtonWidget.hpp"
#include "Widget/ClipWidget.hpp"
#include "Widget/ColorWidget.hpp"
#include "Widget/HorizontalScrollWidgetContainer.hpp"
#include "Widget/ImageWidget.hpp"
#include "Widget/PreviewWidget.hpp"
#include "Widget/ResourceButtonWidget.hpp"
#include "Widget/RootWidget.hpp"
#include "Widget/RTWidgetContainer.hpp"
#include "Widget/ScrollWidgetContainer.hpp"
#include "Widget/TextInputWidget.hpp"
#include "Widget/TextWidget.hpp"
#include "Widget/TrackWidget.hpp"
#include "Widget/Widget.hpp"
#include "Widget/WidgetContainer.hpp"
#include "Widget/WidgetWindow.hpp"



unsigned int Application::getFps() {
    return fps;
}


std::filesystem::path Application::getProjectDir() {
    return std::filesystem::current_path() / "projects" / project->getName();
}

std::filesystem::path Application::getTempDir() {
    if (isDefaultTempDir) {
        return std::filesystem::temp_directory_path() / "linuxMovieMaker" / project->getName();
    } else {
        return std::filesystem::current_path() / "temp" / project->getName();
    }
}

void Application::clearTempData() {
    std::filesystem::remove_all(getTempDir() / "..");
}



void Application::load() {
    showLoadProjectMessageBox();
}

void Application::save() {
    try {
        project->saveToFile(Global::getApp()->getProjectDir() / "project.json");
        unsaved = false;
        updateProjectInfo();
    } catch (Exception* e) {
        e->print();
    }
}

void Application::setUnsaved() {
    if (!unsaved) {
        unsaved = true;
        updateProjectInfo();
    }
}

void Application::unsetUnsaved() {
    unsaved = false;
    updateProjectInfo();
}


void Application::setActiveClip(int trackNumber, int clipNumber) {
    activeClipTrack = trackNumber;
    activeClip = clipNumber;
    refreshProperties();
}

void Application::setPlaying(bool playing) {
    this->playing = playing;
}

std::pair<int, int> Application::getActiveClip() {
    return std::pair<int, int>(activeClipTrack, activeClip);
}

Clip* Application::getActiveClipPtr() {
    if (activeClipTrack == -1 || activeClip == -1) {
        return nullptr;
    }
    return project->getTimeline()->getTrack(activeClipTrack)->getClipById(activeClip);
}

ClipWidget* Application::getClipWidget(int trackId, int clipId) {
    WidgetContainer* trackWidget = (WidgetContainer*)timeline->getChildren()->at(trackId);
    for (int i = 0; i < trackWidget->getChildren()->size(); i++) {
        ClipWidget* clipWidget = (ClipWidget*)trackWidget->getChildren()->at(i);
        if (clipWidget->getClipId() == clipId) {
            return clipWidget;
        }
    }
    return nullptr;
}


void Application::addClip(int trackId, Clip* clipPtr) {
    if (trackId != -1 && clipPtr != nullptr) {
        Track* track = project->getTimeline()->getTrack(trackId);
        track->addClip(clipPtr);

        ((WidgetContainer*)timeline->getChildren()->at(trackId))->addWidget((new ClipWidget())->setClip(trackId, clipPtr));
        setUnsaved();
    }
}

void Application::deleteClip(int trackId, int clipId) {
    if (trackId != -1) {
        WidgetContainer* trackWidget = (WidgetContainer*)timeline->getChildren()->at(trackId);
        for (int i = 0; i < trackWidget->getChildren()->size(); i++) {
            ClipWidget* clipWidget = (ClipWidget*)trackWidget->getChildren()->at(i);
            if (clipWidget->getClipId() == clipId) {
                delete clipWidget;
                trackWidget->getChildren()->erase(trackWidget->getChildren()->begin() + i);
                project->getTimeline()->getTrack(trackId)->removeClip(clipId);
                return;
            }
        }
        setUnsaved();
    }
}


bool Application::isPlaying() {
    return playing;
}


void Application::moveActiveClip(int delta) {
    Clip* clip = getActiveClipPtr();
    Track* track = project->getTimeline()->getTrack(activeClipTrack);
    if (track->isSpaceFree(clip->getTime() + delta, clip->getTime() + delta + clip->getLength() - 1, activeClip)) {
        if (clip->getTime() + delta < 0) {
            if (track->isSpaceFree(0, clip->getLength() - 1, activeClip)) {
                clip->setTime(0);
            }
        } else {
            clip->setTime(clip->getTime() + delta);
        }
    }
    getClipWidget(activeClipTrack, activeClip)->updateRect();
    refreshProperties();
    setUnsaved();
}

void Application::resizeActiveClip(int delta) {
    Clip* clip = getActiveClipPtr();
    Track* track = project->getTimeline()->getTrack(activeClipTrack);
    if (track->isSpaceFree(clip->getTime(), clip->getTime() + clip->getLength() + delta - 1, activeClip)) {
        if (clip->getLength() + delta < 1) {
            if (track->isSpaceFree(clip->getTime(), clip->getTime(), activeClip)) {
                clip->setLength(1);
            }
        } else {
            clip->setLength(clip->getLength() + delta);
        }
    }
    getClipWidget(activeClipTrack, activeClip)->updateRect();
    refreshProperties();
    setUnsaved();
}

void Application::trimActiveClip(int delta) {
    Clip* clip = getActiveClipPtr();
    if (clip->getTrimPosition() + delta < 0) {
        clip->setTrimPosition(0);
    } else {
        clip->setTrimPosition(clip->getTrimPosition() + delta);
    }
    refreshProperties();
    setUnsaved();
}


void Application::selectTextField(TextInputWidget* widget) {
    if (selectedTextField != nullptr) {
        selectedTextField->setActive(false);
    }
    selectedTextField = widget;
}

void Application::writeChar(char character) {
    if (selectedTextField != nullptr) {
        if (character == 8) {
            if (!selectedTextField->getInput().empty()) {
                std::string str = selectedTextField->getInput();
                selectedTextField->setInput(str.substr(0, str.size() - 1));
            }
        } else {
            selectedTextField->setInput(selectedTextField->getInput() + character);
        }
    }
}

void Application::moveCursor(int delta) {
    Timeline* timelinePtr = project->getTimeline();
    try {
        timelinePtr->setCurrentPosition(timelinePtr->getCurrentPosition() + delta);
    } catch (Exception* e) {
        if (e->getCode() == EXCEPTION_INVALID_VALUE) {
            timelinePtr->setCurrentPosition(0);
        } else {
            throw e;
        }
    }
    Global::getApp()->updateCursor();
}


void Application::openPreviewPopupWindow() {
    if (previewPopupWindow == nullptr) {
        previewPopupWindow = new sf::RenderWindow();
        previewPopupWindow->create(sf::VideoMode(project->getWidth(), project->getHeight()), "Linux Movie Maker v" VERSION " - Preview");
    }
    previewPopupWindow->requestFocus();
}



void Application::startRendering() {
    rendering = true;
    converting = false;
    renderingMessageBox->setVisible(true);
    window.setFramerateLimit(0);
    project->getTimeline()->setCurrentPosition(0);

    std::filesystem::create_directories(getTempDir() / "render");
}

void Application::cancelRendering() {
    rendering = false;
    converting = false;
    renderingMessageBox->setVisible(false);
    window.setFramerateLimit(Settings::getSetting_ulong("windowFramerateLimit"));
}

void Application::refreshResourcePanel() {
    std::cout << "Refreshing resource panel...\n";
    std::vector<Widget*>* children = resourcePanel->getChildren();
    for (int i = 0; i < children->size(); i++) {
        delete children->at(i);
    }
    children->clear();

    for (int i = 0; i < resourceManager->getResourceCount(); i++) {
        if (!resourceManager->isResourceLoaded(i)) continue;

        Resource* resource = resourceManager->getResource(i);
        
        resourcePanel->addWidget(
            (new ResourceButtonWidget(*(new WidgetRect(0, i * 50, 0, 50))->setRelSizeX(1)))
            ->setResourceId(i)
        );
        resourcePanel->addWidget(
            (new TextWidget(resource->getName(), WidgetRect(55, i * 50, 0, 50)))
            ->setCharacterSize(20)
            ->setAlignment(TEXT_ALIGNMENT_LEFT)
            ->onParentResize(
                [](Widget* widget, sf::Vector2f size) {
                    WidgetRect* rect = widget->getRect();
                    rect->setAbsSizeX(size.x - 55);
                }
            )
        );
        resourcePanel->addWidget(
            (new TextWidget(std::string("[#") + std::to_string(i) + "] " + resource->getPath().string(), WidgetRect(65, 20 + i * 50, 0, 50)))
            ->setColor(sf::Color(128, 128, 128))
            ->setCharacterSize(10)
            ->setAlignment(TEXT_ALIGNMENT_LEFT)
            ->onParentResize(
                [](Widget* widget, sf::Vector2f size) {
                    WidgetRect* rect = widget->getRect();
                    rect->setAbsSizeX(size.x - 65);
                }
            )
        );
        resourcePanel->addWidget(
            (new ImageWidget(*(new WidgetRect(0, i * 50, 50, 50))))
            ->setTexture(resource->getThumbnail())
            ->setColor(sf::Color(255, 255, 255))
        );
    }
}

void Application::redrawAllTracks() {
    std::cout << "Redrawing all tracks...\n";
    timelineTracks->clearChildren();
    timeline->clearChildren();
    
    Timeline* timelinePtr = project->getTimeline();

    for (int i = 0; i < timelinePtr->getTrackCount(); i++) {

        Track* trackPtr = timelinePtr->getTrack(i);
        timelineTracks->addWidget((new TrackWidget(*(new WidgetRect(0, 1 + 50 * (i + 1), 0, 48))))
            ->setTrackId(i)
            ->setColor(trackPtr->getColor())
            ->setName(trackPtr->getName())
            ->getWidget()
            ->onParentResize(
                [](Widget* widget, sf::Vector2f size) {
                    widget->getRect()->setPositionX(2)->setSizeX(size.x - 2);
                }
            )
        );


        WidgetContainer* timelineArea = (new WidgetContainer(WidgetRect(0, 1 + 50 * (i + 1), project->getLength(), 48)))
            ->setColor(sf::Color(42, 42, 42));

        for (int j = 0; j < trackPtr->getClipCount(); j++) {
            Clip* clip = trackPtr->getClipById(j);
            timelineArea->addWidget((new ClipWidget())->setClip(i, clip));
        }
 
        timeline->addWidget(timelineArea);
    }
    {
        sf::FloatRect floatRect = timelineTracks->getFloatRect();
        timelineTracks->parentResize(sf::Vector2f(floatRect.width, floatRect.height));
    }

    timelineCursor = (new WidgetContainer(*(new WidgetRect(project->getTimeline()->getCurrentPosition(), 0, 2, 0))->setRelSizeY(1)))
        ->setColor(sf::Color(255, 42, 42, 127))
        ->addWidget((new TextWidget("", WidgetRect(5, 25, 200, 50)))
            ->setColor(sf::Color(255, 42, 42))
            ->setAlignment(TEXT_ALIGNMENT_LEFT)
            ->setCharacterSize(15)
            ->setStyle(sf::Text::Style::Bold)
        );
    timeline->addWidget(timelineCursor);

    updateCursor();
    refreshProperties();
    return;
}

void Application::updateCursor() {
    timelineCursor->getRect()->setAbsPositionX(project->getTimeline()->getCurrentPosition());
    ((TextWidget*)timelineCursor->getChildren()->at(0))->setString(Util::secondsToString(project->frameToSeconds(project->getTimeline()->getCurrentPosition())));
}

void Application::updatePreview() {
    previewTexture = previewContainer->getRenderTexture();
    drawPreview();
}

void Application::drawPreview() {
    sf::FloatRect floatRect = previewContainer->getFloatRect();

    unsigned int currentPos = project->getTimeline()->getCurrentPosition();

    previewTexture->clear(sf::Color::Black);
    if (currentPos > project->getLength()) {
        sf::RectangleShape shape(sf::Vector2f(project->getWidth(), project->getHeight()));
        shape.setFillColor(sf::Color::Blue);
        previewTexture->draw(shape);
    }

    for (int i = project->getTimeline()->getTrackCount() - 1; i >= 0; i--) {
        Track* trackPtr = project->getTimeline()->getTrack(i);
        Clip* clipPtr;
        try {
            clipPtr = trackPtr->getClipById(trackPtr->getClipIdAtPosition(currentPos));
        } catch (Exception* e) {
            clipPtr = nullptr;
        }
        if (clipPtr != nullptr) {
            clipPtr->draw(previewTexture, currentPos - clipPtr->getTime() + clipPtr->getTrimPosition());
        }
    }

    previewTexture->display();
}

void Application::updateProjectInfo() {
    if (!project->getName().empty()) {
        window.setTitle("Linux Movie Maker v" VERSION " - " + project->getName() + (unsaved ? "*" : ""));
    } else {
        window.setTitle("Linux Movie Maker v" VERSION);
    }
    previewInfo->setString(std::to_string(project->getWidth()) + "x" + std::to_string(project->getHeight()) + ", " + std::to_string(project->getFramerate()) + " fps");
}

void Application::refreshProperties() {
    const int height = 25;
    projectProperties->clearChildren();
    trackProperties->clearChildren();
    resourceProperties->clearChildren();
    clipProperties->clearChildren();

    Clip* activeClipPtr = getActiveClipPtr();
    Track* trackPtr = nullptr;
    if (activeClipTrack != -1) {
        trackPtr = Global::getApp()->project->getTimeline()->getTrack(activeClipTrack);
    }
    

    projectProperties->addWidget((new TextWidget("Project", *(new WidgetRect(0, 25 * 0, 0, 25))->setRelSizeX(1)))->setStyle(sf::Text::Style::Bold));
    {
        int line = 1;
        projectProperties->addWidget((new TextWidget("Name: " + project->getName(), *(new WidgetRect(0, height * line++, 0, height))->setRelSizeX(1)))->setAlignment(TEXT_ALIGNMENT_LEFT));
        projectProperties->addWidget((new TextWidget("Width: " + std::to_string(project->getWidth()), *(new WidgetRect(0, height * line++, 0, height))->setRelSizeX(1)))->setAlignment(TEXT_ALIGNMENT_LEFT));
        projectProperties->addWidget((new TextWidget("Height: " + std::to_string(project->getHeight()), *(new WidgetRect(0, height * line++, 0, height))->setRelSizeX(1)))->setAlignment(TEXT_ALIGNMENT_LEFT));
        projectProperties->addWidget((new TextWidget("Framerate: " + std::to_string(project->getFramerate()), *(new WidgetRect(0, height * line++, 0, height))->setRelSizeX(1)))->setAlignment(TEXT_ALIGNMENT_LEFT));
    }
    

    trackProperties->addWidget((new TextWidget("Track", *(new WidgetRect(0, 25 * 0, 0, 25))->setRelSizeX(1)))->setStyle(sf::Text::Style::Bold));
    if (trackPtr == nullptr) {
        trackProperties->addWidget((new TextWidget("No track selected.", *(new WidgetRect(0, height * 1, 0, 20))->setRelSizeX(1)))
            ->setColor(sf::Color(127, 127, 127))
        );
    } else {
        int line = 1;
        trackProperties->addWidget((new TextWidget("Name: " + trackPtr->getName(), *(new WidgetRect(0, height * line++, 0, height))->setRelSizeX(1)))->setAlignment(TEXT_ALIGNMENT_LEFT));
        trackProperties->addWidget((new TextWidget("Color: ", *(new WidgetRect(0, height * line, 0, height))->setRelSizeX(1)))->setAlignment(TEXT_ALIGNMENT_LEFT));
        trackProperties->addWidget((new ColorWidget(WidgetRect(150, height * line++ + 5, height, height)))->setColor(trackPtr->getColor()));
        trackProperties->addWidget((new TextWidget("Clip Count: " + std::to_string(trackPtr->getClipCount()), *(new WidgetRect(0, height * line++, 0, height))->setRelSizeX(1)))->setAlignment(TEXT_ALIGNMENT_LEFT));
        trackProperties->addWidget((new TextWidget("Internal ID: " + std::to_string(activeClipTrack), *(new WidgetRect(0, height * line++, 0, height))->setRelSizeX(1)))->setAlignment(TEXT_ALIGNMENT_LEFT));
    }


    resourceProperties->addWidget((new TextWidget("Resource", *(new WidgetRect(0, 25 * 0, 0, 25))->setRelSizeX(1)))->setStyle(sf::Text::Style::Bold));
    if (activeClipPtr == nullptr) {
        resourceProperties->addWidget((new TextWidget("No clip selected.", *(new WidgetRect(0, height * 1, 0, 20))->setRelSizeX(1)))
            ->setColor(sf::Color(127, 127, 127))
        );
    } else {
        if (activeClipPtr->getClipType() == CLIP_RESOURCE) {
            ResourceClip* resourceClip = (ResourceClip*)activeClipPtr;
            Resource* resource = resourceClip->getResource();
            int line = 1;
            resourceProperties->addWidget((new TextWidget("Name: " + resource->getName(), *(new WidgetRect(0, height * line++, 0, height))->setRelSizeX(1)))->setAlignment(TEXT_ALIGNMENT_LEFT));
            resourceProperties->addWidget((new TextWidget("Path: " + resource->getPath().string(), *(new WidgetRect(0, height * line++, 0, height))->setRelSizeX(1)))->setAlignment(TEXT_ALIGNMENT_LEFT));
            resourceProperties->addWidget((new TextWidget("Type: " + ResourceManager::getTypeString(resource->getType()), *(new WidgetRect(0, height * line++, 0, height))->setRelSizeX(1)))->setAlignment(TEXT_ALIGNMENT_LEFT));
            resourceProperties->addWidget((new TextWidget("Thumbnail: ", *(new WidgetRect(0, height * line, 0, height))->setRelSizeX(1)))->setAlignment(TEXT_ALIGNMENT_LEFT));
            resourceProperties->addWidget((new ImageWidget(WidgetRect(150, height * line++ + 5, height, height)))->setTexture(resource->getThumbnail()));
            resourceProperties->addWidget((new TextWidget("Internal ID: " + std::to_string(resourceClip->getResourceId()), *(new WidgetRect(0, height * line++, 0, height))->setRelSizeX(1)))->setAlignment(TEXT_ALIGNMENT_LEFT));
        } else {
            resourceProperties->addWidget((new TextWidget("No resource clip selected.", *(new WidgetRect(0, height * 1, 0, 20))->setRelSizeX(1)))
                ->setColor(sf::Color(127, 127, 127))
            );
        }
    }


    clipProperties->addWidget((new TextWidget("Clip", *(new WidgetRect(0, 25 * 0, 0, 25))->setRelSizeX(1)))->setStyle(sf::Text::Style::Bold));
    if (activeClipPtr == nullptr) {
        clipProperties->addWidget((new TextWidget("No clip selected.", *(new WidgetRect(0, height * 1, 0, 20))->setRelSizeX(1)))
            ->setColor(sf::Color(127, 127, 127))
        );
    } else {
        int line = 1;

        // Create button for editing int values
        auto editIntButton = [&](WidgetContainer* parent, KFValue<int>* valuePtr, int sensitivity = 10, int divisor = 1) {

            struct ExtraData {
                KFValue<int>* valuePtr;
                int sensitivity;
                int divisor;
            };
            struct ExtraData* extraData = new ExtraData(ExtraData {valuePtr, sensitivity, divisor});

            parent->addWidget(
                (new ButtonWidget("/", WidgetRect(0, height * line + 5, height, height)))
                    ->onParentResize([](Widget* widget, sf::Vector2f size) {
                        WidgetRect* rect = widget->getRect();
                        rect->setAbsPositionX(size.x - (rect->getSizeX().val() * 1));
                    })
                    ->onClick([](Widget* widget, sf::Vector2i) {
                        struct ExtraData {
                            KFValue<int>* valuePtr;
                            int sensitivity;
                            int divisor;
                        } extraData;
                        extraData = *((ExtraData*)widget->getExtraData());
                        extraData.valuePtr->set(extraData.valuePtr->get() + (float)Global::getDelta(extraData.sensitivity) / extraData.divisor);
                        Global::getApp()->refreshProperties();
                        Global::getApp()->setUnsaved();
                    })
                    ->setExtraData(extraData)
            );
        };

        // Create button for editing float values
        auto editFloatButton = [&](WidgetContainer* parent, KFValue<float>* valuePtr, int sensitivity = 10, int divisor = 1) {

            struct ExtraData {
                KFValue<float>* valuePtr;
                int sensitivity;
                int divisor;
            };
            struct ExtraData* extraData = new ExtraData(ExtraData {valuePtr, sensitivity, divisor});

            parent->addWidget(
                (new ButtonWidget("/", WidgetRect(0, height * line + 5, height, height)))
                    ->onParentResize([](Widget* widget, sf::Vector2f size) {
                        WidgetRect* rect = widget->getRect();
                        rect->setAbsPositionX(size.x - (rect->getSizeX().val() * 1));
                    })
                    ->onClick([](Widget* widget, sf::Vector2i) {
                        struct ExtraData {
                            KFValue<float>* valuePtr;
                            int sensitivity;
                            int divisor;
                        } extraData;
                        extraData = *((ExtraData*)widget->getExtraData());
                        extraData.valuePtr->set(extraData.valuePtr->get() + (float)Global::getDelta(extraData.sensitivity) / extraData.divisor);
                        Global::getApp()->refreshProperties();
                        Global::getApp()->setUnsaved();
                    })
                    ->setExtraData(extraData)
            );
        };

        // Create button for editing string values
        auto editStringButton = [&](WidgetContainer* parent, KFValue<std::string>* valuePtr) {

            struct ExtraData {
                KFValue<std::string>* valuePtr;
            };
            struct ExtraData* extraData = new ExtraData(ExtraData {valuePtr});

            parent->addWidget(
                (new ButtonWidget("/", WidgetRect(0, height * line + 5, height, height)))
                    ->onParentResize([](Widget* widget, sf::Vector2f size) {
                        WidgetRect* rect = widget->getRect();
                        rect->setAbsPositionX(size.x - (rect->getSizeX().val() * 1));
                    })
                    ->onClick([](Widget* widget, sf::Vector2i) {
                        struct ExtraData {
                            KFValue<std::string>* valuePtr;
                        } extraData;
                        extraData = *((ExtraData*)widget->getExtraData());
                        extraData.valuePtr->set("Text");
                        Global::getApp()->refreshProperties();
                        Global::getApp()->setUnsaved();
                    })
                    ->setExtraData(extraData)
            );
        };

        // Create button for editing color values
        auto editColorButton = [&](WidgetContainer* parent, KFValueColor* valuePtr) {

            struct ExtraData {
                KFValueColor* valuePtr;
            };
            struct ExtraData* extraData = new ExtraData(ExtraData {valuePtr});

            parent->addWidget(
                (new ButtonWidget("/", WidgetRect(0, height * line + 5, height, height)))
                    ->onParentResize([](Widget* widget, sf::Vector2f size) {
                        WidgetRect* rect = widget->getRect();
                        rect->setAbsPositionX(size.x - (rect->getSizeX().val() * 1));
                    })
                    ->onClick([](Widget* widget, sf::Vector2i) {
                        struct ExtraData {
                            KFValueColor* valuePtr;
                        } extraData;
                        extraData = *((ExtraData*)widget->getExtraData());
                        extraData.valuePtr->r.set(0);
                        extraData.valuePtr->g.set(255);
                        extraData.valuePtr->b.set(255);
                        extraData.valuePtr->a.set(255);
                        Global::getApp()->refreshProperties();
                        Global::getApp()->setUnsaved();
                    })
                    ->setExtraData(extraData)
            );
        };

        clipProperties->addWidget((new TextWidget("Position: " + Util::secondsToString(project->frameToSeconds(activeClipPtr->getTime())), *(new WidgetRect(0, height * line, 0, height))->setRelSizeX(1)))->setAlignment(TEXT_ALIGNMENT_LEFT));
        clipProperties->addWidget((new ButtonWidget(">", WidgetRect(0, height * line + 5, height, height)))
            ->onParentResize([](Widget* widget, sf::Vector2f size) { WidgetRect* rect = widget->getRect(); rect->setAbsPositionX(size.x - (rect->getSizeX().val() * 1)); })
            ->onClick( [](Widget*, sf::Vector2i) {
                Global::getApp()->moveActiveClip(Global::getDelta(Global::getApp()->project->getFramerate()));
            })
        );
        clipProperties->addWidget((new ButtonWidget("<", WidgetRect(0, height * line + 5, height, height)))
            ->onParentResize([](Widget* widget, sf::Vector2f size) { WidgetRect* rect = widget->getRect(); rect->setAbsPositionX(size.x - (rect->getSizeX().val() * 2)); })
            ->onClick( [](Widget*, sf::Vector2i) {
                Global::getApp()->moveActiveClip(Global::getDelta(-Global::getApp()->project->getFramerate()));
            })
        );
        line++;

        clipProperties->addWidget((new TextWidget("Length: " + Util::secondsToString(project->frameToSeconds(activeClipPtr->getLength())), *(new WidgetRect(0, height * line, 0, height))->setRelSizeX(1)))->setAlignment(TEXT_ALIGNMENT_LEFT));
        clipProperties->addWidget((new ButtonWidget(">", WidgetRect(0, height * line + 5, height, height)))
            ->onParentResize([](Widget* widget, sf::Vector2f size) { WidgetRect* rect = widget->getRect(); rect->setAbsPositionX(size.x - (rect->getSizeX().val() * 1)); })
            ->onClick( [](Widget*, sf::Vector2i) {
                Global::getApp()->resizeActiveClip(Global::getApp()->project->getFramerate());
            })
        );
        clipProperties->addWidget((new ButtonWidget("<", WidgetRect(0, height * line + 5, height, height)))
            ->onParentResize([](Widget* widget, sf::Vector2f size) { WidgetRect* rect = widget->getRect(); rect->setAbsPositionX(size.x - (rect->getSizeX().val() * 2)); })
            ->onClick( [](Widget*, sf::Vector2i) {
                Global::getApp()->resizeActiveClip(-Global::getApp()->project->getFramerate());
            })
        );
        line++;

        clipProperties->addWidget((new TextWidget("Trim: " + Util::secondsToString(project->frameToSeconds(activeClipPtr->getTrimPosition())), *(new WidgetRect(0, height * line, 0, height))->setRelSizeX(1)))->setAlignment(TEXT_ALIGNMENT_LEFT));
        clipProperties->addWidget((new ButtonWidget(">", WidgetRect(0, height * line + 5, height, height)))
            ->onParentResize([](Widget* widget, sf::Vector2f size) { WidgetRect* rect = widget->getRect(); rect->setAbsPositionX(size.x - (rect->getSizeX().val() * 1)); })
            ->onClick( [](Widget*, sf::Vector2i) {
                Global::getApp()->trimActiveClip(Global::getApp()->project->getFramerate());
            })
        );
        clipProperties->addWidget((new ButtonWidget("<", WidgetRect(0, height * line + 5, height, height)))
            ->onParentResize([](Widget* widget, sf::Vector2f size) { WidgetRect* rect = widget->getRect(); rect->setAbsPositionX(size.x - (rect->getSizeX().val() * 2)); })
            ->onClick( [](Widget*, sf::Vector2i) {
                Global::getApp()->trimActiveClip(-Global::getApp()->project->getFramerate());
            })
        );
        line++;

        clipProperties->addWidget((new TextWidget("Thumbnail: ", *(new WidgetRect(0, height * line, 0, height))->setRelSizeX(1)))->setAlignment(TEXT_ALIGNMENT_LEFT));
        clipProperties->addWidget((new ImageWidget(WidgetRect(150, height * line++ + 5, height, height)))->setTexture(activeClipPtr->getThumbnail()));
        clipProperties->addWidget((new TextWidget("Internal ID: " + std::to_string(activeClip), *(new WidgetRect(0, height * line++, 0, height))->setRelSizeX(1)))->setAlignment(TEXT_ALIGNMENT_LEFT));

        if (activeClipPtr->getClipType() == CLIP_TEXT) {
            TextClip* textClip = (TextClip*)activeClipPtr;

            clipProperties->addWidget((new TextWidget("Position X: " + std::to_string((int)textClip->getPosition()->x.get()), *(new WidgetRect(0, height * line, 0, height))->setRelSizeX(1)))->setAlignment(TEXT_ALIGNMENT_LEFT));
            editFloatButton(clipProperties, &(textClip->getPosition()->x));
            line++;

            clipProperties->addWidget((new TextWidget("Position Y: " + std::to_string((int)textClip->getPosition()->y.get()), *(new WidgetRect(0, height * line, 0, height))->setRelSizeX(1)))->setAlignment(TEXT_ALIGNMENT_LEFT));
            editFloatButton(clipProperties, &(textClip->getPosition()->y));
            line++;

            clipProperties->addWidget((new TextWidget("Origin X: " + std::to_string((int)textClip->getOrigin()->x.get()), *(new WidgetRect(0, height * line, 0, height))->setRelSizeX(1)))->setAlignment(TEXT_ALIGNMENT_LEFT));
            editFloatButton(clipProperties, &(textClip->getOrigin()->x));
            line++;

            clipProperties->addWidget((new TextWidget("Origin Y: " + std::to_string((int)textClip->getOrigin()->y.get()), *(new WidgetRect(0, height * line, 0, height))->setRelSizeX(1)))->setAlignment(TEXT_ALIGNMENT_LEFT));
            editFloatButton(clipProperties, &(textClip->getOrigin()->y));
            line++;

            {
                std::stringstream ss;
                ss << "Scale X: " << std::fixed << std::setprecision(2) << textClip->getScale()->x.get();
                clipProperties->addWidget((new TextWidget((ss).str(), *(new WidgetRect(0, height * line, 0, height))->setRelSizeX(1)))->setAlignment(TEXT_ALIGNMENT_LEFT));
                editFloatButton(clipProperties, &(textClip->getScale()->x), 10, 100);
                line++;
            }

            {
                std::stringstream ss;
                ss << "Scale Y: " << std::fixed << std::setprecision(2) << textClip->getScale()->y.get();
                clipProperties->addWidget((new TextWidget((ss).str(), *(new WidgetRect(0, height * line, 0, height))->setRelSizeX(1)))->setAlignment(TEXT_ALIGNMENT_LEFT));
                editFloatButton(clipProperties, &(textClip->getScale()->y), 10, 100);
                line++;
            }

            clipProperties->addWidget((new TextWidget("Rotation: " + std::to_string((int)textClip->getRotation()->get()), *(new WidgetRect(0, height * line, 0, height))->setRelSizeX(1)))->setAlignment(TEXT_ALIGNMENT_LEFT));
            editFloatButton(clipProperties, textClip->getRotation());
            line++;

            clipProperties->addWidget((new TextWidget("String: " + textClip->getText()->get(), *(new WidgetRect(0, height * line, 0, height))->setRelSizeX(1)))->setAlignment(TEXT_ALIGNMENT_LEFT));
            editStringButton(clipProperties, textClip->getText());
            line++;

            clipProperties->addWidget((new TextWidget("Font Size: " + std::to_string(textClip->getFontSize()->get()), *(new WidgetRect(0, height * line, 0, height))->setRelSizeX(1)))->setAlignment(TEXT_ALIGNMENT_LEFT));
            editIntButton(clipProperties, textClip->getFontSize());
            line++;

            clipProperties->addWidget((new TextWidget("Color: ", *(new WidgetRect(0, height * line, 0, height))->setRelSizeX(1)))->setAlignment(TEXT_ALIGNMENT_LEFT));
            clipProperties->addWidget((new ColorWidget(WidgetRect(150, height * line + 5, height, height)))->setColor(textClip->getColor()->get()));
            editColorButton(clipProperties, textClip->getColor());
            line++;

            {
                std::stringstream ss;
                ss << "Outline Thickness: " << std::fixed << std::setprecision(1) << textClip->getOutline()->get();
                clipProperties->addWidget((new TextWidget((ss).str(), *(new WidgetRect(0, height * line, 0, height))->setRelSizeX(1)))->setAlignment(TEXT_ALIGNMENT_LEFT));
                editFloatButton(clipProperties, textClip->getOutline(), 10, 10);
                line++;
            }

            clipProperties->addWidget((new TextWidget("Outline Color: ", *(new WidgetRect(0, height * line, 0, height))->setRelSizeX(1)))->setAlignment(TEXT_ALIGNMENT_LEFT));
            clipProperties->addWidget((new ColorWidget(WidgetRect(150, height * line + 5, height, height)))->setColor(textClip->getOutlineColor()->get()));
            editColorButton(clipProperties, textClip->getOutlineColor());
            line++;

            clipProperties->addWidget((new TextWidget("Style: ", *(new WidgetRect(0, height * line, 0, height))->setRelSizeX(1)))->setAlignment(TEXT_ALIGNMENT_LEFT));
            clipProperties->addWidget((new ButtonWidget("S", WidgetRect(0, height * line + 5, height, height)))
                ->setColor((textClip->getStyle()->get() & sf::Text::StrikeThrough) ? sf::Color(42, 127, 42) : sf::Color(42, 42, 42))
                ->onParentResize([](Widget* widget, sf::Vector2f size) { WidgetRect* rect = widget->getRect(); rect->setAbsPositionX(size.x - (rect->getSizeX().val() * 1)); })
                ->onClick( [](Widget*, sf::Vector2i) {
                    auto style = ((TextClip*)Global::getApp()->getActiveClipPtr())->getStyle();
                    style->set(style->get() ^ sf::Text::StrikeThrough);
                    
                    Global::getApp()->refreshProperties(); Global::getApp()->setUnsaved();
                })
            );
            clipProperties->addWidget((new ButtonWidget("U", WidgetRect(0, height * line + 5, height, height)))
                ->setColor((textClip->getStyle()->get() & sf::Text::Underlined) ? sf::Color(42, 127, 42) : sf::Color(42, 42, 42))
                ->onParentResize([](Widget* widget, sf::Vector2f size) { WidgetRect* rect = widget->getRect(); rect->setAbsPositionX(size.x - (rect->getSizeX().val() * 2)); })
                ->onClick( [](Widget*, sf::Vector2i) {
                    auto style = ((TextClip*)Global::getApp()->getActiveClipPtr())->getStyle();
                    style->set(style->get() ^ sf::Text::Underlined);

                    Global::getApp()->refreshProperties(); Global::getApp()->setUnsaved();
                })
            );
            clipProperties->addWidget((new ButtonWidget("I", WidgetRect(0, height * line + 5, height, height)))
                ->setColor((textClip->getStyle()->get() & sf::Text::Italic) ? sf::Color(42, 127, 42) : sf::Color(42, 42, 42))
                ->onParentResize([](Widget* widget, sf::Vector2f size) { WidgetRect* rect = widget->getRect(); rect->setAbsPositionX(size.x - (rect->getSizeX().val() * 3)); })
                ->onClick( [](Widget*, sf::Vector2i) {
                    auto style = ((TextClip*)Global::getApp()->getActiveClipPtr())->getStyle();
                    style->set(style->get() ^ sf::Text::Italic);

                    Global::getApp()->refreshProperties(); Global::getApp()->setUnsaved();
                })
            );
            clipProperties->addWidget((new ButtonWidget("B", WidgetRect(0, height * line + 5, height, height)))
                ->setColor((textClip->getStyle()->get() & sf::Text::Bold) ? sf::Color(42, 127, 42) : sf::Color(42, 42, 42))
                ->onParentResize([](Widget* widget, sf::Vector2f size) { WidgetRect* rect = widget->getRect(); rect->setAbsPositionX(size.x - (rect->getSizeX().val() * 4)); })
                ->onClick( [](Widget*, sf::Vector2i) {
                    auto style = ((TextClip*)Global::getApp()->getActiveClipPtr())->getStyle();
                    style->set(style->get() ^ sf::Text::Bold);

                    Global::getApp()->refreshProperties(); Global::getApp()->setUnsaved();
                })
            );
            line++;

            {
                std::stringstream ss;
                ss << "Line Spacing: " << std::fixed << std::setprecision(2) << textClip->getLineSpacing()->get();
                clipProperties->addWidget((new TextWidget((ss).str(), *(new WidgetRect(0, height * line, 0, height))->setRelSizeX(1)))->setAlignment(TEXT_ALIGNMENT_LEFT));
                editFloatButton(clipProperties, textClip->getLineSpacing(), 5, 100);
                line++;
            }

            {
                std::stringstream ss;
                ss << "Letter Spacing: " << std::fixed << std::setprecision(2) << textClip->getLetterSpacing()->get();
                clipProperties->addWidget((new TextWidget((ss).str(), *(new WidgetRect(0, height * line, 0, height))->setRelSizeX(1)))->setAlignment(TEXT_ALIGNMENT_LEFT));
                editFloatButton(clipProperties, textClip->getLetterSpacing(), 5, 100);
                line++;
            }

        }

    }

    propertiesWindow->setRect(*propertiesWindow->getRect());
    propertiesWindow->mouseMove(sf::Mouse::getPosition(window));
}


void Application::showProjectProperties() {
    projectProperties->setVisible(true);
    resourceProperties->setVisible(false);
    trackProperties->setVisible(false);
    clipProperties->setVisible(false);
}

void Application::showResourceProperties() {
    projectProperties->setVisible(false);
    resourceProperties->setVisible(true);
    trackProperties->setVisible(false);
    clipProperties->setVisible(false);
}

void Application::showTrackProperties() {
    projectProperties->setVisible(false);
    resourceProperties->setVisible(false);
    trackProperties->setVisible(true);
    clipProperties->setVisible(false);
}

void Application::showClipProperties() {
    projectProperties->setVisible(false);
    resourceProperties->setVisible(false);
    trackProperties->setVisible(false);
    clipProperties->setVisible(true);
}



void Application::toggleFileMenu() { toggleFileMenu(!isFileMenuOpen()); }
void Application::toggleFileMenu(bool open) {
    fileMenu->setVisible(open);
    editMenu->setVisible(false);
    clipMenu->setVisible(false);
}
bool Application::isFileMenuOpen() { return fileMenu->isVisible(); }

void Application::toggleEditMenu() { toggleEditMenu(!isEditMenuOpen()); }
void Application::toggleEditMenu(bool open) {
    fileMenu->setVisible(false);
    editMenu->setVisible(open);
    clipMenu->setVisible(false);
}
bool Application::isEditMenuOpen() { return editMenu->isVisible(); }

void Application::toggleClipMenu() { toggleClipMenu(!isClipMenuOpen()); }
void Application::toggleClipMenu(bool open) {
    fileMenu->setVisible(false);
    editMenu->setVisible(false);
    clipMenu->setVisible(open);
}
bool Application::isClipMenuOpen() { return clipMenu->isVisible(); }



void Application::showAboutMessageBox() { this->aboutMessageBox->setVisible(true); }
void Application::showLoadResourceMessageBox() { this->loadResourceMessageBox->setVisible(true); }
void Application::showRenameResourceMessageBox(int resourceId) {
    this->renameResourceMessageBox->setExtraData(new int(resourceId));
    this->renameResourceMessageBox->setVisible(true);
}
void Application::showCreateTextClipMessageBox() { this->createTextClipMessageBox->setVisible(true); }
void Application::showLoadProjectMessageBox() { this->loadProjectMessageBox->setVisible(true); }
void Application::showNewProjectMessageBox() { this->newProjectMessageBox->setVisible(true); }
void Application::showChangeTextClipMessageBox() { this->changeTextClipMessageBox->setVisible(true); }



void Application::start() {;
    std::cout << "Application start\n";

    unsaved = true;
    isDefaultTempDir = std::filesystem::exists(std::filesystem::temp_directory_path());

    std::string projectName = "project";
    int projectNumber = 0;
    while (std::filesystem::exists(std::filesystem::path("projects") / projectName)) {
        projectNumber++;
        projectName = "project" + std::to_string(projectNumber);
    }

    project = (new Project())->setFramerate(30)->setWidth(854)->setHeight(480)->setLength(300)->setName(projectName);
    project->getTimeline()->addTrack((new Track())->setName("Track 1")->setColor(sf::Color(0, 80, 100)));
    project->getTimeline()->addTrack((new Track())->setName("Track 2")->setColor(sf::Color(0, 100, 0)));

    window.create(sf::VideoMode(1280, 720), "Linux Movie Maker", sf::Style::Default);
    window.setFramerateLimit(Settings::getSetting_ulong("windowFramerateLimit"));

    previewPopupWindow = nullptr;
    previewPopupWindowFullscreen = false;

    resourceManager = new ResourceManager();

    playing = false;
    rendering = false;
    converting = false;

    selectedTextField = nullptr;

    std::cout << "Initializing widgets...\n";

    // Initialize widgets
    rootWidget = new RootWidget(&window);

    // Define main containers
    topBar = (new WidgetContainer(*(new WidgetRect(0, 0, 0, 20))->setRelSizeX(1)));
    topBar->setColor(sf::Color(42, 42, 42));
    rootWidget->addWidget(topBar);
    mainWindow = (new WidgetContainer(*(new WidgetRect(0, 20, 0, 200))->setRelSizeX(1)));
    mainWindow->onParentResize(
            [](Widget* widget, sf::Vector2f size) {
                widget->setRect(*(new WidgetRect(*widget->getRect()))->setAbsSizeY(size.y - 20));
            }
        );
    rootWidget->addWidget(mainWindow);

    // ======== MAIN WINDOW ========

    // ======== MAIN WINDOW : Resource Window ========

    resourceWindow = (new WidgetWindow("Resources", *(new WidgetRect())->setRelPosition(0, 0)->setRelSize(0.299, 0.7)))
        ->addWidget(
            (new ButtonWidget("Refresh Resources", WidgetRect(0, 0, 200, 30)))
            ->setColor(sf::Color(128, 42, 128))
            ->onClick(
                [](Widget*, sf::Vector2i) {
                    Global::getApp()->refreshResourcePanel();
                }
            )
            ->onParentResize(
                [](Widget* widget, sf::Vector2f size) {
                    WidgetRect* rect = widget->getRect();

                    if (size.x < 400) rect->setAbsSizeX(size.x / 2);
                    else rect->setAbsSizeX(200);
                    rect->setAbsPositionX(rect->getSizeX().val() * 0);
                    rect->setAbsPositionY(size.y - rect->getSizeY().val());
                }
            ))
        ->addWidget(
            (new ButtonWidget("Load Resource...", WidgetRect(0, 0, 200, 30)))
            ->setColor(sf::Color(42, 42, 128))
            ->onClick(
                [](Widget*, sf::Vector2i) {
                    Global::getApp()->showLoadResourceMessageBox();
                }
            )
            ->onParentResize(
                [](Widget* widget, sf::Vector2f size) {
                    WidgetRect* rect = widget->getRect();

                    if (size.x < 400) rect->setAbsSizeX(size.x / 2);
                    else rect->setAbsSizeX(200);
                    rect->setAbsPositionX(rect->getSizeX().val() * 1);
                    rect->setAbsPositionY(size.y - rect->getSizeY().val());
                }
            ));

    resourcePanel = new ScrollWidgetContainer(*(new WidgetRect())->setRelPosition(0, 0.2)->setRelSize(1, 0.7)); // Contains all loaded resources
    resourceWindow->addWidget(resourcePanel);

    mainWindow->addWidget(resourceWindow);

    // ======== MAIN WINDOW : Properties Window ========

    propertiesWindow = (new WidgetWindow("Properties", *(new WidgetRect())->setRelPosition(0.3, 0)->setRelSize(0.299, 0.7)));

    projectProperties = (new ScrollWidgetContainer(*(new WidgetRect())->setRelPosition(0.05, 0.1)->setRelSize(0.9, 0.8)));
    projectProperties->setVisible(false);
    propertiesWindow->addWidget(projectProperties);

    trackProperties = (new ScrollWidgetContainer(*(new WidgetRect())->setRelPosition(0.05, 0.1)->setRelSize(0.9, 0.8)));
    trackProperties->setVisible(false);
    propertiesWindow->addWidget(trackProperties);

    resourceProperties = (new ScrollWidgetContainer(*(new WidgetRect())->setRelPosition(0.05, 0.1)->setRelSize(0.9, 0.8)));
    resourceProperties->setVisible(false);
    propertiesWindow->addWidget(resourceProperties);

    clipProperties = (new ScrollWidgetContainer(*(new WidgetRect())->setRelPosition(0.05, 0.1)->setRelSize(0.9, 0.8)));
    clipProperties->setVisible(false);
    propertiesWindow->addWidget(clipProperties);


    propertiesWindow->addWidget(
        (new ButtonWidget("Project Info", WidgetRect(0, 0, 200, 30)))
        ->setColor(sf::Color(128, 42, 42))
        ->onParentResize(
            [](Widget* widget, sf::Vector2f size) {
                WidgetRect* rect = widget->getRect();

                if (size.x < 800) rect->setAbsSizeX(size.x / 4);
                else rect->setAbsSizeX(200);
                rect->setAbsPositionX(rect->getSizeX().val() * 0);
                rect->setAbsPositionY(size.y - rect->getSizeY().val());
            }
        )
        ->onClick(
            [](Widget*, sf::Vector2i) {
                Global::getApp()->showProjectProperties();
            }
        ));
    propertiesWindow->addWidget(
        (new ButtonWidget("Track Info", WidgetRect(0, 0, 200, 30)))
        ->setColor(sf::Color(42, 128, 42))
        ->onParentResize(
            [](Widget* widget, sf::Vector2f size) {
                WidgetRect* rect = widget->getRect();

                if (size.x < 800) rect->setAbsSizeX(size.x / 4);
                else rect->setAbsSizeX(200);
                rect->setAbsPositionX(rect->getSizeX().val() * 1);
                rect->setAbsPositionY(size.y - rect->getSizeY().val());
            }
        )
        ->onClick(
            [](Widget*, sf::Vector2i) {
                Global::getApp()->showTrackProperties();
            }
        ));
    propertiesWindow->addWidget(
        (new ButtonWidget("Resource Info", WidgetRect(0, 0, 200, 30)))
        ->setColor(sf::Color(128, 128, 42))
        ->onParentResize(
            [](Widget* widget, sf::Vector2f size) {
                WidgetRect* rect = widget->getRect();

                if (size.x < 800) rect->setAbsSizeX(size.x / 4);
                else rect->setAbsSizeX(200);
                rect->setAbsPositionX(rect->getSizeX().val() * 2);
                rect->setAbsPositionY(size.y - rect->getSizeY().val());
            }
        )
        ->onClick(
            [](Widget*, sf::Vector2i) {
                Global::getApp()->showResourceProperties();
            }
        ));
    propertiesWindow->addWidget(
        (new ButtonWidget("Clip Info", WidgetRect(0, 0, 200, 30)))
        ->setColor(sf::Color(42, 128, 128))
        ->onParentResize(
            [](Widget* widget, sf::Vector2f size) {
                WidgetRect* rect = widget->getRect();

                if (size.x < 800) rect->setAbsSizeX(size.x / 4);
                else rect->setAbsSizeX(200);
                rect->setAbsPositionX(rect->getSizeX().val() * 3);
                rect->setAbsPositionY(size.y - rect->getSizeY().val());
            }
        )
        ->onClick(
            [](Widget*, sf::Vector2i) {
                Global::getApp()->showClipProperties();
            }
        ));
    
    mainWindow->addWidget(propertiesWindow);

    // ======== MAIN WINDOW : Preview Window ========

    previewWindow = (new WidgetWindow("Preview", *(new WidgetRect())->setRelPosition(0.6, 0)->setRelSize(0.4, 0.7)));

    previewContainer = (new PreviewWidget(*(new WidgetRect())->setRelPosition(0, 0.1)->setRelSize(1, 0.8)));
    previewWindow->addWidget(previewContainer);

    previewInfo = (new TextWidget("", *(new WidgetRect())->setRelPosition(0, 0)->setRelSize(0.95, 0.1)))
        ->setAlignment(TEXT_ALIGNMENT_RIGHT)
        ->setCharacterSize(20)
        ->setColor(sf::Color(255, 255, 255, 127));
    previewWindow->addWidget(previewInfo);

    previewWindow->addWidget((new ButtonWidget("Open in a separate window", *(new WidgetRect(0, 0, 200, 30))))
        ->onParentResize(
            [](Widget* widget, sf::Vector2f size) {
                WidgetRect* rect = widget->getRect();
                rect->setAbsPositionY(size.y - rect->getSizeY().val());
            }
        )
        ->onClick(
            [](Widget*, sf::Vector2i) {
                Global::getApp()->openPreviewPopupWindow();
            }
        ));
    
    mainWindow->addWidget(previewWindow);

    // ======== MAIN WINDOW : Timeline Window ========

    timeline = (new HorizontalScrollWidgetContainer(*(new WidgetRect(0, 0, 0, 0))->setRelPositionX(0.1)->setRelSize(0.9, 1)));
    timeline->setScrollSensitivity(60);
    timeline->onClick(
        [](Widget* widget, sf::Vector2i mousePos) {
            if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
                Global::getApp()->project->getTimeline()->setCurrentPosition(widget->absToRel(mousePos).x);
                Global::getApp()->updateCursor();
            }
        }
    );
    timeline->onMouseMove(
        [](Widget* widget, sf::Vector2i mousePos) {
            if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
                Global::getApp()->project->getTimeline()->setCurrentPosition(widget->absToRel(mousePos).x);
                Global::getApp()->updateCursor();
            }
        }
    );

    timelineTracks = (new WidgetContainer(*(new WidgetRect(0, 0, 0, 0))->setRelSize(0.1, 1)));

    timelineCursor = nullptr;

    timelineWindow = (new WidgetWindow("Timeline", *(new WidgetRect())->setRelPosition(0, 0.7)->setRelSize(1, 0.3)));
    timelineWindow->setColor(sf::Color(31, 31, 31, 127));
    timelineWindow->onClick(
        [](Widget*, sf::Vector2i) {
            Global::getApp()->setActiveClip(-1, -1);
        }
    );
    timelineWindow->addWidget(timeline);
    timelineWindow->addWidget(timelineTracks);
    timelineWindow->addWidget((new ButtonWidget("+", *(new WidgetRect(0, 0, 25, 25))))
        ->onParentResize(
            [](Widget* widget, sf::Vector2f size) {
                widget->getRect()->setPositionX(size.x - 25);
            }
        )
        ->onClick(
            [](Widget*, sf::Vector2i) {
                Project* projectPtr = Global::getApp()->project;
                if (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift)
                 || sf::Keyboard::isKeyPressed(sf::Keyboard::RShift)) {
                    projectPtr->setLength(projectPtr->getLength() + 1);
                } else if (sf::Keyboard::isKeyPressed(sf::Keyboard::LControl)
                        || sf::Keyboard::isKeyPressed(sf::Keyboard::RControl)) {
                    projectPtr->setLength(projectPtr->getLength() + 600);
                } else {
                    projectPtr->setLength(projectPtr->getLength() + 60);
                }
                Global::getApp()->redrawAllTracks();
            }
        ));
    timelineWindow->addWidget((new ButtonWidget("-", *(new WidgetRect(0, 0, 25, 25))))
        ->onParentResize(
            [](Widget* widget, sf::Vector2f size) {
                widget->getRect()->setPositionX(size.x - 50);
            }
        )
        ->onClick(
            [](Widget*, sf::Vector2i) {
                Project* projectPtr = Global::getApp()->project;
                try {
                    if (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift)
                     || sf::Keyboard::isKeyPressed(sf::Keyboard::RShift)) {
                        projectPtr->setLength(projectPtr->getLength() - 1);
                    } else if (sf::Keyboard::isKeyPressed(sf::Keyboard::LControl)
                            || sf::Keyboard::isKeyPressed(sf::Keyboard::RControl)) {
                        projectPtr->setLength(projectPtr->getLength() - 600);
                    } else {
                        projectPtr->setLength(projectPtr->getLength() - 60);
                    }
                } catch (Exception* e) {
                    if (e->getCode() == EXCEPTION_INVALID_VALUE) {
                        projectPtr->setLength(1);
                    } else {
                        throw e;
                    }
                }
                Global::getApp()->redrawAllTracks();
            }
        ));
    timelineWindow->addWidget((new ButtonWidget(">", *(new WidgetRect(0, 0, 25, 25))))
        ->onParentResize(
            [](Widget* widget, sf::Vector2f size) {
                widget->getRect()->setPositionX(size.x - 75);
            }
        )
        ->onClick(
            [](Widget*, sf::Vector2i) {
                Project* project = Global::getApp()->project;
                int delta = project->getFramerate();
                if (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift) || sf::Keyboard::isKeyPressed(sf::Keyboard::RShift)) delta = 1;
                delta *= 1 + ((sf::Keyboard::isKeyPressed(sf::Keyboard::LControl) || sf::Keyboard::isKeyPressed(sf::Keyboard::RControl)) * 9);
                Global::getApp()->moveCursor(delta);
            }
        ));
    timelineWindow->addWidget((new ButtonWidget("<", *(new WidgetRect(0, 0, 25, 25))))
        ->onParentResize(
            [](Widget* widget, sf::Vector2f size) {
                widget->getRect()->setPositionX(size.x - 100);
            }
        )
        ->onClick(
            [](Widget*, sf::Vector2i) {
                Project* project = Global::getApp()->project;
                int delta = -project->getFramerate();
                if (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift) || sf::Keyboard::isKeyPressed(sf::Keyboard::RShift)) delta = -1;
                delta *= 1 + ((sf::Keyboard::isKeyPressed(sf::Keyboard::LControl) || sf::Keyboard::isKeyPressed(sf::Keyboard::RControl)) * 9);
                Global::getApp()->moveCursor(delta);
            }
        ));

    mainWindow->addWidget(timelineWindow);

    // ======== MAIN WINDOW END ========


    // ======== TOP BAR ========

    // ======== TOP BAR : Buttons ========

    topBar->addWidget(
        (new ButtonWidget("File", WidgetRect(0, 0, 100, 20)))
        ->onClick(
            [](Widget*, sf::Vector2i) {
                Global::getApp()->toggleFileMenu();
            }
        ));

    topBar->addWidget(
        (new ButtonWidget("Edit", WidgetRect(100, 0, 100, 20)))
        ->onClick(
            [](Widget*, sf::Vector2i) {
                Global::getApp()->toggleEditMenu();
            }
        ));

    topBar->addWidget(
        (new ButtonWidget("Clip", WidgetRect(200, 0, 100, 20)))
        ->onClick(
            [](Widget*, sf::Vector2i) {
                Global::getApp()->toggleClipMenu();
            }
        ));

    topBar->addWidget(
        (new ButtonWidget("About", WidgetRect(300, 0, 100, 20)))
        ->onClick(
            [](Widget*, sf::Vector2i) {
                Global::getApp()->showAboutMessageBox();
            }
        ));

    topBar->addWidget(
        (new ButtonWidget("Render", WidgetRect(400, 0, 100, 20)))
        ->onClick(
            [](Widget*, sf::Vector2i) {
                Global::getApp()->startRendering();
            }
        ));

    // ======== TOP BAR : File Menu ========

    fileMenu = (new WidgetContainer(*(new WidgetRect(0, 0, 100, 4 * 20 + 20))))
        ->addWidget(
            (new ButtonWidget("New", *(new WidgetRect(0, 1 * 20, 100, 20))))
            ->onClick(
                [](Widget*, sf::Vector2i) {
                    Global::getApp()->toggleFileMenu(false);
                    Global::getApp()->showNewProjectMessageBox();
                }
            ))
        ->addWidget(
            (new ButtonWidget("Load", *(new WidgetRect(0, 2 * 20, 100, 20))))
            ->onClick(
                [](Widget*, sf::Vector2i) {
                    Global::getApp()->toggleFileMenu(false);
                    Global::getApp()->load();
                }
            ))
        ->addWidget(
            (new ButtonWidget("Save", *(new WidgetRect(0, 3 * 20, 100, 20))))
            ->onClick(
                [](Widget*, sf::Vector2i) {
                    Global::getApp()->toggleFileMenu(false);
                    Global::getApp()->save();
                }
            ))
        ->addWidget(
            (new ButtonWidget("Exit", *(new WidgetRect(0, 4 * 20, 100, 20))))
            ->onClick(
                [](Widget*, sf::Vector2i) {
                    Global::getApp()->toggleFileMenu(false);
                    Global::getApp()->window.close();
                }
            ))
        ->onUnhover(
            [](Widget*) {
                Global::getApp()->toggleFileMenu(false);
            }
        )
        ->setVisible(false);
    rootWidget->addWidget(fileMenu);

    // ======== TOP BAR : Edit Menu ========

    editMenu = (new WidgetContainer(*(new WidgetRect(100, 0, 100, 2 * 20 + 20))))
        ->addWidget(
            (new ButtonWidget("Undo", *(new WidgetRect(0, 1 * 20, 100, 20))))
            ->setTextColor(sf::Color(127, 127, 127))
            ->onClick(
                [](Widget*, sf::Vector2i) {
                    Global::getApp()->toggleEditMenu(false);
                }
            ))
        ->addWidget(
            (new ButtonWidget("Redo", *(new WidgetRect(0, 2 * 20, 100, 20))))
            ->setTextColor(sf::Color(127, 127, 127))
            ->onClick(
                [](Widget*, sf::Vector2i) {
                    Global::getApp()->toggleEditMenu(false);
                }
            ))
        ->onUnhover(
            [](Widget*) {
                Global::getApp()->toggleEditMenu(false);
            }
        )
        ->setVisible(false);
    rootWidget->addWidget(editMenu);

    // ======== TOP BAR : Clip Menu ========

    clipMenu = (new WidgetContainer(*(new WidgetRect(200, 0, 100, 1 * 20 + 20))))
        ->addWidget(
            (new ButtonWidget("Add Text Clip...", *(new WidgetRect(0, 1 * 20, 100, 20))))
            ->onClick(
                [](Widget*, sf::Vector2i) {
                    Global::getApp()->toggleEditMenu(false);
                    Global::getApp()->showCreateTextClipMessageBox();
                }
            ))
        ->onUnhover(
            [](Widget*) {
                Global::getApp()->toggleEditMenu(false);
            }
        )
        ->setVisible(false);
    rootWidget->addWidget(clipMenu);

    // ======== TOP BAR END ========


    // ======== ALERTS  ========

    aboutMessageBox = (new WidgetWindow("Linux Movie Maker", *(new WidgetRect())->setRelPosition(0.25, 0.25)->setRelSize(0.5, 0.5)))
        ->setColor(sf::Color(42, 42, 42, 191))
        ->addWidget(
            (new TextWidget("", *(new WidgetRect())->setRelPosition(0.05, 0.15)->setRelSize(0.9, 0.7)))
            ->setString("This is an early version of a video\neditor made in SFML, there can still be bugs!\nMade by TadeLn, 2020\nhttps://gitlab.com/TadeLn/linux-movie-maker")
            ->setCharacterSize(20)
        )
        ->addWidget(
            (new ButtonWidget("OK", *(new WidgetRect())->setRelPosition(0.40, 0.85)->setRelSize(0.2, 0.1)))
            ->setColor(sf::Color(42, 127, 42))
            ->onClick(
                [](Widget* widget, sf::Vector2i) {
                    widget->getParent()->setVisible(false);
                }
            )
        );

    #ifdef DEBUG 
        aboutMessageBox->setVisible(false);
    #endif

    rootWidget->addWidget(aboutMessageBox);


    loadResourceMessageBox = (new WidgetWindow("Load Resource", *(new WidgetRect())->setRelPosition(0.25, 0.25)->setRelSize(0.5, 0.5)))
        ->setColor(sf::Color(42, 42, 42, 191))
        ->addWidget(
            (new TextInputWidget("Path", *(new WidgetRect())->setRelPosition(0.3, 0.65)->setRelSize(0.4, 0.1)))
            ->setColor(sf::Color(63, 63, 63))
            ->getWidget()
        )
        ->addWidget(
            (new TextWidget("Input a file name or path to the\nfile in your project's folder:", *(new WidgetRect())->setRelPosition(0.05, 0.15)->setRelSize(0.9, 0.4)))
            ->setCharacterSize(20)
        )
        ->addWidget(
            (new ButtonWidget("Load", *(new WidgetRect())->setRelPosition(0.40, 0.85)->setRelSize(0.2, 0.1)))
            ->setColor(sf::Color(42, 127, 42))
            ->onClick(
                [](Widget* widget, sf::Vector2i) {
                    widget->getParent()->setVisible(false);

                    try {
                        TextInputWidget* input = ((TextInputWidget*)widget->getParent()->getChildren()->at(0));
                        std::string filename = input->getInput();
                        Global::getApp()->resourceManager->loadResourceFromFile(
                            Global::getApp()->getProjectDir() / filename,
                            filename
                        );
                        Global::getApp()->refreshResourcePanel();
                        input->setInput("");
                    } catch (Exception* e) {
                        e->print();
                    }
                }
            )
        )
        ->addWidget(
            (new ButtonWidget("Load an image", *(new WidgetRect())->setRelPosition(0.75, 0.85)->setRelSize(0.2, 0.1)))
            ->setColor(sf::Color(127, 42, 127))
            ->onClick(
                [](Widget* widget, sf::Vector2i) {
                    widget->getParent()->setVisible(false);

                    try {
                        TextInputWidget* input = ((TextInputWidget*)widget->getParent()->getChildren()->at(0));
                        std::string filename = input->getInput();
                        Global::getApp()->resourceManager->loadResource(
                            new ImageResource(Global::getApp()->getProjectDir() / filename, filename)
                        );
                        Global::getApp()->refreshResourcePanel();
                        input->setInput("");
                    } catch (Exception* e) {
                        e->print();
                    }
                }
            )
        )
        ->addWidget(
            (new ButtonWidget("X", *(new WidgetRect())->setRelPosition(0.9, 0)->setRelSize(0.1, 0.1)))
            ->setColor(sf::Color(127, 42, 42))
            ->onClick(
                [](Widget* widget, sf::Vector2i) {
                    widget->getParent()->setVisible(false);
                }
            )
        );
    loadResourceMessageBox->setVisible(false);

    rootWidget->addWidget(loadResourceMessageBox);


    renameResourceMessageBox = (new WidgetContainer(*(new WidgetRect())->setRelPosition(0, 0)->setRelSize(1, 1)))
        ->setColor(sf::Color(42, 42, 42, 127))
        ->addWidget(
            (new WidgetWindow("Rename Resource", *(new WidgetRect())->setRelPosition(0.25, 0.25)->setRelSize(0.5, 0.5)))
            ->setColor(sf::Color(42, 42, 42, 191))
            ->addWidget(
                (new TextInputWidget("Name", *(new WidgetRect())->setRelPosition(0.3, 0.65)->setRelSize(0.4, 0.1)))
                ->setColor(sf::Color(63, 63, 63))
                ->getWidget()
            )
            ->addWidget(
                (new TextWidget("Input a new name for the resource:", *(new WidgetRect())->setRelPosition(0.05, 0.15)->setRelSize(0.9, 0.4)))
                ->setCharacterSize(20)
            )
            ->addWidget(
                (new ButtonWidget("Rename", *(new WidgetRect())->setRelPosition(0.40, 0.85)->setRelSize(0.2, 0.1)))
                ->setColor(sf::Color(42, 127, 42))
                ->onClick(
                    [](Widget* widget, sf::Vector2i) {
                        widget->getParent()->setVisible(false);

                        try {
                            TextInputWidget* input = ((TextInputWidget*)widget->getParent()->getChildren()->at(0));
                            if (input == nullptr) throw new Exception("Input text field not found", EXCEPTION_CODE_ERROR);
                            std::string name = input->getInput();

                            int* resourceId = (int*)(widget->getParent()->getExtraData());
                            if (resourceId == nullptr) throw new Exception("Resource ID not set", EXCEPTION_CODE_ERROR);

                            Global::getApp()->resourceManager->renameResource(*resourceId, name);
                            Global::getApp()->refreshResourcePanel();
                            Global::getApp()->redrawAllTracks();
                            input->setInput("");
                        } catch (Exception* e) {
                            e->print();
                        }
                    }
                )
            )
            ->addWidget(
                (new ButtonWidget("X", *(new WidgetRect())->setRelPosition(0.9, 0)->setRelSize(0.1, 0.1)))
                ->setColor(sf::Color(127, 42, 42))
                ->onClick(
                    [](Widget* widget, sf::Vector2i) {
                        widget->getParent()->setVisible(false);
                    }
                )
            )
    );
    renameResourceMessageBox->setVisible(false);

    rootWidget->addWidget(renameResourceMessageBox);


    renderingMessageBox = (new WidgetWindow("Rendering", *(new WidgetRect())->setRelPosition(0.25, 0.25)->setRelSize(0.5, 0.5)))
        ->setColor(sf::Color(42, 42, 42, 191))
        ->addWidget(
            (new TextWidget("Rendering...\n [Esc] to cancel.", *(new WidgetRect())->setRelPosition(0.05, 0.15)->setRelSize(0.9, 0.4)))
            ->setCharacterSize(50)
        );
    renderingProgressText = (new TextWidget("", *(new WidgetRect())->setRelPosition(0.05, 0.6)->setRelSize(0.9, 0.2)))
        ->setCharacterSize(50);
    renderingMessageBox->addWidget(renderingProgressText);
    renderingMessageBox->setVisible(false);

    rootWidget->addWidget(renderingMessageBox);


    renderingDoneMessageBox = (new WidgetWindow("Rendering", *(new WidgetRect())->setRelPosition(0.15, 0.25)->setRelSize(0.7, 0.5)))
        ->setColor(sf::Color(42, 42, 42, 191))
        ->addWidget(
            (new ButtonWidget("OK", *(new WidgetRect())->setRelPosition(0.40, 0.85)->setRelSize(0.2, 0.1)))
            ->setColor(sf::Color(42, 127, 42))
            ->onClick(
                [](Widget* widget, sf::Vector2i) {
                    widget->getParent()->setVisible(false);
                }
            )
        );
    renderingDoneText = (new TextWidget("", *(new WidgetRect())->setRelPosition(0.05, 0.15)->setRelSize(0.9, 0.4)))
        ->setCharacterSize(50);
    renderingDoneMessageBox->addWidget(renderingDoneText);
    renderingDoneMessageBox->setVisible(false);

    rootWidget->addWidget(renderingDoneMessageBox);


    createTextClipMessageBox = (new WidgetWindow("Create a Text Clip", *(new WidgetRect())->setRelPosition(0.25, 0.25)->setRelSize(0.5, 0.5)))
        ->setColor(sf::Color(42, 42, 42, 191))
        ->addWidget(
            (new TextInputWidget("Text", *(new WidgetRect())->setRelPosition(0.3, 0.65)->setRelSize(0.4, 0.1)))
            ->setColor(sf::Color(63, 63, 63))
            ->getWidget()
        )
        ->addWidget(
            (new TextWidget("Input text for a new Text Clip:", *(new WidgetRect())->setRelPosition(0.05, 0.15)->setRelSize(0.9, 0.4)))
            ->setCharacterSize(20)
        )
        ->addWidget(
            (new ButtonWidget("Create", *(new WidgetRect())->setRelPosition(0.40, 0.85)->setRelSize(0.2, 0.1)))
            ->setColor(sf::Color(42, 127, 42))
            ->onClick(
                [](Widget* widget, sf::Vector2i) {
                    widget->getParent()->setVisible(false);

                    TextInputWidget* input = ((TextInputWidget*)widget->getParent()->getChildren()->at(0));
                    std::string text = input->getInput();
                    TextClip* clip = new TextClip();
                    Timeline* timeline = Global::getApp()->project->getTimeline();
                    clip->getText()->set(text);
                    clip->setTime(timeline->getCurrentPosition());
                    clip->setLength(Global::getApp()->project->secondsToFrame(5));
                    try {
                        Global::getApp()->addClip(Global::getApp()->getActiveClip().first, clip);
                    } catch (Exception* e) {
                        e->print();
                        delete clip;
                    }
                }
            )
        )
        ->addWidget(
            (new ButtonWidget("X", *(new WidgetRect())->setRelPosition(0.9, 0)->setRelSize(0.1, 0.1)))
            ->setColor(sf::Color(127, 42, 42))
            ->onClick(
                [](Widget* widget, sf::Vector2i) {
                    widget->getParent()->setVisible(false);
                }
            )
        );
    createTextClipMessageBox->setVisible(false);

    rootWidget->addWidget(createTextClipMessageBox);


    loadProjectMessageBox = (new WidgetWindow("Load Project", *(new WidgetRect())->setRelPosition(0.25, 0.25)->setRelSize(0.5, 0.5)))
        ->setColor(sf::Color(42, 42, 42, 191))
        ->addWidget(
            (new TextInputWidget("Name", *(new WidgetRect())->setRelPosition(0.3, 0.65)->setRelSize(0.4, 0.1)))
            ->setColor(sf::Color(63, 63, 63))
            ->getWidget()
        )
        ->addWidget(
            (new TextWidget("Input project name:", *(new WidgetRect())->setRelPosition(0.05, 0.15)->setRelSize(0.9, 0.4)))
            ->setCharacterSize(20)
        )
        ->addWidget(
            (new ButtonWidget("Load", *(new WidgetRect())->setRelPosition(0.40, 0.85)->setRelSize(0.2, 0.1)))
            ->setColor(sf::Color(42, 127, 42))
            ->onClick(
                [](Widget* widget, sf::Vector2i) {
                    widget->getParent()->setVisible(false);
                    
                    TextInputWidget* input = ((TextInputWidget*)widget->getParent()->getChildren()->at(0));
                    std::string name = input->getInput();
                    try {
                        Global::getApp()->project->loadFromFile(std::filesystem::current_path() / "projects" / name / "project.json");
                    } catch (Exception* e) {
                        e->print();
                    }
                }
            )
        )
        ->addWidget(
            (new ButtonWidget("X", *(new WidgetRect())->setRelPosition(0.9, 0)->setRelSize(0.1, 0.1)))
            ->setColor(sf::Color(127, 42, 42))
            ->onClick(
                [](Widget* widget, sf::Vector2i) {
                    widget->getParent()->setVisible(false);
                }
            )
        );
    loadProjectMessageBox->setVisible(false);

    rootWidget->addWidget(loadProjectMessageBox);


    newProjectMessageBox = (new WidgetWindow("New Project", *(new WidgetRect())->setRelPosition(0.25, 0.25)->setRelSize(0.5, 0.5)))
        ->setColor(sf::Color(42, 42, 42, 191))
        ->addWidget(
            (new TextInputWidget("Name", *(new WidgetRect())->setRelPosition(0.3, 0.65)->setRelSize(0.4, 0.1)))
            ->setColor(sf::Color(63, 63, 63))
            ->getWidget()
        )
        ->addWidget(
            (new TextWidget("Input project name:", *(new WidgetRect())->setRelPosition(0.05, 0.15)->setRelSize(0.9, 0.4)))
            ->setCharacterSize(20)
        )
        ->addWidget(
            (new ButtonWidget("Create", *(new WidgetRect())->setRelPosition(0.40, 0.85)->setRelSize(0.2, 0.1)))
            ->setColor(sf::Color(42, 127, 42))
            ->onClick(
                [](Widget* widget, sf::Vector2i) {
                    widget->getParent()->setVisible(false);
                    
                    TextInputWidget* input = ((TextInputWidget*)widget->getParent()->getChildren()->at(0));
                    std::string name = input->getInput();
    
                    Project* project = Global::getApp()->project;

                    while (project->getTimeline()->getTrackCount() > 0) {
                        project->getTimeline()->removeTrack(0);
                    }
                    std::vector<Resource*>* allResources = Global::getApp()->resourceManager->getResourceVector();
                    while (allResources->size() > 0) {
                        delete allResources->at(0);
                        allResources->erase(allResources->begin());
                    }

                    std::string projectName = name;
                    int projectNumber = 0;
                    while (std::filesystem::exists(std::filesystem::path("projects") / projectName)) {
                        projectNumber++;
                        projectName = name + std::to_string(projectNumber);
                    }
                    project->setName(projectName);
                    project->setFramerate(30)->setWidth(854)->setHeight(480)->setLength(300);
                    project->getTimeline()->addTrack((new Track())->setName("Track 1")->setColor(sf::Color(0, 80, 100)));
                    project->getTimeline()->addTrack((new Track())->setName("Track 2")->setColor(sf::Color(0, 100, 0)));

                    Global::getApp()->setActiveClip(-1, -1);
                    Global::getApp()->refreshResourcePanel();
                    Global::getApp()->redrawAllTracks();
                    Global::getApp()->updatePreview();
                    Global::getApp()->updateProjectInfo();
                    Global::getApp()->showProjectProperties();
                }
            )
        )
        ->addWidget(
            (new ButtonWidget("X", *(new WidgetRect())->setRelPosition(0.9, 0)->setRelSize(0.1, 0.1)))
            ->setColor(sf::Color(127, 42, 42))
            ->onClick(
                [](Widget* widget, sf::Vector2i) {
                    widget->getParent()->setVisible(false);
                }
            )
        );
    newProjectMessageBox->setVisible(false);

    rootWidget->addWidget(newProjectMessageBox);


    changeTextClipMessageBox = (new WidgetWindow("Change Text", *(new WidgetRect())->setRelPosition(0.25, 0.25)->setRelSize(0.5, 0.5)))
        ->setColor(sf::Color(42, 42, 42, 191))
        ->addWidget(
            (new TextInputWidget("Text", *(new WidgetRect())->setRelPosition(0.3, 0.65)->setRelSize(0.4, 0.1)))
            ->setColor(sf::Color(63, 63, 63))
            ->getWidget()
        )
        ->addWidget(
            (new TextWidget("Input new text:", *(new WidgetRect())->setRelPosition(0.05, 0.15)->setRelSize(0.9, 0.4)))
            ->setCharacterSize(20)
        )
        ->addWidget(
            (new ButtonWidget("Confirm", *(new WidgetRect())->setRelPosition(0.40, 0.85)->setRelSize(0.2, 0.1)))
            ->setColor(sf::Color(42, 127, 42))
            ->onClick(
                [](Widget* widget, sf::Vector2i) {
                    widget->getParent()->setVisible(false);
                    
                    TextInputWidget* input = ((TextInputWidget*)widget->getParent()->getChildren()->at(0));
                    std::string text = input->getInput();
                    try {
                        Clip* clip = Global::getApp()->getActiveClipPtr();
                        if (clip->getClipType() == CLIP_TEXT) {
                            ((TextClip*)clip)->getText()->set(text);
                            Global::getApp()->refreshResourcePanel();
                        }
                    } catch (Exception* e) {
                        e->print();
                    }
                }
            )
        )
        ->addWidget(
            (new ButtonWidget("X", *(new WidgetRect())->setRelPosition(0.9, 0)->setRelSize(0.1, 0.1)))
            ->setColor(sf::Color(127, 42, 42))
            ->onClick(
                [](Widget* widget, sf::Vector2i) {
                    widget->getParent()->setVisible(false);
                }
            )
        );
    changeTextClipMessageBox->setVisible(false);

    rootWidget->addWidget(changeTextClipMessageBox);


    rootWidget->setRect(WidgetRect(0, 0, window.getSize().x, window.getSize().y));

    std::cout << "Initializing widgets... Done\n";
    std::cout << "Widget count: " << Widget::_getWidgetCount() << "\n";

    unsaved = false;
    setActiveClip(-1, -1);
    refreshResourcePanel();
    redrawAllTracks();
    updatePreview();
    updateProjectInfo();
    showProjectProperties();
    

    std::chrono::steady_clock::time_point lastFramerateCheckTimestamp = std::chrono::steady_clock::now();
    unsigned int frameCounter = 0;
    fps = 0;

    std::chrono::steady_clock::time_point lastFrameTimestamp = std::chrono::steady_clock::now();

    fpsText.setFont(*Global::getFont());
    fpsText.setCharacterSize(24);
    fpsText.setFillColor(sf::Color::White);


    sf::SoundBuffer jinglebuffer;
    sf::Sound jingle;
    if (jinglebuffer.loadFromFile("res/jingle.wav")) {
        jingle.setBuffer(jinglebuffer);
        jingle.setVolume(20);
        #ifndef DEBUG
            if (Settings::getSetting_long("playJingle")) {
                jingle.play();
            }
        #endif
    }
    // https://www.beepbox.co/#8n31s0k0l00e00t2mm0a7g0fj07i0r1o3210T0v1L4u00q0d0fay0z1C2w2c0h0T0v1L4ue2q3d5f7y3z8C0w5c2h2T0v1L4u11q0d0f8y0z1C2w1c0h0T2v1L4u02q0d1fay0z1C2w1b4000000000000000000001000000000040000000000p1-FEOFaKiIFGHIcJPqVT9002CzW8V8mImyCCEOQSKm0EEP2eImAmyCCwOSjnd000


    while (window.isOpen()) {
        // Increment FPS Counter
        frameCounter++;

        // Calculate change of time
        std::chrono::steady_clock::time_point newFrameTimestamp = std::chrono::steady_clock::now();
        float deltaTime = std::chrono::duration_cast<std::chrono::milliseconds>(newFrameTimestamp - lastFrameTimestamp).count();
        lastFrameTimestamp = newFrameTimestamp;
        if (rendering) {
            if (converting) {
                convertLoop();
            } else {
                renderLoop();
                if (converting) {
                    newFrameTimestamp = std::chrono::steady_clock::now() - std::chrono::seconds(1);
                    startConverting();
                }
            }
        }
        if (!rendering || converting) {
            loop(deltaTime);
        }
        

        // Save FPS Counter
        if (newFrameTimestamp - lastFramerateCheckTimestamp >= std::chrono::seconds(1)) {
            if (rendering || !converting) loop(std::chrono::duration_cast<std::chrono::milliseconds>(newFrameTimestamp - lastFramerateCheckTimestamp).count());
            lastFramerateCheckTimestamp = newFrameTimestamp;
            fps = frameCounter;
            frameCounter = 0;
        }

        // std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }
    clearTempData();
}



void Application::loop(float deltaTime) {

    updatePreview();

    // Process window events
    sf::Event winEvent;
    while (window.pollEvent(winEvent)) {
        if (rendering) {
            switch (winEvent.type) {
                case sf::Event::KeyPressed:
                    if (winEvent.key.code == sf::Keyboard::Escape) {
                        cancelRendering();
                    }
                    break;

                case sf::Event::Resized:
                    window.setView(sf::View(sf::FloatRect(0, 0, winEvent.size.width, winEvent.size.height)));
                    rootWidget->parentResize(sf::Vector2f(winEvent.size.width, winEvent.size.height));
                    break;

                case sf::Event::Closed:
                    window.close();
                    break;
            }
            continue;
        }

        switch (winEvent.type) {
            case sf::Event::Resized:
                window.setView(sf::View(sf::FloatRect(0, 0, winEvent.size.width, winEvent.size.height)));
                rootWidget->parentResize(sf::Vector2f(winEvent.size.width, winEvent.size.height));
                break;

            case sf::Event::Closed:
                window.close();
                break;

            case sf::Event::KeyPressed:

                switch (winEvent.key.code) {
                    case sf::Keyboard::Escape:
                        if (selectedTextField != nullptr) selectedTextField->setInput("");
                        selectTextField(nullptr);
                        break;

                    case sf::Keyboard::Left:
                        {
                            int delta = -project->getFramerate();
                            if (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift) || sf::Keyboard::isKeyPressed(sf::Keyboard::RShift)) delta = -1;
                            delta *= 1 + ((sf::Keyboard::isKeyPressed(sf::Keyboard::LControl) || sf::Keyboard::isKeyPressed(sf::Keyboard::RControl)) * 9);
                            Global::getApp()->moveCursor(delta);
                        }
                        break;

                    case sf::Keyboard::Right:
                        {
                            int delta = project->getFramerate();
                            if (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift) || sf::Keyboard::isKeyPressed(sf::Keyboard::RShift)) delta = 1;
                            delta *= 1 + ((sf::Keyboard::isKeyPressed(sf::Keyboard::LControl) || sf::Keyboard::isKeyPressed(sf::Keyboard::RControl)) * 9);
                            Global::getApp()->moveCursor(delta);
                        }
                        break;

                    case sf::Keyboard::Space:
                        if (selectedTextField == nullptr) playing = !playing;
                        break;

                    case sf::Keyboard::Delete:
                        if (selectedTextField == nullptr) {
                            if (activeClipTrack != -1 && activeClip != -1) {
                                deleteClip(activeClipTrack, activeClip);
                                setActiveClip(-1, -1);
                            }
                        }
                        break;

                }

                if (sf::Keyboard::isKeyPressed(sf::Keyboard::LControl) || sf::Keyboard::isKeyPressed(sf::Keyboard::RControl)) {
                    if (winEvent.key.code == sf::Keyboard::S) {
                        save();
                        break;
                    } else if (winEvent.key.code == sf::Keyboard::O) {
                        load();
                        break;
                    }
                }

                if (winEvent.key.code >= sf::Keyboard::A && winEvent.key.code <= sf::Keyboard::Z) {
                    writeChar(winEvent.key.code - sf::Keyboard::A + 'a' +
                    (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift) || sf::Keyboard::isKeyPressed(sf::Keyboard::RShift)) * ('A' - 'a'));
                }
                if (winEvent.key.code >= sf::Keyboard::Num0 && winEvent.key.code <= sf::Keyboard::Num9) writeChar(winEvent.key.code - sf::Keyboard::Num0 + '0');
                if (winEvent.key.code == sf::Keyboard::BackSpace) writeChar(8);
                if (winEvent.key.code == sf::Keyboard::Space) writeChar(' ');
                if (winEvent.key.code == sf::Keyboard::Period) writeChar('.');
                if (winEvent.key.code == sf::Keyboard::Slash) writeChar('/');
                if (winEvent.key.code == sf::Keyboard::BackSlash) writeChar('\\');
                if (winEvent.key.code == sf::Keyboard::Return) selectTextField(nullptr);
                break;

            case sf::Event::MouseButtonPressed:
                if (winEvent.mouseButton.button == sf::Mouse::Button::Left) {
                    selectTextField(nullptr);
                    rootWidget->click(sf::Vector2i(winEvent.mouseButton.x, winEvent.mouseButton.y));
                } else if (winEvent.mouseButton.button == sf::Mouse::Button::Right) {
                    rootWidget->rightClick(sf::Vector2i(winEvent.mouseButton.x, winEvent.mouseButton.y));
                }
                break;

            case sf::Event::MouseWheelScrolled:
                rootWidget->scroll(sf::Vector2i(winEvent.mouseWheelScroll.x, winEvent.mouseWheelScroll.y), winEvent.mouseWheelScroll.delta);
                break;

            case sf::Event::MouseMoved:
                rootWidget->mouseMove(sf::Vector2i(winEvent.mouseMove.x, winEvent.mouseMove.y));
                break;

        }

    }



    // Handle popup window
    if (previewPopupWindow != nullptr) {
        sf::Event winEvent;
        while (previewPopupWindow->pollEvent(winEvent)) {
            switch (winEvent.type) {
                case sf::Event::Closed:
                    previewPopupWindow->close();
                    break;

                case sf::Event::KeyPressed:
                    switch (winEvent.key.code) {

                        case sf::Keyboard::Escape:
                            previewPopupWindow->close();
                            break;

                        case sf::Keyboard::Space:
                            if (!rendering && selectedTextField == nullptr) playing = !playing;
                            break;
                    
                        case sf::Keyboard::F11:
                            previewPopupWindowFullscreen = !previewPopupWindowFullscreen;
                            if (previewPopupWindowFullscreen) {
                                previewPopupWindow->create(sf::VideoMode(project->getWidth(), project->getHeight()), "Linux Movie Maker v" VERSION " - Fullscreen Preview", sf::Style::Fullscreen);
                            } else {
                                previewPopupWindow->create(sf::VideoMode(project->getWidth(), project->getHeight()), "Linux Movie Maker v" VERSION " - Preview");
                            }
                            previewPopupWindow->requestFocus();
                            break;
                    }
                    break;
            }
        }

        sf::Vector2u size = previewPopupWindow->getSize();
        float correctHeight = size.x * ((float)project->getHeight() / (float)project->getWidth());
        if (size.y != correctHeight) {
            previewPopupWindow->setSize(sf::Vector2u(size.x, correctHeight));
        }

        sf::Sprite sprite;
        sprite.setTexture(previewTexture->getTexture());
        previewPopupWindow->setView(sf::View(sf::FloatRect(0, 0, project->getWidth(), project->getHeight())));

        previewPopupWindow->clear(sf::Color::Black);
        previewPopupWindow->draw(sprite);
        previewPopupWindow->display();

        if (!previewPopupWindow->isOpen()) {
            delete previewPopupWindow;
            previewPopupWindow = nullptr;
        }
    }



    // std::cout << "mouse pos " << winEvent.mouseMove.x << ", " << winEvent.mouseMove.y << "\n";
    if (sf::Mouse::getPosition(window).x < 0
        || sf::Mouse::getPosition(window).y < 0
        || sf::Mouse::getPosition(window).x > window.getView().getSize().x
        || sf::Mouse::getPosition(window).y > window.getView().getSize().y
    ) {
        if (rootWidget->isMouseOver()) {
            rootWidget->unhover();
        }
    } else {
        if (!rootWidget->isMouseOver()) {
            rootWidget->hover();
        }
    }

    if (playing) {
        cursorMoveSec += deltaTime / 1000;
        int frameCount = project->secondsToFrame((double)cursorMoveSec);
        if (frameCount != 0) {
            moveCursor(frameCount);
            cursorMoveSec -= project->frameToSeconds(project->secondsToFrame((double)cursorMoveSec));
        }
    } else {
        cursorMoveSec = 0;
    }

    if (rendering) {
        if (converting) {
            renderingProgressText->setString(
                "Progress: Converting to mp4 with ffmpeg..."
            );
        } else {
            renderingProgressText->setString(
                "Progress: " +
                std::to_string(project->getTimeline()->getCurrentPosition()) + "/" + std::to_string(project->getLength()) + ", " +
                std::to_string((int)(100 * (float)project->getTimeline()->getCurrentPosition() / (float)project->getLength())) + "%"
            );
        }
    }

    // Draw everything
    window.clear(sf::Color(21, 21, 21));

    // Draw widgets (buttons etc.)
    rootWidget->draw(&window);

    #ifdef DEBUG
        // Prepare FPS / debug text
        std::stringstream ss;
        ss << "v" VERSION "\n"
        << "FPS: " << fps << " "
        << "Widgets: " << Widget::_getWidgetCount() << "\n"
        << "Selected Clip: " << activeClipTrack << ":" << activeClip << "";

        // Draw FPS
        fpsText.setString(ss.str());
        fpsText.setPosition(0, window.getSize().y - fpsText.getLocalBounds().height - 10);
        window.draw(fpsText);
    #endif

    // Display everything
    window.display();
}

void Application::renderLoop() {
    drawPreview();

    Timeline* timeline = project->getTimeline();

    sf::Image frame = previewTexture->getTexture().copyToImage();
    std::filesystem::path path = getTempDir() / "render" / (std::to_string(timeline->getCurrentPosition()) + ".png");
    frame.saveToFile(path.string());
    timeline->setCurrentPosition(timeline->getCurrentPosition() + 1);
    if (timeline->getCurrentPosition() >= project->getLength()) {
        converting = true;
    }
}

void Application::convertLoop() {
    // TODO: move code below to Application::startConverting() and use a separate thread
    // Start ffmpeg
    std::filesystem::path inputPath = getTempDir() / "render" / "%d.png";
    std::filesystem::path outputPath = getProjectDir() / "output.mp4";
    int outputNumber = 0;
    while (std::filesystem::exists(outputPath)) {
        outputPath = getProjectDir() / std::string("output" + std::to_string(outputNumber) + ".mp4");
        outputNumber++;
    }
    std::filesystem::path audioPath = getProjectDir() / "audio.mp3";
    if (std::filesystem::exists(audioPath)) {
        system(std::string(
            std::string("ffmpeg -hide_banner -framerate ")
        + std::to_string(project->getFramerate())
        + std::string(" -i ")
        + std::string(inputPath.string())
        + std::string(" -i ")
        + std::string(audioPath.string())
        + std::string(" -vf format=yuv420p ")
        + std::string(outputPath.string())
        ).c_str());
    } else {
        system(std::string(
            std::string("ffmpeg -hide_banner -framerate ")
        + std::to_string(project->getFramerate())
        + std::string(" -i ")
        + std::string(inputPath.string())
        + std::string(" -vf format=yuv420p ")
        + std::string(outputPath.string())
        ).c_str());
    }
    // Check if ffmpeg is done
    if (true) {
        lastOutputPath = outputPath;
        finishRendering();
    }
}

void Application::finishRendering() {
    rendering = false;
    converting = false;
    renderingMessageBox->setVisible(false);
    renderingDoneMessageBox->setVisible(true);
    window.setFramerateLimit(Settings::getSetting_ulong("windowFramerateLimit"));
    renderingDoneText->setString(std::string("Rendering Done!\nVideo saved to:\n") + lastOutputPath.string());
    clearTempData();
}

void Application::startConverting() {
    converting = true;
}