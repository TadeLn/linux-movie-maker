#include "Widget.hpp"
#include "WidgetContainer.hpp"

#include <iostream>
#include <SFML/Graphics.hpp>


unsigned int Widget::getId() {
    return id;
}

WidgetContainer* Widget::getParent() {
    return (WidgetContainer*)parent;
}

WidgetRect* Widget::getRect() {
    return &rect;
}

sf::FloatRect Widget::getFloatRectRelativeToWidget() {
    return rect.getFloatRect(sf::Vector2f(0, 0), sf::Vector2f(0, 0));
}

sf::FloatRect Widget::getFloatRect() {
    if (parent != nullptr) {
        return rect.getFloatRect(parent->getFloatRect());
    } else {
        return rect.getFloatRect(sf::FloatRect(0, 0, 0, 0));
    }
}

bool Widget::isVisible() {
    return visible;
}

void* Widget::getExtraData() {
    return extraData;
}


Widget* Widget::setParent(Widget* parent) {
    this->parent = parent;
    return this;
}

Widget* Widget::setRect(WidgetRect rect) {
    this->rect = rect;
    return this;
}

Widget* Widget::setVisible(bool visible) {
    this->visible = visible;
    return this;
}

Widget* Widget::setExtraData(void* extraData) {
    this->extraData = extraData;
    return this;
}


bool Widget::isInRect(sf::Vector2i mousePos) {
    return getFloatRect().contains(mousePos.x, mousePos.y);
}

bool Widget::isMouseOver() {
    return isHover;
}


void Widget::click(sf::Vector2i mousePos) {
    if (clickCallback != nullptr) {
        clickCallback(this, mousePos);
    }
}

void Widget::rightClick(sf::Vector2i mousePos) {
    if (rightClickCallback != nullptr) {
        rightClickCallback(this, mousePos);
    }
}

void Widget::scroll(sf::Vector2i mousePos, int delta) {
    if (scrollCallback != nullptr) {
        scrollCallback(this, mousePos, delta);
    }
}

void Widget::hover() {
    isHover = true;
    if (hoverCallback != nullptr) {
        hoverCallback(this);
    }
}

void Widget::unhover() {
    isHover = false;
    if (unhoverCallback != nullptr) {
        unhoverCallback(this);
    }
}

void Widget::mouseMove(sf::Vector2i mousePos) {
    if (mouseMoveCallback != nullptr) {
        mouseMoveCallback(this, mousePos);
    }
}

void Widget::parentResize(sf::Vector2f size) {
    if (parentResizeCallback != nullptr) {
        parentResizeCallback(this, size);
    }
}

void (*Widget::getClickCallback())(Widget*, sf::Vector2i) { return clickCallback; }

void (*Widget::getRightClickCallback())(Widget*, sf::Vector2i) { return rightClickCallback; }

void (*Widget::getScrollCallback())(Widget*, sf::Vector2i, int) { return scrollCallback; }

void (*Widget::getHoverCallback())(Widget*) { return hoverCallback; }

void (*Widget::getUnhoverCallback())(Widget*) { return unhoverCallback; }

void (*Widget::getMouseMoveCallback())(Widget*, sf::Vector2i) { return mouseMoveCallback; }

void (*Widget::getParentResizeCallback())(Widget*, sf::Vector2f) { return parentResizeCallback; }


Widget* Widget::onClick(void (*cb)(Widget*, sf::Vector2i)) {
    clickCallback = cb;
    return this;
}

Widget* Widget::onRightClick(void (*cb)(Widget*, sf::Vector2i)) {
    rightClickCallback = cb;
    return this;
}

Widget* Widget::onScroll(void (*cb)(Widget*, sf::Vector2i, int)) {
    scrollCallback = cb;
    return this;
}

Widget* Widget::onHover(void (*cb)(Widget*)) {
    hoverCallback = cb;
    return this;
}

Widget* Widget::onUnhover(void (*cb)(Widget*)) {
    unhoverCallback = cb;
    return this;
}

Widget* Widget::onMouseMove(void (*cb)(Widget*, sf::Vector2i)) {
    mouseMoveCallback = cb;
    return this;
}

Widget* Widget::onParentResize(void (*cb)(Widget*, sf::Vector2f)) {
    parentResizeCallback = cb;
    return this;
}


sf::Vector2i Widget::absToRel(sf::Vector2i absPos) {
    return sf::Vector2i(absPos.x - getFloatRect().left, absPos.y - getFloatRect().top);
}

sf::Vector2f Widget::absToRel(sf::Vector2f absPos) {
    return sf::Vector2f(absPos.x - getFloatRect().left, absPos.y - getFloatRect().top);
}

sf::Vector2i Widget::relToAbs(sf::Vector2i relPos) {
    return sf::Vector2i(relPos.x + getFloatRect().left, relPos.y + getFloatRect().top);
}

sf::Vector2f Widget::relToAbs(sf::Vector2f relPos) {
    return sf::Vector2f(relPos.x + getFloatRect().left, relPos.y + getFloatRect().top);
}


Widget::Widget() {
    id = getNewId(this);
    parent = nullptr;
    rect = WidgetRect();
    visible = true;
    extraData = nullptr;
    isHover = false;

    clickCallback = nullptr;
    rightClickCallback = nullptr;
    scrollCallback = nullptr;
    hoverCallback = nullptr;
    unhoverCallback = nullptr;
    mouseMoveCallback = nullptr;
    parentResizeCallback = nullptr;
}


Widget::~Widget() {
    allWidgets[id] = nullptr;
}


unsigned int Widget::_getLastWidgetId() {
    return nextId;
}

unsigned int Widget::_getWidgetCount() {
    unsigned int count = 0;
    for (Widget* x : allWidgets) {
        if (x != nullptr) {
            ++count;
        }
    }
    return count;
}

std::vector<Widget*> Widget::_getAllWidgets() {
    return allWidgets;
}

unsigned int Widget::nextId = 0;
std::vector<Widget*> Widget::allWidgets = std::vector<Widget*>();

unsigned int Widget::getNewId(Widget* widget) {
    allWidgets.push_back(widget);
    return nextId++;
}
