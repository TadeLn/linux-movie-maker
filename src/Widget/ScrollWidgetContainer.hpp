#pragma once

#include "RTWidgetContainer.hpp"


class ScrollWidgetContainer : public RTWidgetContainer {

public:
    void scroll(sf::Vector2i, int);

    int getScrollAmount();
    int getScrollSensitivity();
    
    ScrollWidgetContainer* setScrollAmount(int);
    ScrollWidgetContainer* setScrollSensitivity(int);

    ScrollWidgetContainer(WidgetRect);

private:
    int scrollSensitivity;
    int scrollAmount;

};
