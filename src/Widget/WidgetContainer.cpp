#include "WidgetContainer.hpp"

#include <SFML/Graphics.hpp>
#include <iostream>


void WidgetContainer::draw(sf::RenderTarget* target) {
    // Draw bg
    sf::FloatRect floatRect = getFloatRect();
    rectangle.setSize(sf::Vector2f(floatRect.width, floatRect.height));
    rectangle.setPosition(floatRect.left, floatRect.top);
    target->draw(rectangle);

    // Draw children
    for (int i = 0; i < widgets.size(); i++) {
        if (widgets[i]->isVisible()) {
            widgets[i]->draw(target);
        }
    }
}

void WidgetContainer::click(sf::Vector2i mousePos) {
    Widget::click(mousePos);
    for (int i = widgets.size() - 1; i >= 0; i--) {
        if (widgets[i]->isMouseOver() && widgets[i]->isVisible()) {
            widgets[i]->click(mousePos);
            break;
        }
    }
}

void WidgetContainer::rightClick(sf::Vector2i mousePos) {
    Widget::rightClick(mousePos);
    for (int i = widgets.size() - 1; i >= 0; i--) {
        if (widgets[i]->isMouseOver() && widgets[i]->isVisible()) {
            widgets[i]->rightClick(mousePos);
            break;
        }
    }
}

void WidgetContainer::scroll(sf::Vector2i mousePos, int delta) {
    Widget::scroll(mousePos, delta);
    for (int i = widgets.size() - 1; i >= 0; i--) {
        if (widgets[i]->isMouseOver() && widgets[i]->isVisible()) {
            widgets[i]->scroll(mousePos, delta);
            break;
        }
    }
}

void WidgetContainer::mouseMove(sf::Vector2i mousePos) {
    Widget::mouseMove(mousePos);
    for (int i = 0; i < widgets.size(); i++) {
        if (widgets[i]->isInRect(mousePos)) {
            widgets[i]->mouseMove(mousePos);
        }
        if (widgets[i]->isInRect(mousePos) && !widgets[i]->isMouseOver()) {
            widgets[i]->hover();
        } else if (!widgets[i]->isInRect(mousePos) && widgets[i]->isMouseOver()) {
            widgets[i]->unhover();
        }
    }
}

void WidgetContainer::unhover() {
    Widget::unhover();
    for (int i = 0; i < widgets.size(); i++) {
        if (widgets[i]->isMouseOver()) {
            widgets[i]->unhover();
        }
    }
    return;
}

void WidgetContainer::parentResize(sf::Vector2f size) {
    Widget::parentResize(size);

    // Update child widgets
    sf::FloatRect floatRect = getFloatRect();
    for (int i = 0; i < widgets.size(); i++) {
        widgets[i]->parentResize(sf::Vector2f(floatRect.width, floatRect.height));
    }
    return;
}


sf::Color WidgetContainer::getColor() {
    return rectangle.getFillColor();
}


Widget* WidgetContainer::setRect(WidgetRect rect) {
    Widget::setRect(rect);

    // Update child widgets
    sf::FloatRect floatRect = getFloatRect();
    for (int i = 0; i < widgets.size(); i++) {
        widgets[i]->parentResize(sf::Vector2f(floatRect.width, floatRect.height));
    }
    return this;
}

WidgetContainer* WidgetContainer::setColor(sf::Color color) {
    rectangle.setFillColor(color);
    return this;
}


WidgetContainer* WidgetContainer::addWidget(Widget* widget) {
    widget->setParent(this);
    widgets.push_back(widget);
    return this;
}

std::vector<Widget*>* WidgetContainer::getChildren() {
    return &widgets;
}

WidgetContainer* WidgetContainer::clearChildren() {
    for (int i = 0; i < widgets.size(); i++) {
        delete widgets[i];
    }
    widgets.clear();
    return this;
}


WidgetContainer::WidgetContainer(WidgetRect rect)
: Widget() {
    setRect(rect);

    setColor(sf::Color::Transparent);

    widgets = std::vector<Widget*>();
}

WidgetContainer::~WidgetContainer() {
    clearChildren();
}
