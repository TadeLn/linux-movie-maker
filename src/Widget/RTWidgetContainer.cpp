#include "RTWidgetContainer.hpp"

#include <iostream>
#include "WidgetContainer.hpp"
#include "WidgetRect.hpp"


void RTWidgetContainer::draw(sf::RenderTarget* target) {
    sf::FloatRect floatRect = getFloatRect();

    // Draw bg
    rectangle.setSize(sf::Vector2f(floatRect.width, floatRect.height));
    rectangle.setPosition(0, 0);
    renderTexture->draw(rectangle);

    WidgetRect oldWidgetRect = rect;
    rect.setAbsPosition(offset.x - parent->getFloatRect().left, offset.y - parent->getFloatRect().top);

    // Draw children onto bg
    for (int i = 0; i < widgets.size(); i++) {
        if (widgets[i]->isVisible()) {
            widgets[i]->draw(renderTexture);
        }
    }

    // Finish drawing texture
    rect = oldWidgetRect; 
    renderTexture->display();

    sf::Sprite sprite;
    sprite.setPosition(floatRect.left, floatRect.top);
    sprite.setTexture(renderTexture->getTexture());
    sprite.setTextureRect(sf::IntRect(0, 0, floatRect.width, floatRect.height));
    target->draw(sprite);

    renderTexture->clear(sf::Color::Transparent);
    renderTexture->create(floatRect.width, floatRect.height);
}

void RTWidgetContainer::click(sf::Vector2i mousePos) {
    WidgetContainer::click(sf::Vector2i(mousePos.x - offset.x, mousePos.y - offset.y));
}

void RTWidgetContainer::rightClick(sf::Vector2i mousePos) {
    WidgetContainer::rightClick(sf::Vector2i(mousePos.x - offset.x, mousePos.y - offset.y));
}

void RTWidgetContainer::scroll(sf::Vector2i mousePos, int delta) {
    WidgetContainer::scroll(sf::Vector2i(mousePos.x - offset.x, mousePos.y - offset.y), delta);
}

void RTWidgetContainer::mouseMove(sf::Vector2i mousePos) {
    WidgetContainer::mouseMove(sf::Vector2i(mousePos.x - offset.x, mousePos.y - offset.y));
}

sf::RenderTexture* RTWidgetContainer::getRenderTexture() {
    return renderTexture;
}

RTWidgetContainer::RTWidgetContainer(WidgetRect rect)
: WidgetContainer(rect) {
    renderTexture = new sf::RenderTexture();
    offset = sf::Vector2i(0, 0);
    renderTexture->create(1, 1);
}
