#pragma once

#include <SFML/Graphics.hpp>
#include "Widget.hpp"


enum TextAlignment {
    TEXT_ALIGNMENT_UNSET,
    TEXT_ALIGNMENT_LEFT,
    TEXT_ALIGNMENT_CENTER,
    TEXT_ALIGNMENT_RIGHT
};

class TextWidget : public Widget {

public:
    Widget* setRect(WidgetRect);

    void draw(sf::RenderTarget*);
    void click(sf::Vector2i);
    void mouseMove(sf::Vector2i);
    void parentResize(sf::Vector2f);

    std::string getString();
    sf::Color getColor();
    int getCharacterSize();
    TextAlignment getAlignment();
    unsigned int getStyle();

    TextWidget* setString(std::string);
    TextWidget* setColor(sf::Color);
    TextWidget* setCharacterSize(int);
    TextWidget* setAlignment(TextAlignment);
    TextWidget* setStyle(unsigned int);


    TextWidget(std::string, WidgetRect);
    
protected:
    sf::Text text;
    int fontSize;
    TextAlignment alignment;

};
