#include "TrackWidget.hpp"
#include "../Application.hpp"
#include "../Global.hpp"

#include <iostream>


void TrackWidget::draw(sf::RenderTarget* target) {
    sf::FloatRect floatRect = getFloatRect();
    trackNameWidget->setRect(*(new WidgetRect(floatRect))->setAbsSizeY(floatRect.height / 2));
    trackIdWidget->setRect(*(new WidgetRect(floatRect))->setAbsSizeY(floatRect.height / 2)->setAbsPositionY(floatRect.top + (floatRect.height / 2)));

    if (Global::getApp()->getActiveClip().first == trackId) {
        rectangle.setOutlineThickness(1 + (Global::getApp()->getActiveClip().second == -1));
    } else {
        rectangle.setOutlineThickness(0);
    }
    ButtonWidget::draw(target);
    
    trackNameWidget->draw(target);
    trackIdWidget->draw(target);
}

void TrackWidget::click(sf::Vector2i mousePos) {
    Global::getApp()->setActiveClip(this->trackId, -1);
    ButtonWidget::click(mousePos);
}


int TrackWidget::getTrackId() {
    return trackId;
}

std::string TrackWidget::getName() {
    return name;
}

sf::Color TrackWidget::getColor() {
    return color;
}


TrackWidget* TrackWidget::setTrackId(int trackId) {
    this->trackId = trackId;
    trackIdWidget->setString(std::string("#") + std::to_string(trackId));
    return this;
}

TrackWidget* TrackWidget::setName(std::string name) {
    this->name = name;
    trackNameWidget->setString(name);
    return this;
}

TrackWidget* TrackWidget::setColor(sf::Color color) {
    this->color = color;
    return this;
}


Widget* TrackWidget::getWidget() {
    return this;
}

TrackWidget::TrackWidget(WidgetRect rect) 
: ButtonWidget("", rect) {
    rectangle.setOutlineColor(sf::Color::White);
    trackId = -1;
    name = "";
    trackNameWidget = (new TextWidget(name, WidgetRect()))
        ->setCharacterSize(15);
    trackIdWidget = (new TextWidget(std::string("#") + std::to_string(trackId), WidgetRect()))
        ->setCharacterSize(15)
        ->setColor(sf::Color(127, 127, 127));
}

TrackWidget::~TrackWidget() {
    delete trackNameWidget;
    delete trackIdWidget;
}
