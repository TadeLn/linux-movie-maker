#include "ClipWidget.hpp"

#include <SFML/Graphics.hpp>
#include <iostream>
#include "RTWidgetContainer.hpp"
#include "../Exception.hpp"
#include "../Timeline/Clip/ResourceClip.hpp"
#include "../Timeline/Clip/TextClip.hpp"
#include "../Global.hpp"


void ClipWidget::draw(sf::RenderTarget* target) {
    sf::FloatRect floatRect = getFloatRect();
    outline.setSize(sf::Vector2f(floatRect.width - 4, 44));
    if (floatRect.width < 48) {
        imageWidget->getRect()->setAbsSizeX(floatRect.width - 4);
    } else {
        imageWidget->getRect()->setAbsSizeX(48);
    }

    outline.setFillColor(Global::getApp()->project->getTimeline()->getTrack(track)->getColor());
    if (Global::getApp()->getActiveClipPtr() == clip) {
        outline.setOutlineThickness(2);
    } else {
        outline.setOutlineThickness(0);
    }
    renderTexture->draw(outline);

    RTWidgetContainer::draw(target);
    return;
}

void ClipWidget::click(sf::Vector2i mousePos) {
    Global::getApp()->setActiveClip(track, getClipId());
    RTWidgetContainer::click(mousePos);
}


Clip* ClipWidget::getClip() {
    return clip;
}

int ClipWidget::getTrackId() {
    return track;
}

unsigned int ClipWidget::getClipId() {
    if (clip == nullptr) {
        return 0;
    }
    return Global::getApp()->project->getTimeline()->getTrack(track)->getClipIdAtPosition(clip->getTime());
}


ClipWidget* ClipWidget::setClip(int trackId, Clip* clipPtr) {
    clip = clipPtr;
    track = trackId;

    textWidget->setString("#" + std::to_string(track) + ":" + std::to_string(getClipId()));
    if (clip != nullptr) {
        imageWidget->setTexture(clip->getThumbnail());

        if (clip->getClipType() == CLIP_RESOURCE) {
            textWidget->setString(((ResourceClip*)clip)->getResource()->getName());
        }
        if (clip->getClipType() == CLIP_TEXT) {
            textWidget->setString(((TextClip*)clip)->getText()->get());
        }
        updateRect();
    }
    return this;
}

void ClipWidget::updateRect() {
    setRect(*(new WidgetRect(0, 0, 0, 0))->setRelSizeY(1)->setAbsPositionX(clip->getTime())->setAbsSizeX(clip->getLength()));
}

ClipWidget::ClipWidget()
: RTWidgetContainer(*(new WidgetRect(0, 0, 0, 0))->setRelSizeY(1)) {
    imageWidget = (new ImageWidget(WidgetRect(2, 2, 44, 44)));
    addWidget(imageWidget);

    textWidget = (new TextWidget("", *(new WidgetRect(5, 2, 0, 20))->setRelSizeX(1)))
        ->setColor(sf::Color::White)
        ->setCharacterSize(10)
        ->setAlignment(TEXT_ALIGNMENT_LEFT);
    addWidget(textWidget);

    setClip(0, nullptr);

    rectangle.setFillColor(sf::Color::Transparent);
    outline = sf::RectangleShape();
    outline.setOutlineColor(sf::Color::Red);
    outline.setPosition(sf::Vector2f(2, 2));
}
