#pragma once

#include "RTWidgetContainer.hpp"


class HorizontalScrollWidgetContainer : public RTWidgetContainer {

public:
    void scroll(sf::Vector2i, int);

    int getScrollAmount();
    int getScrollSensitivity();

    HorizontalScrollWidgetContainer* setScrollAmount(int);
    HorizontalScrollWidgetContainer* setScrollSensitivity(int);

    HorizontalScrollWidgetContainer(WidgetRect);

protected:
    int scrollSensitivity;
    int scrollAmount;

};
