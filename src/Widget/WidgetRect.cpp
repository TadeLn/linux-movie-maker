#include "WidgetRect.hpp"

#include <SFML/Graphics.hpp>


WidgetValue WidgetRect::getPositionX() {
    return position.x;
}

WidgetValue WidgetRect::getPositionY() {
    return position.y;
}

WidgetValue WidgetRect::getSizeX() {
    return size.x;
}

WidgetValue WidgetRect::getSizeY() {
    return size.y;
}

WidgetRect* WidgetRect::setPositionX(WidgetValue value) {
    position.x = value;
    return this;
}

WidgetRect* WidgetRect::setPositionY(WidgetValue value) {
    position.y = value;
    return this;
}

WidgetRect* WidgetRect::setSizeX(WidgetValue value) {
    size.x = value;
    return this;
}

WidgetRect* WidgetRect::setSizeY(WidgetValue value) {
    size.y = value;
    return this;
}


WidgetRect* WidgetRect::setAbsPosition(float x, float y) { return setAbsPositionX(x)->setAbsPositionY(y); }
WidgetRect* WidgetRect::setAbsPosition(sf::Vector2f value) { return setAbsPositionX(value.x)->setAbsPositionY(value.y); }
WidgetRect* WidgetRect::setAbsPositionX(float value) { return setPositionX(WidgetValue(value)); }
WidgetRect* WidgetRect::setAbsPositionY(float value) { return setPositionY(WidgetValue(value)); }

WidgetRect* WidgetRect::setAbsSize(float x, float y) { return setAbsSizeX(x)->setAbsSizeY(y); }
WidgetRect* WidgetRect::setAbsSize(sf::Vector2f value) { return setAbsSizeX(value.x)->setAbsSizeY(value.y); }
WidgetRect* WidgetRect::setAbsSizeX(float value) { return setSizeX(WidgetValue(value)); }
WidgetRect* WidgetRect::setAbsSizeY(float value) { return setSizeY(WidgetValue(value)); }

WidgetRect* WidgetRect::setRelPosition(float x, float y) { return setRelPositionX(x)->setRelPositionY(y); }
WidgetRect* WidgetRect::setRelPosition(sf::Vector2f value) { return setRelPositionX(value.x)->setRelPositionY(value.y); }
WidgetRect* WidgetRect::setRelPositionX(float value) { return setPositionX(WidgetValue(value, true)); }
WidgetRect* WidgetRect::setRelPositionY(float value) { return setPositionY(WidgetValue(value, true)); }

WidgetRect* WidgetRect::setRelSize(float x, float y) { return setRelSizeX(x)->setRelSizeY(y); }
WidgetRect* WidgetRect::setRelSize(sf::Vector2f value) { return setRelSizeX(value.x)->setRelSizeY(value.y); }
WidgetRect* WidgetRect::setRelSizeX(float value) { return setSizeX(WidgetValue(value, true)); }
WidgetRect* WidgetRect::setRelSizeY(float value) { return setSizeY(WidgetValue(value, true)); }


sf::FloatRect WidgetRect::getFloatRect(sf::Vector2f parentPosition, sf::Vector2f parentSize) {
    sf::FloatRect result;
    result.left = parentPosition.x + ((int)position.x.isRelative() * (position.x.val() * parentSize.x)) + ((int)!position.x.isRelative() * (position.x.val()));
    result.top = parentPosition.y + ((int)position.y.isRelative() * (position.y.val() * parentSize.y)) + ((int)!position.y.isRelative() * (position.y.val()));
    result.width = ((int)size.x.isRelative() * (size.x.val() * parentSize.x)) + ((int)!size.x.isRelative() * (size.x.val()));
    result.height = ((int)size.y.isRelative() * (size.y.val() * parentSize.y)) + ((int)!size.y.isRelative() * (size.y.val()));
    return result;
}

sf::FloatRect WidgetRect::getFloatRect(sf::FloatRect parentRect) {
    return getFloatRect(
        sf::Vector2f(parentRect.left, parentRect.top),
        sf::Vector2f(parentRect.width, parentRect.height)
    );
}


WidgetRect::WidgetRect()
: WidgetRect(sf::FloatRect(0, 0, 0, 0)) {}

WidgetRect::WidgetRect(sf::FloatRect floatRect) {
    setAbsPositionX(floatRect.left)->setAbsPositionY(floatRect.top);
    setAbsSizeX(floatRect.width)->setAbsSizeY(floatRect.height);
}

WidgetRect::WidgetRect(sf::Vector2f pos, sf::Vector2f size) {
    setAbsPositionX(pos.x)->setAbsPositionY(pos.y);
    setAbsSizeX(size.x)->setAbsSizeY(size.y);
}

WidgetRect::WidgetRect(float x, float y, float width, float height) {
    setAbsPositionX(x)->setAbsPositionY(y);
    setAbsSizeX(width)->setAbsSizeY(height);
}

WidgetRect::WidgetRect(WidgetValue x, WidgetValue y, WidgetValue width, WidgetValue height) {
    setPositionX(x);
    setPositionY(y);
    setSizeX(width);
    setSizeY(height);
}
