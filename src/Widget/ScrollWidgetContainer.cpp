#include "ScrollWidgetContainer.hpp"

#include <iostream>
#include "RTWidgetContainer.hpp"
#include "WidgetRect.hpp"


void ScrollWidgetContainer::scroll(sf::Vector2i mousePos, int delta) {
    RTWidgetContainer::scroll(mousePos, delta);
    setScrollAmount(scrollAmount + (delta * scrollSensitivity));
}


int ScrollWidgetContainer::getScrollAmount() {
    return scrollAmount;
}

int ScrollWidgetContainer::getScrollSensitivity() {
    return scrollSensitivity;
}


ScrollWidgetContainer* ScrollWidgetContainer::setScrollAmount(int scrollAmount) {
    this->scrollAmount = scrollAmount;
    if (this->scrollAmount > 0) this->scrollAmount = 0;
    offset.y = this->scrollAmount;
    return this;
}

ScrollWidgetContainer* ScrollWidgetContainer::setScrollSensitivity(int scrollSensitivity) {
    this->scrollSensitivity = scrollSensitivity;
    return this;
}


ScrollWidgetContainer::ScrollWidgetContainer(WidgetRect rect)
: RTWidgetContainer(rect) {
    scrollAmount = 0;
    scrollSensitivity = 30;
}
