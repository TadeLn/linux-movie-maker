#include "ButtonWidget.hpp"


class ResourceButtonWidget : public ButtonWidget {

public:
    void click(sf::Vector2i);
    void rightClick(sf::Vector2i);

    unsigned int getResourceId();
    ResourceButtonWidget* setResourceId(unsigned int);

    ResourceButtonWidget(WidgetRect);

private:
    unsigned int resourceId;

};
