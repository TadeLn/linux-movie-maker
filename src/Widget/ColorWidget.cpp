#include "ColorWidget.hpp"

#include <SFML/Graphics.hpp>
#include <iostream>
#include "../Global.hpp"


void ColorWidget::draw(sf::RenderTarget* target) {
    sf::FloatRect floatRect = getFloatRect();
    rectangle.setPosition(floatRect.left, floatRect.top);
    rectangle.setSize(sf::Vector2f(floatRect.width, floatRect.height));
    target->draw(rectangle);
    return;
}


sf::Color ColorWidget::getColor() {
    return rectangle.getFillColor();
}


ColorWidget* ColorWidget::setColor(sf::Color color) {
    rectangle.setFillColor(color);
    return this;
}


ColorWidget::ColorWidget(WidgetRect rect)
: Widget() {
    setRect(rect);
    setColor(sf::Color::White);
}
