#include "HorizontalScrollWidgetContainer.hpp"

#include <iostream>
#include "RTWidgetContainer.hpp"
#include "WidgetRect.hpp"


void HorizontalScrollWidgetContainer::scroll(sf::Vector2i mousePos, int delta) {
    RTWidgetContainer::scroll(mousePos, delta);
    setScrollAmount(scrollAmount + (delta * scrollSensitivity));
}


int HorizontalScrollWidgetContainer::getScrollAmount() {
    return scrollAmount;
}

int HorizontalScrollWidgetContainer::getScrollSensitivity() {
    return scrollSensitivity;
}


HorizontalScrollWidgetContainer* HorizontalScrollWidgetContainer::setScrollAmount(int scrollAmount) {
    this->scrollAmount = scrollAmount;
    if (this->scrollAmount > 0) this->scrollAmount = 0;
    offset.x = this->scrollAmount;
    return this;
}

HorizontalScrollWidgetContainer* HorizontalScrollWidgetContainer::setScrollSensitivity(int scrollSensitivity) {
    this->scrollSensitivity = scrollSensitivity;
    return this;
}


HorizontalScrollWidgetContainer::HorizontalScrollWidgetContainer(WidgetRect rect)
: RTWidgetContainer(rect) {
    scrollAmount = 0;
    scrollSensitivity = 30;
}
