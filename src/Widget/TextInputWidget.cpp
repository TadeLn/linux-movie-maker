#include "TextInputWidget.hpp"

#include <SFML/Graphics.hpp>
#include <iostream>
#include "../Global.hpp"


Widget* TextInputWidget::setRect(WidgetRect rect) {
    return ButtonWidget::setRect(rect);
}

void TextInputWidget::draw(sf::RenderTarget* target) {
    if (input == "" || input.empty()) {
        text.setString(defaultText);
        text.setFillColor(sf::Color(127, 127, 127));
    } else {
        text.setString(input);
        text.setFillColor(textColor);
    }
    if (isActive()) {
        rectangle.setOutlineThickness(2);
        rectangle.setOutlineColor(sf::Color::Red);
    } else {
        rectangle.setOutlineThickness(0);
    }
    ButtonWidget::draw(target);
    return;
}

void TextInputWidget::click(sf::Vector2i mousePos) {
    ButtonWidget::click(mousePos);
    setActive(true);
}


std::string TextInputWidget::getInput() {
    return input;
}

std::string TextInputWidget::getDefaultText() {
    return defaultText;
}

sf::Color TextInputWidget::getColor() {
    return ButtonWidget::getColor();
}

sf::Color TextInputWidget::getTextColor() {
    return textColor;
}

bool TextInputWidget::isActive() {
    return active;
}


TextInputWidget* TextInputWidget::setInput(std::string input) {
    this->input = input;
    return this;
}

TextInputWidget* TextInputWidget::setDefaultText(std::string defaultText) {
    this->defaultText = defaultText;
    return this;
}

TextInputWidget* TextInputWidget::setColor(sf::Color color) {
    ButtonWidget::setColor(color);
    return this;
}

TextInputWidget* TextInputWidget::setTextColor(sf::Color textColor) {
    this->textColor = textColor;
    return this;
}

TextInputWidget* TextInputWidget::setActive(bool active) {
    this->active = active;
    if (active) {
        Global::getApp()->selectTextField(this);
    }
    return this;
}

Widget* TextInputWidget::getWidget() {
    return this;
}


TextInputWidget::TextInputWidget(std::string string, WidgetRect rect)
: ButtonWidget(string, rect) {
    setInput("");
    setDefaultText(string);
    setTextColor(sf::Color::White);
    setActive(false);
}
