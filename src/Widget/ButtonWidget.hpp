#pragma once

#include <SFML/Graphics.hpp>
#include "Widget.hpp"


class ButtonWidget : public Widget {

public:
    Widget* setRect(WidgetRect);

    void draw(sf::RenderTarget*);

    std::string getString();
    sf::Color getColor();
    sf::Color getTextColor();

    ButtonWidget* setString(std::string);
    ButtonWidget* setColor(sf::Color);
    ButtonWidget* setTextColor(sf::Color);

    ButtonWidget(std::string, WidgetRect);
    
protected:
    void updateFontSize();
    
    sf::Text text;
    sf::RectangleShape rectangle;
    sf::Color color;

};
