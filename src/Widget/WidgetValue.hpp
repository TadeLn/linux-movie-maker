#pragma once

#include <SFML/Graphics.hpp>


class WidgetValue {

public:
    WidgetValue* setAbs(float);
    WidgetValue* setRel(float);
    bool isRelative();
    float val();

    WidgetValue();
    WidgetValue(float);
    WidgetValue(float, bool);

private:
    float value;
    bool relative;

};
