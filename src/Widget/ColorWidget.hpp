#pragma once

#include <SFML/Graphics.hpp>
#include "Widget.hpp"


class ColorWidget : public Widget {

public:
    void draw(sf::RenderTarget*);

    sf::Color getColor();

    ColorWidget* setColor(sf::Color);

    ColorWidget(WidgetRect);
    
protected:
    sf::RectangleShape rectangle;

};
