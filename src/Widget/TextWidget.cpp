#include "TextWidget.hpp"

#include <SFML/Graphics.hpp>
#include <iostream>
#include "../Global.hpp"


void TextWidget::draw(sf::RenderTarget* target) {
    sf::FloatRect floatRect = getFloatRect();

    text.setCharacterSize(fontSize);
    while (text.getCharacterSize() >= 15 && (text.getLocalBounds().width > floatRect.width || text.getLocalBounds().height > floatRect.height)) {
        text.setCharacterSize(text.getCharacterSize() - 1);
    }
    switch (alignment) {
        case TEXT_ALIGNMENT_LEFT:
            text.setOrigin(0, (int)(text.getLocalBounds().height / 2 + 5));
            text.setPosition(sf::Vector2f(
                (int)(floatRect.left),
                (int)(10 + floatRect.top + (text.getLocalBounds().height / 2))
            ));
            break;

        case TEXT_ALIGNMENT_RIGHT:
            text.setOrigin((int)(text.getLocalBounds().width), (int)(text.getLocalBounds().height / 2 + 5));
            text.setPosition(sf::Vector2f(
                (int)(floatRect.left + floatRect.width),
                (int)(10 + floatRect.top + (text.getLocalBounds().height / 2))
            ));
            break;
            
        case TEXT_ALIGNMENT_CENTER:
        case TEXT_ALIGNMENT_UNSET:
        default:
            text.setOrigin((int)(text.getLocalBounds().width / 2), (int)(text.getLocalBounds().height / 2 + 5));
            text.setPosition(sf::Vector2f(
                (int)(floatRect.left + (floatRect.width / 2)),
                (int)(10 + floatRect.top + (text.getLocalBounds().height / 2))
            ));
    }
    target->draw(text);
    return;
}

void TextWidget::click(sf::Vector2i) {
    return;
}

void TextWidget::mouseMove(sf::Vector2i) {
    return;
}

void TextWidget::parentResize(sf::Vector2f) {
    return;
}


std::string TextWidget::getString() {
    return text.getString();
}

sf::Color TextWidget::getColor() {
    return text.getFillColor();
}

int TextWidget::getCharacterSize() {
    return fontSize;
}

TextAlignment TextWidget::getAlignment() {
    return alignment;
}

unsigned int TextWidget::getStyle() {
    return text.getStyle();
}


Widget* TextWidget::setRect(WidgetRect rect) {
    this->rect = rect;
    return this;
}

TextWidget* TextWidget::setString(std::string string) {
    text.setString(string);
    return this;
}

TextWidget* TextWidget::setColor(sf::Color color) {
    text.setFillColor(color);
    return this;
}

TextWidget* TextWidget::setCharacterSize(int characterSize) {
    fontSize = characterSize;
    return this;
}

TextWidget* TextWidget::setAlignment(TextAlignment alignment) {
    this->alignment = alignment;
    return this;
}

TextWidget* TextWidget::setStyle(unsigned int style) {
    text.setStyle(style);
    return this;
}


TextWidget::TextWidget(std::string string, WidgetRect rect)
: Widget() {
    setString(string);
    setRect(rect);
    setColor(sf::Color::White);
    setCharacterSize(15);
    if (!rect.getSizeY().isRelative()) {
        setCharacterSize(rect.getSizeY().val() - 2);
    }
    setAlignment(TEXT_ALIGNMENT_CENTER);

    text.setFont(*Global::getFont());
    text.setLineSpacing(1);
}
