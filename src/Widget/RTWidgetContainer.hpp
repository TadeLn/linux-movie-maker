#pragma once

#include "WidgetContainer.hpp"


class RTWidgetContainer : public WidgetContainer {

public:
    void draw(sf::RenderTarget*);
    void click(sf::Vector2i);
    void rightClick(sf::Vector2i);
    void scroll(sf::Vector2i, int);
    void mouseMove(sf::Vector2i);

    sf::RenderTexture* getRenderTexture();

    RTWidgetContainer(WidgetRect);

protected:
    sf::RenderTexture* renderTexture;
    sf::Vector2i offset;

};
