#pragma once

#include <SFML/Graphics.hpp>
#include <vector>

class WidgetContainer;
#include "WidgetRect.hpp"


class Widget {

public:

    unsigned int getId();
    WidgetContainer* getParent();
    WidgetRect* getRect();
    sf::FloatRect getFloatRect(); // Absolute float rect (relative to window)
    sf::FloatRect getFloatRectRelativeToWidget();
    bool isVisible();
    void* getExtraData();

    Widget* setParent(Widget*);
    Widget* setRect(WidgetRect);
    Widget* setVisible(bool);
    Widget* setExtraData(void*);

    bool isInRect(sf::Vector2i); // Absolute mouse pos (relative to window)
    bool isMouseOver();

    virtual void draw(sf::RenderTarget*) = 0;

    virtual void click(sf::Vector2i); // Absolute mouse pos (relative to window)
    virtual void rightClick(sf::Vector2i); // Absolute mouse pos (relative to window)
    virtual void scroll(sf::Vector2i, int); // Absolute mouse pos (relative to window)
    virtual void hover();
    virtual void unhover();
    virtual void mouseMove(sf::Vector2i); // Absolute mouse pos (relative to window)
    virtual void parentResize(sf::Vector2f); // Size in px

    void (*getClickCallback())(Widget*, sf::Vector2i);
    void (*getRightClickCallback())(Widget*, sf::Vector2i);
    void (*getScrollCallback())(Widget*, sf::Vector2i, int);
    void (*getHoverCallback())(Widget*);
    void (*getUnhoverCallback())(Widget*);
    void (*getMouseMoveCallback())(Widget*, sf::Vector2i);
    void (*getParentResizeCallback())(Widget*, sf::Vector2f);

    Widget* onClick(void (*)(Widget*, sf::Vector2i));
    Widget* onRightClick(void (*)(Widget*, sf::Vector2i));
    Widget* onScroll(void (*)(Widget*, sf::Vector2i, int));
    Widget* onHover(void (*)(Widget*));
    Widget* onUnhover(void (*)(Widget*));
    Widget* onMouseMove(void (*)(Widget*, sf::Vector2i));
    Widget* onParentResize(void (*)(Widget*, sf::Vector2f));

    sf::Vector2i absToRel(sf::Vector2i);
    sf::Vector2f absToRel(sf::Vector2f);
    sf::Vector2i relToAbs(sf::Vector2i);
    sf::Vector2f relToAbs(sf::Vector2f);

    Widget();
    virtual ~Widget();

    static unsigned int _getLastWidgetId();
    static unsigned int _getWidgetCount();
    static std::vector<Widget*> _getAllWidgets();

protected:
    unsigned int id;
    Widget* parent;
    WidgetRect rect;
    bool visible;
    void* extraData;


    bool isHover;

    void (*clickCallback)(Widget*, sf::Vector2i);
    void (*rightClickCallback)(Widget*, sf::Vector2i);
    void (*scrollCallback)(Widget*, sf::Vector2i, int);
    void (*hoverCallback)(Widget*);
    void (*unhoverCallback)(Widget*);
    void (*mouseMoveCallback)(Widget*, sf::Vector2i);
    void (*parentResizeCallback)(Widget*, sf::Vector2f);

    static unsigned int nextId;
    static std::vector<Widget*> allWidgets;
    static unsigned int getNewId(Widget*);

};
