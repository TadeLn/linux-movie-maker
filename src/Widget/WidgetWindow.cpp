#include "WidgetWindow.hpp"

#include <SFML/Graphics.hpp>
#include <iostream>
#include "../Global.hpp"


void WidgetWindow::draw(sf::RenderTarget* target) {
    WidgetContainer::draw(target);

    sf::FloatRect floatRect = getFloatRect();
    // Draw title
    text.setOrigin((int)(text.getLocalBounds().width / 2), (int)(text.getLocalBounds().height / 2 + 5));
    text.setPosition(sf::Vector2f(
        (int)(floatRect.left + (floatRect.width / 2)),
        (int)(floatRect.top + 10 + (text.getCharacterSize() / 2))
    ));
    target->draw(text);
    return;
}


std::string WidgetWindow::getTitle() {
    return text.getString();
}

sf::Color WidgetWindow::getTitleColor() {
    return text.getFillColor();
}


WidgetWindow* WidgetWindow::setTitle(std::string title) {
    text.setString(title);
    return this;
}

WidgetWindow* WidgetWindow::setTitleColor(sf::Color titleColor) {
    text.setFillColor(sf::Color::White);
    return this;
}


WidgetWindow::WidgetWindow(std::string title, WidgetRect rect)
: WidgetContainer(rect) {
    setTitle(title);
    setColor(sf::Color(42, 42, 42, 127));
    setTitleColor(sf::Color::White);

    text.setFont(*Global::getFont());
    text.setCharacterSize(20);
    text.setLineSpacing(1);
    text.setStyle(sf::Text::Bold);

    widgets = std::vector<Widget*>();
}
