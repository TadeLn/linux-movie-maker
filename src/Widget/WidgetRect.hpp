#pragma once

#include <SFML/Graphics.hpp>
#include "WidgetValue.hpp"


class WidgetRect {

public:
    WidgetValue getPositionX();
    WidgetValue getPositionY();
    WidgetValue getSizeX();
    WidgetValue getSizeY();

    WidgetRect* setPositionX(WidgetValue);
    WidgetRect* setPositionY(WidgetValue);
    WidgetRect* setSizeX(WidgetValue);
    WidgetRect* setSizeY(WidgetValue);

    WidgetRect* setAbsPosition(float, float);
    WidgetRect* setAbsPosition(sf::Vector2f);
    WidgetRect* setAbsPositionX(float);
    WidgetRect* setAbsPositionY(float);
    
    WidgetRect* setAbsSize(float, float);
    WidgetRect* setAbsSize(sf::Vector2f);
    WidgetRect* setAbsSizeX(float);
    WidgetRect* setAbsSizeY(float);
    
    WidgetRect* setRelPosition(float, float);
    WidgetRect* setRelPosition(sf::Vector2f);
    WidgetRect* setRelPositionX(float);
    WidgetRect* setRelPositionY(float);

    WidgetRect* setRelSize(float, float);
    WidgetRect* setRelSize(sf::Vector2f);
    WidgetRect* setRelSizeX(float);
    WidgetRect* setRelSizeY(float);

    sf::FloatRect getFloatRect(sf::Vector2f, sf::Vector2f);
    sf::FloatRect getFloatRect(sf::FloatRect);

    sf::Vector2<WidgetValue> position;
    sf::Vector2<WidgetValue> size;

    WidgetRect();
    WidgetRect(sf::FloatRect);
    WidgetRect(sf::Vector2f, sf::Vector2f);
    WidgetRect(float, float, float, float);
    WidgetRect(WidgetValue, WidgetValue, WidgetValue, WidgetValue);

};
