#pragma once

#include <SFML/Graphics.hpp>

class RootWidget;

#include "WidgetContainer.hpp"


class RootWidget : public WidgetContainer {

public:
    sf::FloatRect getFloatRect();

    void parentResize(sf::Vector2f);

    RootWidget(sf::RenderWindow*);
    
protected:
    sf::RenderWindow* winPtr;

};
