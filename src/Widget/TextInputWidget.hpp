#pragma once

#include <SFML/Graphics.hpp>
#include "ButtonWidget.hpp"
#include "Widget.hpp"


class TextInputWidget : protected ButtonWidget {

public:
    Widget* setRect(WidgetRect);

    void draw(sf::RenderTarget*);
    void click(sf::Vector2i);

    std::string getInput();
    std::string getDefaultText();
    sf::Color getColor();
    sf::Color getTextColor();
    bool isActive();

    TextInputWidget* setInput(std::string);
    TextInputWidget* setDefaultText(std::string);
    TextInputWidget* setColor(sf::Color);
    TextInputWidget* setTextColor(sf::Color);
    TextInputWidget* setActive(bool);

    Widget* getWidget();

    TextInputWidget(std::string, WidgetRect);
    
protected:
    std::string input;
    std::string defaultText;
    sf::Color textColor;
    bool active;

};
