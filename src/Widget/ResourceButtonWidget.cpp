#include "ResourceButtonWidget.hpp"
#include "../Timeline/Clip/ResourceClip.hpp"
#include "../Timeline/Timeline.hpp"
#include "../Exception.hpp"
#include "../Global.hpp"


void ResourceButtonWidget::click(sf::Vector2i mousePos) {
    ButtonWidget::click(mousePos);

    ResourceClip* clip = new ResourceClip();
    Timeline* timeline = Global::getApp()->project->getTimeline();
    clip->setTime(timeline->getCurrentPosition());
    clip->setLength(Global::getApp()->project->secondsToFrame(5));
    clip->setResourceId(((ResourceButtonWidget*)this)->getResourceId());
    try {
        Global::getApp()->addClip(Global::getApp()->getActiveClip().first, clip);
    } catch (Exception* e) {
        e->print();
        delete clip;
    }
}

void ResourceButtonWidget::rightClick(sf::Vector2i mousePos) {
    ButtonWidget::rightClick(mousePos);
    Global::getApp()->showRenameResourceMessageBox(getResourceId());
}


unsigned int ResourceButtonWidget::getResourceId() {
    return resourceId;
}

ResourceButtonWidget* ResourceButtonWidget::setResourceId(unsigned int resourceId) {
    this->resourceId = resourceId;
    return this;
}


ResourceButtonWidget::ResourceButtonWidget(WidgetRect rect)
: ButtonWidget("", rect) {
    setResourceId(0);
}
