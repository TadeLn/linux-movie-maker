#pragma once

#include <SFML/Graphics.hpp>
#include "Widget.hpp"


class ImageWidget : public Widget {

public:
    void draw(sf::RenderTarget*);

    sf::Texture* getTexture();
    sf::Color getColor();

    ImageWidget* setImage(sf::Image*);
    ImageWidget* setTexture(sf::Texture*);
    ImageWidget* setColor(sf::Color);

    ImageWidget(WidgetRect);
    
protected:
    sf::Color color;
    sf::Texture* texture;

};
