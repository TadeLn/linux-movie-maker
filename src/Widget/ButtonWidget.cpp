#include "ButtonWidget.hpp"

#include <SFML/Graphics.hpp>
#include <iostream>
#include "../Global.hpp"


void ButtonWidget::draw(sf::RenderTarget* target) {
    if (isHover) {
        rectangle.setFillColor(sf::Color(170, 170, 170));
    } else {
        rectangle.setFillColor(color);
    }
    sf::FloatRect floatRect = getFloatRect();
    //std::cout << "Rect: " << floatRect.left << ", " << floatRect.top << ", " << floatRect.width << ", " << floatRect.height << "\n";
    rectangle.setPosition(floatRect.left, floatRect.top);
    rectangle.setSize(sf::Vector2f(floatRect.width, floatRect.height));
    target->draw(rectangle);

    updateFontSize();
    text.setPosition(sf::Vector2f(
        (int)(floatRect.left + (floatRect.width / 2)),
        (int)(floatRect.top + (floatRect.height / 2))
    ));
    target->draw(text);
    return;
}


std::string ButtonWidget::getString() {
    return text.getString();
}

sf::Color ButtonWidget::getColor() {
    return color;
}

sf::Color ButtonWidget::getTextColor() {
    return text.getFillColor();
}


Widget* ButtonWidget::setRect(WidgetRect rect) {
    this->rect = rect;
    return this;
}

ButtonWidget* ButtonWidget::setString(std::string string) {
    text.setString(string);
    return this;
}

ButtonWidget* ButtonWidget::setColor(sf::Color color) {
    this->color = color;
    return this;
}

ButtonWidget* ButtonWidget::setTextColor(sf::Color textColor) {
    text.setFillColor(sf::Color::White);
    return this;
}


ButtonWidget::ButtonWidget(std::string string, WidgetRect rect)
: Widget() {
    setString(string);
    setRect(rect);

    setTextColor(sf::Color::White);
    color = sf::Color(42, 42, 42);

    text.setFont(*Global::getFont());
    text.setLineSpacing(1);
}


void ButtonWidget::updateFontSize() {
    text.setCharacterSize(30);
    sf::FloatRect floatRect = getFloatRect();
    while (text.getCharacterSize() >= 15 && (text.getLocalBounds().width > floatRect.width - 20 || text.getLocalBounds().height > floatRect.height - 20)) {
        text.setCharacterSize(text.getCharacterSize() - 1);
    }
    text.setOrigin((int)(text.getLocalBounds().width / 2), (int)(text.getLocalBounds().height / 2 + 5));
}
