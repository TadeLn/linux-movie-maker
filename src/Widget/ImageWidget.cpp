#include "ImageWidget.hpp"

#include <SFML/Graphics.hpp>
#include <iostream>
#include "../Global.hpp"


void ImageWidget::draw(sf::RenderTarget* target) {
    sf::FloatRect floatRect = getFloatRect();

    sf::RectangleShape rectangle;
    rectangle.setPosition(floatRect.left, floatRect.top);
    rectangle.setSize(sf::Vector2f(floatRect.width, floatRect.height));
    rectangle.setTexture(texture);
    rectangle.setFillColor(color);
    target->draw(rectangle);
    return;
}


sf::Color ImageWidget::getColor() {
    return color;
}

ImageWidget* ImageWidget::setImage(sf::Image* image) {
    texture->loadFromImage(*image);
    return this;
}

ImageWidget* ImageWidget::setTexture(sf::Texture* texture) {
    this->texture = texture;
    return this;
}

ImageWidget* ImageWidget::setColor(sf::Color color) {
    this->color = color;
    return this;
}


ImageWidget::ImageWidget(WidgetRect rect)
: Widget() {
    setRect(rect);
    setColor(sf::Color::White);
    texture = Global::getInvalidTexture();
}
