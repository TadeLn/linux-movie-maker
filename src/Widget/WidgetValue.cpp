#include "WidgetValue.hpp"

#include <SFML/Graphics.hpp>


WidgetValue* WidgetValue::setAbs(float value) {
    value = value;
    relative = false;
    return this;
}

WidgetValue* WidgetValue::setRel(float value) {
    value = value;
    relative = true;
    return this;
}

bool WidgetValue::isRelative() {
    return relative;
}

float WidgetValue::val() {
    return value;
}

WidgetValue::WidgetValue() {
    value = 0;
    relative = false;
}

WidgetValue::WidgetValue(float value) {
    this->value = value;
    relative = false;
}

WidgetValue::WidgetValue(float value, bool relative) {
    this->value = value;
    this->relative = relative;
}
