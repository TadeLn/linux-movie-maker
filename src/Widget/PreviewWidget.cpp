#include "PreviewWidget.hpp"

#include <iostream>
#include "WidgetContainer.hpp"
#include "WidgetRect.hpp"
#include "../Global.hpp"


void PreviewWidget::draw(sf::RenderTarget* target) {
    sf::FloatRect floatRect = getFloatRect();
    Project* project = Global::getApp()->project;

    WidgetRect oldWidgetRect = rect;
    rect.setAbsPosition(offset.x - parent->getFloatRect().left, offset.y - parent->getFloatRect().top);

    // Draw children onto bg
    for (int i = 0; i < widgets.size(); i++) {
        if (widgets[i]->isVisible()) {
            widgets[i]->draw(renderTexture);
        }
    }

    // Finish drawing texture
    rect = oldWidgetRect; 
    renderTexture->display();

    sf::Vector2f size;
    sf::RectangleShape shape;
    shape.setPosition(sf::Vector2f(floatRect.left, floatRect.top));
    float multiplierX = floatRect.width / project->getWidth();
    float multiplierY = floatRect.height / project->getHeight();
    if (multiplierX < multiplierY) {
        size.x = project->getWidth() * multiplierX;
        size.y = project->getHeight() * multiplierX;
        shape.setPosition(sf::Vector2f(floatRect.left, floatRect.top + ((floatRect.height - size.y) / 2)));
    } else {
        size.x = project->getWidth() * multiplierY;
        size.y = project->getHeight() * multiplierY;
        shape.setPosition(sf::Vector2f(floatRect.left + ((floatRect.width - size.x) / 2), floatRect.top));
    }
    shape.setTexture(&renderTexture->getTexture());
    shape.setTextureRect(sf::IntRect(0, 0, project->getWidth(), project->getHeight()));
    shape.setSize(size);
    target->draw(shape);

    renderTexture->clear(sf::Color::Transparent);
    renderTexture->create(project->getWidth(), project->getHeight());
}

PreviewWidget::PreviewWidget(WidgetRect rect)
: RTWidgetContainer(rect) {}
