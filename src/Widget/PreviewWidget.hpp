#pragma once

#include "RTWidgetContainer.hpp"


class PreviewWidget : public RTWidgetContainer {

public:
    void draw(sf::RenderTarget*);

    PreviewWidget(WidgetRect);

};
