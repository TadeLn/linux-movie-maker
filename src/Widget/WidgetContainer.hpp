#pragma once

class WidgetContainer;

#include <SFML/Graphics.hpp>
#include "Widget.hpp"


class WidgetContainer : public Widget {

public:
    Widget* setRect(WidgetRect);

    void draw(sf::RenderTarget*);
    void click(sf::Vector2i);
    void rightClick(sf::Vector2i);
    void scroll(sf::Vector2i, int);
    void mouseMove(sf::Vector2i);
    void unhover();
    void parentResize(sf::Vector2f);

    sf::Color getColor();
    WidgetContainer* setColor(sf::Color);

    WidgetContainer* addWidget(Widget*);
    std::vector<Widget*>* getChildren();
    WidgetContainer* clearChildren();

    WidgetContainer(WidgetRect);
    ~WidgetContainer();
    
protected:
    std::vector<Widget*> widgets;

    sf::RectangleShape rectangle;

};
