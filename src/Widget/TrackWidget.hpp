#pragma once

#include <SFML/Graphics.hpp>
#include "ButtonWidget.hpp"
#include "TextWidget.hpp"
#include "Widget.hpp"


class TrackWidget : protected ButtonWidget {

public:
    void draw(sf::RenderTarget*);
    void click(sf::Vector2i);

    int getTrackId();
    std::string getName();
    sf::Color getColor();

    TrackWidget* setTrackId(int);
    TrackWidget* setName(std::string);
    TrackWidget* setColor(sf::Color);

    Widget* getWidget();

    TrackWidget(WidgetRect);
    ~TrackWidget();
    
protected:
    int trackId;
    std::string name;
    TextWidget* trackNameWidget;
    TextWidget* trackIdWidget;

};
