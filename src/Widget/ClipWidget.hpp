#pragma once

#include <SFML/Graphics.hpp>
#include "RTWidgetContainer.hpp"
#include "TextWidget.hpp"
#include "ImageWidget.hpp"
#include "../Timeline/Clip/Clip.hpp"


class ClipWidget : public RTWidgetContainer {

public:
    void draw(sf::RenderTarget*);
    void click(sf::Vector2i);
    
    Clip* getClip();
    int getTrackId();
    unsigned int getClipId();

    ClipWidget* setClip(int, Clip*);

    void updateRect();

    ClipWidget();
    
protected:
    Clip* clip;
    int track;

    TextWidget* textWidget;
    ImageWidget* imageWidget;
    sf::RectangleShape outline;

};
