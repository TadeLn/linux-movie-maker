#pragma once

#include <SFML/Graphics.hpp>
#include "Widget.hpp"
#include "../Application.hpp"


class WidgetWindow : public WidgetContainer {

public:
    Widget* setRect(WidgetRect);

    void draw(sf::RenderTarget*);

    std::string getTitle();
    sf::Color getTitleColor();

    WidgetWindow* setTitle(std::string);
    WidgetWindow* setTitleColor(sf::Color);

    WidgetWindow(std::string, WidgetRect);
    
protected:
    sf::Text text;

};
