#include "RootWidget.hpp"

#include <vector>
#include <iostream>
#include <chrono>
#include <thread>


sf::FloatRect RootWidget::getFloatRect() {
    return sf::FloatRect(0, 0, winPtr->getView().getSize().x, winPtr->getView().getSize().y);
}

void RootWidget::parentResize(sf::Vector2f newSize) {
    rect.setAbsSize(newSize);
    for (int i = 0; i < widgets.size(); i++) {
        widgets[i]->parentResize(sf::Vector2f(this->getFloatRect().width, this->getFloatRect().height));
    }
}



RootWidget::RootWidget(sf::RenderWindow* winPtr)
: WidgetContainer(WidgetRect(0, 0, 0, 0)) {
    this->winPtr = winPtr;
    rect = *((new WidgetRect())
        ->setAbsPositionX(0)
        ->setAbsPositionY(0)
        ->setAbsSizeX(winPtr->getView().getSize().x) // Window width
        ->setAbsSizeY(winPtr->getView().getSize().y) // Window height
    );
    widgets = std::vector<Widget*>();
}
