#include "Settings.hpp"

#include <vector>
#include <utility>
#include <string>
#include <iostream>
#include <fstream>

#include "Exception.hpp"
#include "Util.hpp"


namespace Settings {

    std::string getSetting_string(std::string setting) {
        for (int i = 0; i < settings.size(); i++) {
            if (settings[i].first == setting) {
                return settings[i].second;
            }
        }
        // If no setting is found, add a new setting, with an empty value
        setSetting(setting, "");
        return std::string("");
    }

    long getSetting_long(std::string setting) {
        return std::stol(getSetting_string(setting));
    }

    long getSetting_long(std::string setting, long defaultValue) {
        try {
            return getSetting_long(setting);
        } catch (std::exception& e) {
            std::cout << e.what();
            return defaultValue;
        }
    }

    unsigned long getSetting_ulong(std::string setting) {
        return std::stoul(getSetting_string(setting));
    }

    unsigned long getSetting_ulong(std::string setting, unsigned long defaultValue) {
        try {
            return getSetting_ulong(setting);
        } catch (std::exception& e) {
            std::cout << e.what();
            return defaultValue;
        }
    }

    double getSetting_double(std::string setting) {
        return std::stod(getSetting_string(setting));
    }

    double getSetting_double(std::string setting, double defaultValue) {
        try {
            return getSetting_double(setting);
        } catch (std::exception& e) {
            std::cout << e.what();
            return defaultValue;
        }
    }


    void setSetting(std::string setting, std::string value) {
        for (int i = 0; i < settings.size(); i++) {
            if (settings[i].first == setting) {
                settings[i].second = value;
                return;
            }
        }
        settings.push_back(std::pair<std::string, std::string>(setting, value));
    }

    void setSetting(std::string setting, long value) {
        setSetting(setting, std::to_string(value));
    }

    void setSetting(std::string setting, unsigned long value) {
        setSetting(setting, std::to_string(value));
    }

    void setSetting(std::string setting, double value) {
        setSetting(setting, std::to_string(value));
    }

    void loadSettings(std::string filename) {
        std::ifstream file(filename);
        if (file.is_open()) {
            std::string line;
            while (std::getline(file, line)) {
                Util::ltrim(line);

                if (line.empty()) continue;

                if (line[0] == '#') continue;

                std::string setting = line.substr(0, line.find_first_of('='));
                Util::trim(setting);

                std::string value = line.substr(line.find_first_of('=') + 1);

                setSetting(setting, value);

            }
            file.close();
        } else {
            throw new Exception("Could not load settings from file " + filename, EXCEPTION_FILE_ERROR);
        }
    }

};
